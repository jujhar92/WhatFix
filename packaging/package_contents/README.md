# GEORBIS README

This package is designed in a way so as to not need root priveledges to run. It is also self contained in that it does not need any other dependencies. This package has been tested on RHEL7 (minimal) as well as Ubuntu 14.04LTS.

## Usage

Starting the server:
```
$ chmod a+x bin/georbis.sh
$ bin/georbis.sh start
```

Stopping the server:
```
$ bin/georbis.sh stop
```

## Post Installation
Add a firewall exception for port 19090 on your machine if this server is to be accessible from outside.

On RHEL 7, this can be achieved using:

```
$ su
# firewall-cmd --zone=public --add-port=19090/tcp --permanent
# firewall-cmd --reload
```