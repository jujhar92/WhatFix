#!/bin/bash

# Check if deps folder exists
if [ ! -d "$GEORBIS_PATH/deps" ]; then
    echo "ERROR: deps folder does not exist. Installation invalid."
    return 1
fi

# Check if deps folder is empty; if so terminate
[ "$(ls -A "$GEORBIS_PATH"/deps)" ] && deps_empty=false || deps_empty=true
if [ "$deps_empty" = true ]; then
    echo "ERROR: Deps folder empty. Installation invalid"
    return 1
fi

# Now, we iterate over all the directories in deps and add all the lib
# folders to LD_LIBRARY_PATH and all the bin folders to PATH
IFS=$'\n'
for dir in `ls "$GEORBIS_PATH/deps/"`; do
    lib_dir="$GEORBIS_PATH/deps/$dir/lib"    
    if [ -d "$lib_dir" ]; then
        LD_LIBRARY_PATH="$lib_dir":$LD_LIBRARY_PATH;
    fi
    bin_dir="$GEORBIS_PATH/deps/$dir/bin"    
    if [ -d "$bin_dir" ]; then
        PATH="$bin_dir":$PATH;
        chmod -R a+x "$bin_dir"
    fi    
done

export PATH=$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH
