#!/bin/bash

# Starts/stops Apache Tomcat

start () {
    # Starts nginx web server
    printf "Starting web server..."
    status=$("$GEORBIS_BIN_PATH/http_server.sh" start)
    printf "$status\n" 

    # Start the Database server
    printf "Starting database server..."
    status=$("$GEORBIS_BIN_PATH/database_server.sh" start)
    printf "$status\n"

    # Start the Tomcat server
    printf "Starting servelet container..."
    status=$("$GEORBIS_BIN_PATH/servelet_container.sh" start)
    printf "$status\n"   

    # Georbis Web Services
    printf "Starting GIS Services..."
    # Start the fcgi processes
    status=$("$GEORBIS_BIN_PATH/gis_services.sh" start)
    printf "$status\n"
}
stop () {
    # Stop nginx web server
    printf "Stopping web server..."
    status=$("$GEORBIS_BIN_PATH/http_server.sh" stop)
    printf "$status\n" 

    # Stop the Database server
    printf "Stopping database server..."
    status=$("$GEORBIS_BIN_PATH/database_server.sh" stop)
    printf "$status\n"

    # Stop the Tomcat server
    printf "Stopping servelet container..."
    status=$("$GEORBIS_BIN_PATH/servelet_container.sh" stop)
    printf "$status\n"   

    # Stop Georbis Web Services
    printf "Stopping Georbis Web Services..."
    # Start the fcgi processes
    status=$("$GEORBIS_BIN_PATH/gis_services.sh" stop)
    printf "$status\n"
}
restart () {
    stop
    start
}
get_current_script_dir(){
    # Get path to current script file
    current_script_dir="${BASH_SOURCE[0]}"
    while [ -h "$current_script_dir" ]; do # resolve $current_script_dir until the file is no longer a symlink
      current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
      current_script_dir="$(readlink "$current_script_dir")"
      [[ $current_script_dir != /* ]] && current_script_dir="$current_dir/$current_script_dir" # if $current_script_dir was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
    echo "$current_dir"
}

if [ -z "$GEORBIS_PATH" ]; then
    set -a
    GEORBIS_PATH="$(dirname $(get_current_script_dir))"
    GEORBIS_BIN_PATH="$GEORBIS_PATH/bin"

    # Make all script files executable
    chmod a+x "$GEORBIS_PATH/bin"/*.sh

    # Make sure we have a deps folder
    if [ ! -d "$GEORBIS_PATH/deps" ]; then
        echo "ERROR: Unable to locate deps directory. Invalid installation."
        return 1
    fi

    # Invoke the configuration variables
    source "$GEORBIS_PATH/config/configuration.sh"

    # Setup runtime paths using the dependencies shipped in deps folder
    source "$GEORBIS_PATH/bin/runtime_env.sh"
fi

. "$GEORBIS_PATH/bin/util.sh"

# See how we were called.
case "$1" in
  start)
        start
    ;;
  stop)
        stop
    ;;
  status)
        status
        ;;
  restart)
    restart
        ;;
  *)
  # Invalid mode:  
    echo "Usage: $0 {start|stop|status|restart}"
    return 1
esac
exit 0