#!/bin/bash
#
# description: Georbis Service Manager
# pidfile: ../data/georbis.pid

start () {
    # Setup the startup environment for spawning fcgi
    setupEnv

    # Spawn the testcgi process
    test_status=$(spawn_fcgi_process "$georbis_dir/bin/georbis-testservice" $GEORBIS_TEST_FCGI_PORT "georbis-test")

    # Spawn the map service
    map_status=$(spawn_fcgi_process "$georbis_dir/bin/georbis-mapservice" $GEORBIS_WMS_FCGI_PORT "georbis-map")    
    
    # Spawn the feature service
    feature_status=$(spawn_fcgi_process "$georbis_dir/bin/georbis-featureservice" $GEORBIS_WFS_FCGI_PORT "georbis-feat")

    # Spawn the processing service
    processing_status=$(cd "$GEORBIS_PATH/data/service_data/data/processing" && spawn_fcgi_process "$georbis_dir/bin/georbis-processingservice" $GEORBIS_WPS_FCGI_PORT "georbis-proc")

    if [[ "$test_status" == "OK" && "$map_status" == "OK" && "$feature_status" == "OK" && "$processing_status" == "OK" ]]; then
        echo "OK"
    else
        echo "FAILIURE"    
    fi
}
stop () {
    test_status=$(stop_fcgi_process "georbis-test")
    map_status=$(stop_fcgi_process "georbis-map")
    feature_status=$(stop_fcgi_process "georbis-feat")
    processing_status=$(stop_fcgi_process "georbis-proc")

    if [[ "$test_status" == "OK" && "$map_status" == "OK" && "$feature_status" == "OK" && "$processing_status" == "OK" ]]; then
        echo "OK"
    else
        echo "FAILIURE"    
    fi
}
restart () {
    stop
    start
}
get_current_script_dir(){
    # Get path to current script file
    current_script_dir="${BASH_SOURCE[0]}"
    while [ -h "$current_script_dir" ]; do # resolve $current_script_dir until the file is no longer a symlink
      current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
      current_script_dir="$(readlink "$current_script_dir")"
      [[ $current_script_dir != /* ]] && current_script_dir="$current_dir/$current_script_dir" # if $current_script_dir was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
    echo "$current_dir"
}
spawn_fcgi_process(){
    # Spawns a FCGI process with the specified parameters
    # arg1 - path to the FCGI process to be run
    # arg2 - port number in which the process must listen
    # arg3 - process name to use when checking in process table

    # Check if there is an already running instance of map service
    run_status=$(check_if_process_is_running "$3")
    if [[ "$run_status" == "yes" ]]; then
        printf "Restarting $3 fcgi process...\n"
        stop
    fi

    pidfile="$GEORBIS_PATH/data/$3.pid"
    "$nginx_dir/sbin/spawn-fcgi" -f "$1" -p $2 -P "$pidfile" -a 127.0.0.1 >> "$GEORBIS_PATH/data/logs/$3_spawn.log" 2>&1

    sleep 3

    status="OK"

    # Make sure that we have spawned the georbis-mapservice process
    # Open the PID file and read the PID
    georbis_service_pid=$(head -n 1 "$pidfile")
    if [[ -z "$georbis_service_pid" ]]; then
        status="FAILED"
        printf "Unable find PID of spawned $3 fcgi process\n"
    else
        # Search for this PID in the process list
        run_status=$(check_process_status "$3" "$georbis_service_pid")
        if [[ "$run_status" != "RUNNING" ]]; then
            status="FAILED"
            printf "Unable to spawn $3 fcgi process\n"            
        fi
    fi

    echo "$status"
}
stop_fcgi_process(){
    # Stops a FCGI process with the specified parameters
    # arg1 - process name to use when checking in process table
    status="OK"
    # Open the PID file and read the PID
    pidfile="$GEORBIS_PATH/data/$1.pid"
    georbis_service_pid=$(head -n 1 "$pidfile")
    if [[ -n "$georbis_service_pid" ]]; then
        # Search for this PID in the process list
        ps_georbis_service=$(ps cax | grep "$georbis_service_pid")
        # If a process does indeed exist with the current PID
        # then kill it
        if [[ ! -z "$ps_georbis_service" ]]; then
            kill -9 "$georbis_service_pid"
        else
            # No problem, no service already running
            status="OK"                
        fi
    else
        status="FAILIURE"
        printf "Unable to read the PID file of active $1\n"
    fi
    echo "$status"
}
setupEnv(){
    # Find the Proj4 directory
    proj_dir=$(find_deps_folder "proj.4" "required")

    # Prepare the template main.cfg file for the Processing service and copy it to appropriate folder
    declare -A config_params
    config_params[GEORBIS_SERVER_ADDRESS]="http://$GEORBIS_POSTGRES_HOST:$GEORBIS_MAIN_SERVER_PORT"
    config_params[GEORBIS_DATA_DIR]="$GEORBIS_PATH/data"
    config_params[GEORBIS_SERVICE_DATA]="$GEORBIS_PATH/data/service_data"
    config_params[GEORBIS_SERVICES_DIR]="$georbis_dir"
    config_params[GEORBIS_POSTGRES_USERNAME]="$GEORBIS_POSTGRES_USERNAME"
    config_params[GEORBIS_POSTGRES_PASSWORD]="$GEORBIS_POSTGRES_PASSWORD"
    config_params[GEORBIS_POSTGRES_PORT]="$GEORBIS_POSTGRES_PORT"
    config_params[GEORBIS_GIS_DB_NAME]="$GEORBIS_GIS_DB_NAME"
    config_params[GEORBIS_PROJ_DATA]="$proj_dir/share/proj"

    mkdir -p "$GEORBIS_PATH/data/service_data/config"

    # WMS configuration setup
    # Setup the Proj library env var
    export PROJ_LIB="$proj_dir/share/proj"
    # Create the service data folder for wms
    mkdir -p "$GEORBIS_PATH/data/service_data/data/wms"
    # Copy over the sample world map file if it does not already exist
    if [ ! -f "$GEORBIS_PATH/data/service_data/data/wms/world.tif" ]; then
        cp "$GEORBIS_PATH/sample_data/raster/world.tif" "$GEORBIS_PATH/data/service_data/data/wms/world.tif"       
    fi
    cp "$GEORBIS_PATH/config/templates/georbis-services/wms/world.map" "$GEORBIS_PATH/data/service_data/data/wms/world.map"
    # Sed replace all occurences of config keys with correspdnding values from config_params
    for param in "${!config_params[@]}"; do
        sed -i "s|@@$param@@|${config_params[$param]}|g" "$GEORBIS_PATH/data/service_data/data/wms/world.map"
    done 

    # WFS configuration setup
    # Set the environment variable indicating where to find the config file for feature service
    export GEORBIS_WFS_CONFIG_FILE="$GEORBIS_PATH/data/service_data/config/wfs.xml"
    # Copy over the config file from templates directory
    cp "$GEORBIS_PATH/config/templates/georbis-services/wfs/wfs.xml" "$GEORBIS_PATH/data/service_data/config/wfs.xml"
    # Sed replace all occurences of config keys with correspdnding values from config_params
    for param in "${!config_params[@]}"; do
        sed -i "s|@@$param@@|${config_params[$param]}|g" "$GEORBIS_PATH/data/service_data/config/wfs.xml"
    done
    # Upload default data from of world boundaries shape file that comes with the packaging
    # We do this only once during first install, hence we create a file in data folder indicating
    # that sample data has been installed
    install_check_file="$GEORBIS_PATH/data/service_data/data/wfs/world_data.sql"
    if [ ! -f "$install_check_file" ]; then
        # This is a first time installation
        # Create an empty check file
        mkdir -p "$GEORBIS_PATH/data/service_data/data/wfs"
        touch "$install_check_file"
        # Set the password for this PostGres server
        export PGPASSWORD=$GEORBIS_POSTGRES_PASSWORD
        # Change directory into sample_data folder and write sql instructions needed
        # to upload sample shape file into an sql file
        ( cd "$GEORBIS_PATH/sample_data/vector/world_boundaries" && shp2pgsql -g geom -s 4326 -W LATIN1 -I TM_WORLD_BORDERS_SIMPL-0.3.shp world_boundaries > "$install_check_file" 2>"$GEORBIS_PATH/data/logs/wfs_install.log" ) 
        # Now execute the sql instructions in database
        psql -f "$install_check_file" -U $GEORBIS_POSTGRES_USERNAME -d $GEORBIS_GIS_DB_NAME --no-password -h $GEORBIS_POSTGRES_HOST -p $GEORBIS_POSTGRES_PORT  >>"$GEORBIS_PATH/data/logs/wfs_install.log" 2>&1
    fi

    # WPS configuration setup
    # Copy over the template WPS main.cfg file to data directory
    mkdir -p "$GEORBIS_PATH/data/service_data/data/processing" 	
    cp "$GEORBIS_PATH/config/templates/georbis-services/wps/main.cfg" "$GEORBIS_PATH/data/service_data/data/processing/main.cfg"
    # Remove any previously existent symbolic link
    if [ -f "$georbis_dir/bin/main.cfg" ]; then
        rm "$georbis_dir/bin/main.cfg"
    fi
    # Create a symbolic link to this file in the service bin folder
    #ln -s "$GEORBIS_PATH/data/service_data/data/processing/main.cfg" "$georbis_dir/bin/main.cfg"
    # Sed replace all occurences of config keys with correspdnding values from config_params
    for param in "${!config_params[@]}"; do
        sed -i "s|@@$param@@|${config_params[$param]}|g" "$GEORBIS_PATH/data/service_data/data/processing/main.cfg"
    done
    # Create 3 folders needed by WFS server to write files to
    mkdir -p "$GEORBIS_PATH/data/service_data/data/processing/cache"
    mkdir -p "$GEORBIS_PATH/data/service_data/data/processing/tmp"
}

if [ -z "$GEORBIS_PATH" ]; then
    set -a
    GEORBIS_PATH="$(dirname $(get_current_script_dir))"
    # Invoke the configuration variables
    source "$GEORBIS_PATH/config/configuration.sh"
    # Setup runtime paths using the dependencies shipped in deps folder
    source "$GEORBIS_PATH/bin/runtime_env.sh"
fi

. "$GEORBIS_PATH/bin/util.sh"

. "$GEORBIS_PATH/bin/util.sh"

# First, we find the path to the nginx server directory
nginx_dir=$(find_deps_folder "nginx" "required")

# Make sure that the GEORBIS_MAPSERVICES_PORT environment variable is defined
if [ -z "$GEORBIS_WMS_FCGI_PORT" ]; then
    echo "GEORBIS_WMS_FCGI_PORT environment variable must be set before calling this script"
    exit
fi

# Find the Georbis services directory
georbis_dir=$(find_deps_folder "georbis-services" "required")

# See how we were called.
case "$1" in
  start)
        start
    ;;
  stop)
        stop
    ;;
  status)
    status georbis-mapservice
        RETVAL=$?
        ;;
  restart)
    restart
        ;;
  *)
        echo "Usage: $0 {start|stop|status|restart}"
        RETVAL=2
        ;;
esac
exit $RETVAL
