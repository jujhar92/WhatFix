#!/bin/bash

# Starts/stops Apache Tomcat

start () {
    # Check if there is an instance already running
    # Check to make sure that the process has indeed been stopped
    catalina_pid=$(head -n 1 "$CATALINA_PID")
    status=$(check_process_status "java" "$catalina_pid")
    if [[ "$status" == "RUNNING" ]]; then
        # Restart the application
        printf "Stopping current servelet container instance...\n"
        stop
        printf "Spawning new servelet container instance...\n"
    fi

    # Start Apache Tomcat
    catalina.sh start >> "$GEORBIS_PATH/data/logs/catalina_start_log.txt" 2>&1

    sleep 3
    
    # Make sure the process started
    # Open the PID file and read the PID
    catalina_pid=$(head -n 1 "$CATALINA_PID")
    status=$(check_process_status "java" "$catalina_pid")
    if [[ "$status" == "RUNNING" ]]; then
        echo "OK"
    else
        echo "FAILED"    
    fi
}
stop () {
    # Cache the PID of currently running Tomcat process
    # since the stop script removes the PID file after stop
    catalina_pid=$(head -n 1 "$CATALINA_PID")

    # Stop Apache Tomcat
    catalina.sh stop n -force >>"$GEORBIS_PATH/data/logs/catalina_start_log.txt" 2>&1

    # Check to make sure that the process has indeed been stopped
    status=$(check_process_status "java" "$catalina_pid")
    if [[ "$status" == "NOT RUNNING" ]]; then
        echo "OK"
    else
        # Its possible that user simply tried to stop and already stopped server
        echo "FAILED"
    fi
}
restart () {
    stop
    start
}
get_current_script_dir(){
    # Get path to current script file
    current_script_dir="${BASH_SOURCE[0]}"
    while [ -h "$current_script_dir" ]; do # resolve $current_script_dir until the file is no longer a symlink
      current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
      current_script_dir="$(readlink "$current_script_dir")"
      [[ $current_script_dir != /* ]] && current_script_dir="$current_dir/$current_script_dir" # if $current_script_dir was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
    echo "$current_dir"
}

if [ -z "$GEORBIS_PATH" ]; then
    GEORBIS_PATH=$(dirname $(get_current_script_dir))    
    # Invoke the configuration variables
    source "$GEORBIS_PATH/config/configuration.sh"
    # Setup runtime paths using the dependencies shipped in deps folder
    source "$GEORBIS_PATH/bin/runtime_env.sh"
fi

. "$GEORBIS_PATH/bin/util.sh"

# First, we find the path to the nginx server directory
tomcat_dir=$(find_deps_folder "tomcat_dir" "required")

# Make sure we have an java directory
jre_dir=$(find_deps_folder "jre" "required")

# Set the JAVA_HOME variable
export JAVA_HOME="$jre_dir"    
touch "$GEORBIS_PATH/data/catalina.pid"
export CATALINA_PID="$GEORBIS_PATH/data/catalina.pid"

# See how we were called.
case "$1" in
  start)
        start
    ;;
  stop)
        stop
    ;;
  status)
        status
        ;;
  restart)
    restart
        ;;
  *)
  # Invalid mode:  
    echo "Usage: $0 {start|stop|status|restart}"
    return 1
esac
exit 0