#!/bin/bash

# Starts/stops Postgres server


setup() {    
    # Check if the configuration script has been run
    if (( ${#GEORBIS_CONFIG_CALLED} == 0 )); then
        echo "config/configuration.sh must be invoked before calling install.sh!"   
        return 1
    fi

    # Check if installation has already been done - i.e. check if a data folder
    # exists. If it does, print an error and abort
    if [ -d "$GEORBIS_PATH/data" ]; then
        echo "ERROR: Existing installation found. Either run uninstall.sh or pick another directory."
        return 1  
    fi

    # Create a folder where the data will reside
    mkdir -p "$GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY"
    chmod 0700 "$GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY"

    # Create a folder for logs
    mkdir -p "$GEORBIS_PATH/data/logs"

    # PostGres Setup
    # Start by checking if the PostGres we need to use is that available in the system
    # or that embedded with the installation
    if [ "$GEORBIS_USE_EMBEDDED_POSTGRES" = true ] ; then
        # First locate the postgres folder
        postgres_dir=$(find "$GEORBIS_PATH/deps" -maxdepth 1 -type d -name '*postgres*');

        # Set the port number as required
        export PGPORT=$GEORBIS_POSTGRES_PORT

        # Set the password for this PostGres server
        export PGPASSWORD=$GEORBIS_POSTGRES_PASSWORD
        
        # Create a folder to house POSTGRES data    
        mkdir -p "$GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY"

        # Inititalize database with specified username
        initdb -D "$GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY" -U "$GEORBIS_POSTGRES_USERNAME" -A trust >>"$GEORBIS_PATH/data/logs/pg_install_log.txt" 2>&1
            
        # Start the PostGres server
        nohup pg_ctl --pgdata="$GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY" start >>"$GEORBIS_PATH/data/logs/pg_install_log.txt" 2>&1
        sleep 5
    fi

    # The following steps are common irrespective of whether we use an embedded server or not

    # First create a database named authdb for authorization tables
    createdb --host=$GEORBIS_POSTGRES_HOST --port=$GEORBIS_POSTGRES_PORT --username="$GEORBIS_POSTGRES_USERNAME" "$GEORBIS_AUTH_DB_NAME" >>"$GEORBIS_PATH/data/logs/pg_install_log.txt" 2>&1

    # Next create a database named gis for all the gis stores
    createdb --host=$GEORBIS_POSTGRES_HOST --port=$GEORBIS_POSTGRES_PORT --username="$GEORBIS_POSTGRES_USERNAME" "$GEORBIS_GIS_DB_NAME" >>"$GEORBIS_PATH/data/logs/pg_install_log.txt" 2>&1

    # Enable PostGIS extensions on the GIS tables
    declare -a postgis_extension_commands=("CREATE EXTENSION fuzzystrmatch;"  "CREATE EXTENSION postgis;" "CREATE EXTENSION postgis_topology;" "CREATE EXTENSION postgis_tiger_geocoder;")

    for postgis_cmd in "${postgis_extension_commands[@]}" ; do
        psql --dbname="$GEORBIS_GIS_DB_NAME" --port=$GEORBIS_POSTGRES_PORT --username=$GEORBIS_POSTGRES_USERNAME --no-password --command="$postgis_cmd" >>"$GEORBIS_PATH/data/logs/pg_install_log.txt" 2>&1
    done

    # Finally, shut the server down since the regular start script will restart it anyway
    nohup pg_ctl --pgdata="$GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY" stop >>"$GEORBIS_PATH/data/logs/pg_install_log.txt" 2>&1
    sleep 5
}
start () {
    # Check if directory has a data folder. If not, it means that the first time 
    # setup for PostGRES and PostGIS has not been performed.
    # In such a case, first perform the installation and then continue with startup
    if [ ! -d "$GEORBIS_PATH/data" ]; then
        setup
    fi

    # Start the PostGres server
    export PGPORT=$GEORBIS_POSTGRES_PORT
    nohup pg_ctl start --pgdata="$GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY" >>"$GEORBIS_PATH/data/logs/pg_run_log.txt" 2>&1 &
    sleep 5

    # Query the server to check if we have a valid status
    startup_status=$(pg_ctl status --pgdata="$GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY") 
    # If server started successfully, we will find the string "server is running"
    # in the status returned above
    if [[ "$startup_status" == *"server is running"* ]]; then
        echo "OK"
    else
        echo "FAILED"    
    fi
}
stop () {
    # Initiate the stop signal on the server - synchronously
    pg_ctl stop --pgdata="$GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY" -m fast >>"$GEORBIS_PATH/data/logs/pg_run_log.txt" 2>&1 &

    sleep 3

    # Query the server to check if we have a valid stop status
    stop_status=$(pg_ctl status --pgdata="$GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY")
    # If server stopped successfully, we will find the string "no server running"
    # in the status returned above
    if [[ "$stop_status" == *"no server running"* ]]; then
        echo "OK"
    else
        echo "FAILED"    
    fi
}
restart () {
    stop
    start
}
get_current_script_dir(){
    # Finds the path to the current script directory

    # Get path to current script file
    current_script_dir="${BASH_SOURCE[0]}"
    while [ -h "$current_script_dir" ]; do # resolve $current_script_dir until the file is no longer a symlink
      current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
      current_script_dir="$(readlink "$current_script_dir")"
      [[ $current_script_dir != /* ]] && current_script_dir="$current_dir/$current_script_dir" # if $current_script_dir was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
    echo "$current_dir"
}

if [ -z "$GEORBIS_PATH" ]; then
    GEORBIS_PATH=$(dirname $(get_current_script_dir))    
    # Invoke the configuration variables
    source "$GEORBIS_PATH/config/configuration.sh"
    # Setup runtime paths using the dependencies shipped in deps folder
    source "$GEORBIS_PATH/bin/runtime_env.sh"
fi

. "$GEORBIS_PATH/bin/util.sh"

# First, we find the path to the nginx server directory
postgres_dir=$(find_deps_folder "postgres" "required")

# See how we were called.
case "$1" in
  start)
        start
    ;;
  stop)
        stop
    ;;
  status)
        status
        ;;
  restart)
    restart
        ;;
  *)
  # Invalid mode:  
    echo "Usage: $0 {start|stop|status|restart}"
    return 1
esac
exit 0