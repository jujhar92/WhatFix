#!/bin/bash

# Finds the path to the current script directory
get_current_script_dir(){
	# Get path to current script file
	current_script_dir="${BASH_SOURCE[0]}"
	while [ -h "$current_script_dir" ]; do # resolve $current_script_dir until the file is no longer a symlink
	  current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
	  current_script_dir="$(readlink "$current_script_dir")"
	  [[ $current_script_dir != /* ]] && current_script_dir="$current_dir/$current_script_dir" # if $current_script_dir was a relative symlink, we need to resolve it relative to the path where the symlink file was located
	done
	current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
	echo "$current_dir"
}

# Finds a specified dependency
# arg1 - name of dependency
# arg2 - set to "required" if we must abort in case folder is not found
find_deps_folder(){
	# Check if the number of arguments is atleast 1
	if [ "$#" -lt 1 ]; then
		echo "Incorrect number of args. Usage: find_deps_folder <dependency_name> [requirement_flag]"
		return 1
	fi
	dependency_name=$1
	# If the second argument is specified, interpret that as the requirement flag	
	if [ "$#" -ge 2 ]; then
		requirement=$2
	fi
	# Check if the specified folder exists in the dependencies path
	dependency_dir=$(find "$GEORBIS_PATH/deps" -maxdepth 1 -type d -name "*$dependency_name*")
	if [ ! -d "$dependency_dir" ]; then
		if [ "$requirement" == "requirement" ]; then
	    	echo "ERROR: Unable to locate $dependency_name dir. Aborting..."
	    	return 1
	    fi
	fi
	echo "$dependency_dir"
}

# Checks if a process is running with the given PID
# Returns "RUNNING" only if the process name is found in the process table
# AND it is running under the specified PID
# arg1 - name of process
# arg2 - process PID
check_process_status(){
	# Check if the number of arguments is 2
	if [ "$#" -ne 2 ]; then
		echo "Incorrect number of args. Usage: check_process_status <process_name> <process_pid>"
		return 1
	fi
	# Search for this PID in the process list
    ps_process=$(ps cax | grep "$2")
    if [[ ! -z "$ps_process" ]]; then
        if [[ "$ps_process" == *"$1"* ]]; then
            echo "RUNNING"
        else
            echo "NOT RUNNING"    
        fi
    else
    	echo "NOT RUNNING"    
    fi    
}

# Checks if a process is running with the given name
# Returns "yes" if the process name is found in the process table; "no" otherwise
# arg1 - name of process
check_if_process_is_running(){
	# Check if the number of arguments is 2
	if [ "$#" -ne 1 ]; then
		printf "\nIncorrect number of args. Usage: check_if_process_is_running <process_name>\n"
		return 1
	fi
	# Search for this PID in the process list
    ps_process=$(ps cax | grep "$1")
    if [[ ! -z "$ps_process" ]]; then
        echo "yes"
    else
        echo "no"    
    fi    
}