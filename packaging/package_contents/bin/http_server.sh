#!/bin/bash

# Starts nginx by setting the port needed to spawn from using an environment variable
# Also re-configures the conf file to launch Georbis services using the specified port number

start () {
    # Check if there is an already running instance of map service
    run_status=$(check_if_process_is_running "nginx")
    if [[ "$run_status" == "yes" ]]; then
        printf "Restarting http server...\n"
        stop
    fi

    # We allow for this proxy server to be configured by third party applications
    # using this so that they can provide access to their server applications
    # We do this by looking in the templates folder and copying the contents
    # of any directtive specified in any .template file there to the current
    # conf template proxy forwarding
    #template_files=$(find "$GEORBIS_PATH/conf/templates/nginx" -f "*.template")
    #find "$current_httpd_dir" -type f -print0 | xargs -0 sed -i "s|/home/balajeerc/Projects/IGVDS_Build_Dir/igvds/deps/httpd-2.4.16|$current_httpd_dir|g"
    #sed -i "s|/home/balajeerc/Projects/IGVDS_Build_Dir/igvds/deps/httpd-2.4.16|$current_httpd_dir|g"

    # Start by making sure that all the required environment variables have been set
    all_vars_found="yes"
    declare -a required_environment_variables=( GEORBIS_MAIN_SERVER_PORT
                                                GEORBIS_SERVICES_BIN_DIR
                                                GEORBIS_WMS_FCGI_SCRIPT
                                                GEORBIS_WMS_FCGI_PORT
                                                GEORBIS_WFS_FCGI_SCRIPT
                                                GEORBIS_WFS_FCGI_PORT
                                                GEORBIS_WPS_FCGI_SCRIPT
                                                GEORBIS_WPS_FCGI_PORT
                                                GEORBIS_TEST_FCGI_SCRIPT
                                                GEORBIS_TEST_FCGI_PORT
                                                GEORBIS_WMS_DATA_DIR )
    declare -A parsed_env_vals 
    for required_env_var in "${required_environment_variables[@]}" ; do
        current_env_val="${!required_env_var}"
        if [ -z "$current_env_val" ]; then
             echo "Environment variable $required_env_var not defined"
             all_vars_found="no"
        else
            parsed_env_vals["$required_env_var"]=$(echo "$current_env_val")
        fi
    done

    if [ "$all_vars_found" = "no" ]; then
        echo "Aborting since the environment variables listed above were not defined"
        return 1
    fi

    # Check if we can find a templated conf file
    template_conf_file=$(find "$GEORBIS_PATH/config" -name "nginx_georbis_conf.template")
    if [ ! -f "$template_conf_file" ]; then
        echo "Unable to find nginx_georbis_conf.template file"
        return 1
    fi

    cp $template_conf_file "$nginx_dir/conf/nginx.conf"

    # Finally, we go one environment variable after another referenced in the
    # conf file and replace them with their actual values
    for required_env_var in "${required_environment_variables[@]}" ; do
        sed -i "s|@@$required_env_var@@|${parsed_env_vals[$required_env_var]}|g" "$nginx_dir/conf/nginx.conf"   
    done

    # Start nginx server
    "$nginx_dir/sbin/nginx" -p "$nginx_dir"

    sleep 2

    # Check to make sure that nginx is running
    status=$(check_if_process_is_running nginx)
    if [[ "$status" == "yes" ]]; then
        echo "OK"  
    else
        echo "FAILED"
    fi
}
stop () {
    # Initiate stop only if the server is not already running
    # Check to make sure that nginx is stopped
    status=$(check_if_process_is_running "nginx")
    if [[ "$status" == "yes" ]]; then
        # Stop nginx server
        "$nginx_dir/sbin/nginx" -p "$nginx_dir" -s stop 
    fi

    sleep 2

    # Check to make sure that nginx is stopped
    status=$(check_if_process_is_running "nginx")
    if [[ "$status" == "no" ]]; then
        echo "OK"  
    else
        echo "FAILED"
    fi    
}
restart () {
    stop
    start
}
get_current_script_dir(){
    # Get path to current script file
    current_script_dir="${BASH_SOURCE[0]}"
    while [ -h "$current_script_dir" ]; do # resolve $current_script_dir until the file is no longer a symlink
      current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
      current_script_dir="$(readlink "$current_script_dir")"
      [[ $current_script_dir != /* ]] && current_script_dir="$current_dir/$current_script_dir" # if $current_script_dir was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
    echo "$current_dir"
}

if [ -z "$GEORBIS_PATH" ]; then
    set -a
    GEORBIS_PATH=$(dirname $(get_current_script_dir))    
    # Invoke the configuration variables
    source "$GEORBIS_PATH/config/configuration.sh"
    # Setup runtime paths using the dependencies shipped in deps folder
    source "$GEORBIS_PATH/bin/runtime_env.sh"
fi

. "$GEORBIS_PATH/bin/util.sh"

# First, we find the path to the nginx server directory
nginx_dir=$(find_deps_folder "nginx" "required")

# Find the georbis services folder
georbis_services_dir=$(find_deps_folder "georbis-services" "required")

export GEORBIS_SERVICES_BIN_DIR="$georbis_services_dir/bin"
export GEORBIS_WMS_FCGI_SCRIPT="georbis-mapservice"
export GEORBIS_WFS_FCGI_SCRIPT="georbis-featureservice"
export GEORBIS_WPS_FCGI_SCRIPT="georbis-processingservice"
export GEORBIS_TEST_FCGI_SCRIPT="georbis-testservice"
export GEORBIS_WMS_DATA_DIR="$GEORBIS_PATH/data/service_data/data/wms"

# See how we were called.
case "$1" in
  start)
        start
    ;;
  stop)
        stop
    ;;
  status)
    status georbis-mapservice
        RETVAL=$?
        ;;
  restart)
    restart
        ;;
  *)
        echo "Usage: $0 {start|stop|status|restart}"
        RETVAL=2
        ;;
esac
exit $RETVAL