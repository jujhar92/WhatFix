#!/bin/bash

# ========= Main server related ========== #

# Main port number on which the server is to be accessed
# This includes all services. Calls made to this port will then
# be forwarded to the appropriate service/server by using
# a reverse proxy. 
# NOTE: This must be the only port which the firewall allows
# access to
GEORBIS_MAIN_SERVER_PORT=19090

# ========= Georbis WebApp related ========= #

# Port on which the GEORBIS Webapp will run
GEORBIS_WEBAPP_PORT=19091

# ========= Georbis Service related ======== #

# Port numbers the the Georbis FCGI application will listen on
GEORBIS_TEST_FCGI_PORT=19096
GEORBIS_WMS_FCGI_PORT=19097
GEORBIS_WFS_FCGI_PORT=19098
GEORBIS_WPS_FCGI_PORT=19099

# ========= PostGRES related ========= #

# IP address for PostGRES database
GEORBIS_POSTGRES_HOST="localhost"

# Username used while accessing PostGRES database
GEORBIS_POSTGRES_USERNAME="georbis"

# Username used while accessing PostGRES database
GEORBIS_POSTGRES_PASSWORD="asdf123"

# PostGres port for system installed PostGres
GEORBIS_POSTGRES_PORT=19095

# Environment variable indicating whether to use the PostGIS/PostGRES
# DB server installed in existing system. If this variable is set to false
# then an embedded PostGres-PostGIS DB server is used
GEORBIS_USE_EMBEDDED_POSTGRES=true

# Directory where all database directory will be stored
# The directory size is expected to grow considerably so it would be
# prudent to set a directory that has sufficient capacity. 
# You can use this option
# NOTE: This option is used only when GEORBIS_USE_EMBEDDED_POSTGRES=true 
GEORBIS_EMBEDDED_POSTGRES_DATA_DIRECTORY=$GEORBIS_PATH/data/postgres_data

# Name of database used to store authorization information
GEORBIS_AUTH_DB_NAME="authdb"

# Name of database used to store GIS data
GEORBIS_GIS_DB_NAME="georbis"

# ====================================== #

# The following variable is for script initialization. Do not modify or remove. 
GEORBIS_CONFIG_CALLED="yes"

