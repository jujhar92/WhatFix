package com.vizexperts.georbis;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;

import com.vizexperts.georbis.db.UserDAO;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;

@Controller
@RestController
public class AuthController {

    @Autowired
    UserDAO authDAO;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("admin/listUsersStartingWith")
    public ModelAndView findUsersStartingWith(@RequestParam(value = "name", required = true) String name) {
        List<AppUser> userList = userRepository.findByUsernameStartingWith(name);
        List<String> nameList = new ArrayList<String>();
        for (AppUser user : userList) {
            nameList.add(user.getName());
        }
        ModelAndView mav = new ModelAndView();
        mav.setViewName("authlist");
        mav.addObject("userlist", nameList);
        return mav;
    }

    @RequestMapping("admin/addUser")
    public ModelAndView addUser(@RequestParam(value = "name", required = true) String name,
                                @RequestParam(value = "password", required = true) String password,
                                @RequestParam(value = "role", required = true) String role,
                                @RequestParam(value = "enabled", required = false, defaultValue = "false") boolean enabled) {
        // First make sure there is no existing user
        ModelAndView modelView = new ModelAndView();
        modelView.setViewName("create_user");
        List<AppUser> existingUsersWithGivenName = userRepository.findByUsername(name);
        if (existingUsersWithGivenName.size() > 0) {
            String errorMsg = "Another user entry already exists with specified name: " + name;
            modelView.addObject("error", errorMsg);
            return modelView;
        }

        AppUser user = new AppUser();
        user.setName(name);
        user.setPassword(password);
        user.setRole(role);

        if(enabled) {
            user.setEnabled(1);
        }else{
            user.setEnabled(0);
        }

        AppUser savedUser = userRepository.save(user);
        assertNotNull(savedUser);
        modelView.addObject("msg", "User added successfully");
        return modelView;
    }

    @RequestMapping("admin/manage_user")
    public ModelAndView showUserAdmin(@RequestParam(value = "mode", required = false, defaultValue = "create") String mode,
                                      @RequestParam(value = "name", required = false, defaultValue = "user") String username) {
        ModelAndView modelView = new ModelAndView();
        modelView.setViewName("create_user");
        return modelView;
    }
}