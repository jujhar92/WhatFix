package com.vizexperts.georbis;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private DataSource authDBDataSource;

    @Autowired
    public void setDataSource(DataSource dataSource){
        authDBDataSource = dataSource;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
            .dataSource(authDBDataSource)
            .usersByUsernameQuery("select username, password, enabled from public.jdbcauth where username=?")
            .authoritiesByUsernameQuery("select username, role from public.jdbcauth where username=?");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/admin/**").access("hasRole('ADMIN')")
            .and()
                .formLogin().loginPage("/login").failureUrl("/login?error")
                .usernameParameter("name").passwordParameter("password")
            .and()
                .logout().logoutSuccessUrl("/login?logout").permitAll()
                .logoutUrl("/j_spring_security_logout").permitAll()
            .and()
                .exceptionHandling().accessDeniedPage("/403")
            .and()
                .csrf();
    }

}