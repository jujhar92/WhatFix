package com.vizexperts.georbis;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<AppUser, Long> {
    List<AppUser> findByUsername(String name);
    List<AppUser> findByUsernameStartingWith(String partialName);
}