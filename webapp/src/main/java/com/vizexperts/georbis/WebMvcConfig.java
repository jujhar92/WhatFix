package com.vizexperts.georbis;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * Created by Balajee on 7/7/2015.
 */
@EnableWebMvc
@Configuration
@ComponentScan
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        super.configureViewResolvers(registry);
        registry.jsp("/WEB-INF/views/", ".jsp");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/data/**").addResourceLocations("/WEB-INF/data/");
        registry.addResourceHandler("/html/**").addResourceLocations("/WEB-INF/html/");
        registry.addResourceHandler("/libs/**").addResourceLocations("/WEB-INF/libs/");
        registry.addResourceHandler("/scripts/**").addResourceLocations("/WEB-INF/scripts/");
        registry.addResourceHandler("/styles/**").addResourceLocations("/WEB-INF/styles/");
    }
}