<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<body>
	<%@include file="logout_header.jsp" %>
	<h2>${title}</h2>
	<h2>${message}</h2>
	<a href="/admin/manage_user">User Administration</a>
</body>
</html>