<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <title>Authorised users</title>
</head>

<body>
<%@include file="logout_header.jsp" %>
<h2>List of Authorised Users</h2>

<table>
  <c:forEach items="${userlist}" var="list">
    <tr>
      <td>
        <c:out value="${list}" />
      </td>
    </tr>
  </c:forEach>
</table>

</body>

</html>