CREATE TABLE IF NOT EXISTS public.jdbcauth
(
    username VARCHAR(32),
    password VARCHAR(32) default NULL,
    role VARCHAR(32) default NULL,
    enabled INT default 0,
    PRIMARY KEY(username)
);

INSERT INTO public.jdbcauth (username, password, role, enabled)
SELECT 'admin', 'admin', 'ADMIN', 1
WHERE
NOT EXISTS (
    SELECT username FROM public.jdbcauth WHERE username = 'admin'
);