include "core.thrift"

namespace cpp GeorbIS.Processing
namespace java com.vizexperts.georbis.processing
namespace py georbis.processing

service ElevationQuery{
    double getElevationAtPointE(1: required core.Point3D point);
    double getElevationAtPointG(1: required core.GeoPoint point);
    list<double> getElevationsAtPointsE(1: required list<core.Point3D> points = []);
    list<double> getElevationsAtPointsG(1: required list<core.GeoPoint> points = []);
}

service MeasurementQuery{
    double getAerielDistanceE(1: required list<core.Point3D> points = []);
    double getAeirelDistanceG(1: required list<core.GeoPoint> points = []);
    double getProjectedDistanceE(1: required list<core.Point3D> points = []);
    double getProjectedDistanceG(1: required list<core.GeoPoint> points = []);
    double getAerielAreaE(1: required list<core.Point3D> points = []);
    double getAerielAreaG(1: required list<core.GeoPoint> points = []);
    double getProjectedAreaE(1: required list<core.Point3D> points = []);
    double getProjectedAreaG(1: required list<core.GeoPoint> points = []);
}

service MinMaxElevationQuery{
    list<core.Point3D> getMinMaxElevationE(1: required list<core.Point3D> points = []);
    list<core.GeoPoint> getMinMaxElevationG(1: required list<core.GeoPoint> points = []);
}

service ElevationDifferenceQuery{
    double getElevationDifference(1: required list<core.GeoPoint> points = []);
}

service AngleSlopeQuery{
    double getAngleBetweenPoints(1: required core.GeoPoint point1, 2: required core.GeoPoint point2);
    double getSlopeBetweenPoints(1: required core.GeoPoint point1, 2: required core.GeoPoint point2);
}

service WorldManager{
    bool addNewWorld();
    void removeCurrentWorld();
    bool addRasterLayer(1: required string url);
    bool addElevationLayer(1: required string url);
    bool addVectorLayer(1: required string url);
    bool removeLayer(1: required string url);
    void removeAllLayer(); 
}
