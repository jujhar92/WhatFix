namespace cpp GeorbIS.Core
namespace java com.vizexperts.georbis.core
namespace py georbis.core

struct Point4D{
    1: required double x;
    2: required double y;
    3: required double z;
    4: required double w;
}

struct Point3D{
    1: required double x;
    2: required double y;
    3: required double z;
}

struct Point2D{
    1: required double x;
    2: required double y;
}

struct GeoPoint{
    1: required double latitude;
    2: required double longitude;
    3: required double altitude;
}

