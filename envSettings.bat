:: Set the path to the GEORBIS_SERVER_DEPENDENCIES_DIRECTORY
:: Get it from: 192.168.1.4:18080/svn/tree/trunk/Products/
set GEORBIS_SERVER_DEPENDENCIES_DIRECTORY=E:\Projects\ADE-GIS\GeorbISServerDependencies

:: Typically, you don't have to edit below this line
:: ==================================================

:: Set the path to the QGIS runtime folder below
set QGIS_PATH=%GEORBIS_SERVER_DEPENDENCIES_DIRECTORY%\qgis_runtime

:: Set the path to where the thrift compiler can be found
set THRIFT_PATH=%GEORBIS_SERVER_DEPENDENCIES_DIRECTORY%\thrift
set THRIFT_LIB_PATH=%THRIFT_PATH%\lib\java
set THRIFT_LIB_DEPS_PATH=%THRIFT_PATH%\lib\java\lib

:: Set the path to where the Maven build tool can be found
set MAVEN_PATH=%GEORBIS_SERVER_DEPENDENCIES_DIRECTORY%\apache-maven

:: Set the path to where the Gradle build tool can be found
set GRADLE_PATH=%GEORBIS_SERVER_DEPENDENCIES_DIRECTORY%\gradle

:: Set the path to the Geoserver folder
set GEOSERVER_PATH=%GEORBIS_SERVER_DEPENDENCIES_DIRECTORY%\GeoServer
set GEOSERVER_JAR_PATH=%GEOSERVER_PATH%\webapps\geoserver\WEB-INF\lib