
:: Set your /path/to/consulting/OSS directory here
set OSSROOT=F:/GGN_consulting/consulting/OSS

:: Set build options   debug|release
set OSSBUILD=debug

:: Set build Architecture   x86|x64
set OSSARCH=x64

:: Set your Path to terrain kit SDK
set TERRAINSDK_DIR=F:/VizSim

:: Set Portico directory
set PORTICO_DIR=C:/Program Files/Portico/portico-2.0.0

:: Set PostgreSQL directory
set POSTGRESQL_DIR=C:/Program Files/PostgreSQL/9.3