#!/bin/bash
export OSSROOT=/root/Desktop/Consulting_VREALITY/OSS
echo $OSSROOT
#Set PlatForm Options
export OSSBUILD=release
echo $OSSBUILD
export OSSARCH=x64
echo $OSSARCH
export TERRAINSDK_DIR=/root/Teamcity/TKP_SDK/trunk
export VRPLATFORM_DIR=/root/Teamcity/Vreality
buildServer=BLR
. ./vrenv.sh release x64

# Generate files from scratch
rm -f CMakeCache.txt
cmake -G "Unix Makefiles" -DUSE_NAMESPACE=viz .

make -j4
cd Package/Linux
# Need to remove .rpmmacros from home dir as it is added by VizDisplay buid
# in Linux build.  VRPlatform works without .rpmmacros
rm -f ~/.rpmmacros
chmod a+x ./makeVREALITYDeps.sh
./makeVREALITYDeps.sh
chmod a+x ./makeVREALITY.sh
./makeVREALITY.sh
chmod a+x ./makeVRScheduler.sh
./makeVRScheduler.sh