# Locate Xerces-c
# This module defines
# XERCES_LIBRARY
# XERCES_FOUND, if false, do not try to link to xerces-c
# XERCES_INCLUDE_DIR, where to find the headers
#
# $XERCES_DIR is an environment variable that would
# correspond to the ./configure --prefix=$XERCES_DIR
#
# Created by Robert Osfield.

FIND_PATH(XERCES_INCLUDE_DIR xercesc
    $ENV{XERCESC_INC}
)

MACRO(FIND_XERCES_LIBRARY MYLIBRARY MYLIBRARYNAME)

FIND_LIBRARY(${MYLIBRARY}
    NAMES ${MYLIBRARYNAME}
    PATHS
    ${XERCESC_DIR}/lib
    $ENV{XERCESC_DIR}/lib
    ${XERCESC_DIR}/lib/$ENV{OSSARCH}
    $ENV{XERCESC_DIR}/lib/$ENV{OSSARCH}
)

ENDMACRO(FIND_XERCES_LIBRARY MYLIBRARY MYLIBRARYNAME)

SET(XERCES_LIST Xerces xerces-c_3 xerces-c_3D xerces-c)
FIND_XERCES_LIBRARY(XERCES_LIBRARY "${XERCES_LIST}")
FIND_XERCES_LIBRARY(XERCES_LIBRARY_DEBUG xerces-c_3D libxerces-c.so)

SET(XERCES_FOUND "NO")
IF(XERCES_LIBRARY AND XERCES_INCLUDE_DIR)
    SET(XERCES_FOUND "YES")
ENDIF(XERCES_LIBRARY AND XERCES_INCLUDE_DIR)
