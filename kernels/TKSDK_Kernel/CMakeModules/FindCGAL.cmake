# Locate boost
# This module defines
# CGAL_INCLUDE_DIR, where to find the headers
#
# $CGAL_DIR is an environment variable that would
# correspond to the path where CGAL library is located
#
# Created by Udit

FIND_PATH(CGAL_INCLUDE_DIR CGAL/CORE/CORE.h
            ${CGAL_DIR}/include
            $ENV{CGAL_DIR}/include/$ENV{OSSBUILD}
            ${CGAL_INC}
            $ENV{CGAL_INC})

MACRO(FIND_CGAL_LIBRARY MYLIBRARY MYLIBRARYNAME)

    FIND_LIBRARY(${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
        $ENV{CGAL_LIB}
		$ENV{CGAL_DIR}/lib/$ENV{OSSARCH}
        $ENV{CGAL_DIR}/lib/$ENV{OSSARCH}/debug
        $ENV{CGAL_DIR}/lib/$ENV{OSSARCH}/release
        )

ENDMACRO(FIND_CGAL_LIBRARY LIBRARY LIBRARYNAME)

# optimized
IF (WIN32)
    FIND_CGAL_LIBRARY(CGAL_LIBRARY             CGAL-vc90-mt-4.0)
    FIND_CGAL_LIBRARY(CGAL_LIBRARY_DEBUG       CGAL-vc90-mt-gd-4.0)
    FIND_CGAL_LIBRARY(CGAL_IMAGEIO_LIBRARY           CGAL_ImageIO-vc90-mt-4.0)
    FIND_CGAL_LIBRARY(CGAL_IMAGEIO_LIBRARY_DEBUG     CGAL_ImageIO-vc90-mt-gd-4.0)
    FIND_CGAL_LIBRARY(CGAL_QT_LIBRARY         CGAL_Qt4-vc90-mt-4.0)
    FIND_CGAL_LIBRARY(CGAL_QT_LIBRARY_DEBUG   CGAL_Qt4-vc90-mt-gd-4.0)
    FIND_CGAL_LIBRARY(CGAL_CORE_LIBRARY             CGAL_Core-vc90-mt-4.0)
    FIND_CGAL_LIBRARY(CGAL_CORE_LIBRARY_DEBUG       CGAL_Core-vc90-mt-gd-4.0)
ELSE (WIN32)
    
ENDIF (WIN32)

SET(CGAL_FOUND "NO")
IF(CGAL_LIBRARY AND CGAL_INCLUDE_DIR)
    SET(CGAL_FOUND "YES")
ENDIF(CGAL_LIBRARY AND CGAL_INCLUDE_DIR)
