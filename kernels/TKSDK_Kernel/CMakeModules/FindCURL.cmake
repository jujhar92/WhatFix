# Locate CURL
# This module defines
# CURL_LIBRARY
# CURL_FOUND, if false, do not try to link to CURL
# CURL_INCLUDE_DIR, where to find the headers
#
# $CURL_DIR is an environment variable that would
# correspond to the ./configure --prefix=$CURL_DIR
#
# Created by Robert Osfield.

FIND_PATH(CURL_INCLUDE_DIR curl/curl.h
	${CURL_INC}
	${CURL_DIR}/inc
    ${CURL_DIR}/include
    
	$ENV{CURL_INC}
	$ENV{CURL_DIR}/inc
    $ENV{CURL_DIR}/include
)

MACRO(FIND_CURL_LIBRARY MYLIBRARY MYLIBRARYNAME)

    FIND_LIBRARY(${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
        ${CURL_LIB}
	$ENV{CURL_LIB}

	${CURL_DIR}/lib/$ENV{OSSARCH}/debug
	${CURL_DIR}/lib/$ENV{OSSARCH}/release
		
	${CURL_DIR}/lib/${OSSBUILD}
	${CURL_DIR}/lib/
	
	$ENV{CURL_LIB}
	$ENV{CURL_DIR}/lib/$ENV{OSSARCH}/debug
	$ENV{CURL_DIR}/lib/$ENV{OSSARCH}/release
	$ENV{CURL_DIR}/lib/lib/${OSSBUILD}
	$ENV{CURL_DIR}/lib
    )

ENDMACRO(FIND_CURL_LIBRARY LIBRARY LIBRARYNAME)

IF (WIN32)

# Find release (optimized) libs
FIND_CURL_LIBRARY(CURL_LIBRARY libcurl_imp)

# Find debug libs
FIND_CURL_LIBRARY(CURL_LIBRARY_DEBUG libcurld_imp)
ELSE (WIN32)
# Find release (optimized) libs
FIND_CURL_LIBRARY(CURL_LIBRARY curl)

# Find debug libs
FIND_CURL_LIBRARY(CURL_LIBRARY_DEBUG curl)
ENDIF (WIN32)

SET(CURL_FOUND "NO")
IF(CURL_LIBRARY AND CURL_INCLUDE_DIR)
    SET(CURL_FOUND "YES")
ENDIF(CURL_LIBRARY AND CURL_INCLUDE_DIR)
