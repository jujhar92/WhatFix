# Locate GDAL
# This module defines
# GDAL_LIBRARY
# GDAL_FOUND, if false, do not try to link to GDAL
# GDAL_INCLUDE_DIR, where to find the headers
#
# $GDAL_DIR is an environment variable that would
# correspond to the ./configure --prefix=$GDAL_DIR
#
# Created by Robert Osfield.

FIND_PATH(GDAL_INCLUDE_DIR gdal.h
    $ENV{GDAL_INC}
    ${GDAL_DIR}/include
    ${GDAL_DIR}/inc
    $ENV{GDAL_DIR}/inc
    $ENV{GDAL_DIR}/include
	$ENV{GDAL_DIR}/include/$ENV{OSSARCH}
    $ENV{GDAL_INC}
)

MACRO(FIND_GDAL_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIBRARY(${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
		$ENV{GDAL_DIR}/lib/$ENV{OSSARCH}/${BUILD}
		NO_DEFAULT_PATH
		)
		
ENDMACRO(FIND_GDAL_LIBRARY LIBRARY LIBRARYNAME BUILD)
	
# Find release (optimized) libs
IF (WIN32)
    FIND_GDAL_LIBRARY(GDAL_LIBRARY gdal_i release)
    FIND_GDAL_LIBRARY(GDAL_LIBRARY_DEBUG gdal_i debug)
ELSE (WIN32)
    IF (UNIX)
        FIND_GDAL_LIBRARY(GDAL_LIBRARY gdal release)
        FIND_GDAL_LIBRARY(GDAL_LIBRARY_DEBUG gdal debug)
    ENDIF (UNIX)
ENDIF (WIN32)


SET(GDAL_FOUND "NO")
IF(GDAL_LIBRARY AND GDAL_INCLUDE_DIR)
    SET(GDAL_FOUND "YES")
ENDIF(GDAL_LIBRARY AND GDAL_INCLUDE_DIR)
