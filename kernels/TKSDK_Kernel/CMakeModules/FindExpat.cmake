# Locate gdal
# This module defines
# EXPAT_LIBRARY
# EXPAT_FOUND, if false, do not try to link to gdal
# EXPAT_INCLUDE_DIR, where to find the headers
#
# $EXPAT_DIR is an environment variable that would
# correspond to the ./configure --prefix=$EXPAT_DIR
#
# Created by Robert Osfield.

FIND_PATH(EXPAT_INCLUDE_DIR expat.h
            ${DELTA_DIR}/inc
            ${EXPAT_DIR}/inc
            $ENV{DELTA_DIR}/inc
            $ENV{EXPAT_DIR}/inc
            $ENV{OSSROOT}/libpq/include
            ${DELTA_DIR}/include
            ${EXPAT_DIR}/include
            $ENV{DELTA_DIR}/include
            $ENV{EXPAT_DIR}/include
			)
			
MACRO(FIND_EXPAT_LIBRARY MYLIBRARY MYLIBRARYNAME BUILD)

    FIND_LIBRARY(${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
	$ENV{EXPAT_DIR}/lib/$ENV{OSSARCH}/${BUILD}
	$ENV{EXPAT_LIB}
	${EXPAT_LIB}
	$ENV{EXPAT_DIR}/lib
	NO_DEFAULT_PATH
	)
		
ENDMACRO(FIND_EXPAT_LIBRARY LIBRARY LIBRARYNAME BUILD)

# optimized
IF (WIN32)
    FIND_EXPAT_LIBRARY(EXPAT_LIBRARY libexpat release)
    FIND_EXPAT_LIBRARY(EXPAT_LIBRARY_DEBUG libexpat debug)
ELSE (WIN32)
    FIND_EXPAT_LIBRARY(EXPAT_LIBRARY expat release)
    FIND_EXPAT_LIBRARY(EXPAT_LIBRARY_DEBUG expat debug)
ENDIF (WIN32)

SET(EXPAT_FOUND "NO")
IF(EXPAT_LIBRARY AND EXPAT_INCLUDE_DIR)
    SET(EXPAT_FOUND "YES")
ENDIF(EXPAT_LIBRARY AND EXPAT_INCLUDE_DIR)
