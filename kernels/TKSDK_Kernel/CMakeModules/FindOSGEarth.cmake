# Locate OSG
# This module defines
# OSG_EARTH_LIBRARY
# OSG_EARTH_FOUND, if false, do not try to link to OSG
# OSG_EARTH_INCLUDE_DIR, where to find the headers
#
# OSGEARTH_DIR is an environment variable that would
# correspond to the ./configure --prefix=$OSGEARTH_DIR
#
# Created by Vaibhav Bansal

###### headers ######
FIND_PATH(OSG_EARTH_INCLUDE_DIR osgEarth/Map    
    ${OSGEARTH_DIR}/include
    ${OSGEARTH_DIR}/inc
    $ENV{OSGEARTH_DIR}/inc
    $ENV{OSGEARTH_DIR}/src
    $ENV{OSGEARTH_DIR}/include
    $ENV{OSGEARTH_INC}
    )


###### libraries ######
MACRO(FIND_OSG_EARTH_LIBRARY MYLIBRARY MYLIBRARYNAME)
	
    FIND_LIBRARY(${MYLIBRARY}
					NAMES ${MYLIBRARYNAME}
					PATHS
					
			$ENV{OSGEARTH_LIB}					
			$ENV{OSGEARTH_LIB}/../debug					
			$ENV{OSGEARTH_LIB}/../release					
    )
        
ENDMACRO(FIND_OSG_EARTH_LIBRARY LIBRARY LIBRARYNAME)

MACRO(FIND_OSG_EARTH_PLUGIN MYLIBRARY MYLIBRARYNAME)

	FIND_FILE(${MYLIBRARY}
			${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
        $ENV{OSGEARTH_LIB}
        $ENV{OSGEARTH_DIR}/lib
        $ENV{OSGEARTH_DIR}/lib/$ENV{OSSARCH}/Release
        $ENV{OSGEARTH_DIR}/lib/$ENV{OSSARCH}/Debug  
	)

ENDMACRO(FIND_OSG_EARTH_PLUGIN LIBRARY LIBRARYNAME)

FIND_OSG_EARTH_LIBRARY(OSG_EARTH_LIBRARY	osgEarth)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_UTIL_LIBRARY	osgEarthUtil)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_ANNOTATION_LIBRARY	osgEarthAnnotation)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_SYMBOLOGY_LIBRARY	osgEarthSymbology)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_FEATURES_LIBRARY	osgEarthFeatures)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_WEBVIEW_LIBRARY	osgEarthWebView)

FIND_OSG_EARTH_LIBRARY(OSG_EARTH_LIBRARY_DEBUG	osgEarthd)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_UTIL_LIBRARY_DEBUG	osgEarthUtild)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_ANNOTATION_LIBRARY_DEBUG	osgEarthAnnotationd)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_SYMBOLOGY_LIBRARY_DEBUG	osgEarthSymbologyd)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_FEATURES_LIBRARY_DEBUG	osgEarthFeaturesd)
FIND_OSG_EARTH_LIBRARY(OSG_EARTH_WEBVIEW_LIBRARY_DEBUG	osgEarthWebViewd)

IF(UNIX)
#FIND_OSG_EARTH_PLUGIN(OSG_EARTH_ENGINE_PLUGIN_LIBRARY	osgdb_osgearth_engine_osgterrain)
ENDIF(UNIX)

SET( OSG_EARTH_FOUND "NO" )
IF( OSG_EARTH_LIBRARY AND OSG_EARTH_INCLUDE_DIR )
    SET( OSG_EARTH_FOUND "YES" )
ENDIF( OSG_EARTH_LIBRARY AND OSG_EARTH_INCLUDE_DIR )


