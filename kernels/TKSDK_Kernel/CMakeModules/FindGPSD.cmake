# Locate gdal
# This module defines
# GPSD_LIBRARY
# GPSD_FOUND, if false, do not try to link to gdal
# GPSD_INCLUDE_DIR, where to find the headers
#
# $GPSD_DIR is an environment variable that would
# correspond to the ./configure --prefix=$GPSD_DIR
#
# Created by Robert Osfield.

FIND_PATH(GPSD_INCLUDE_DIR viz_types.h
            $ENV{GPSD_INC}
            )

MACRO(FIND_GPSD_LIBRARY MYLIBRARY MYLIBRARYNAME)

    FIND_LIBRARY("${MYLIBRARY}_DEBUG"
        NAMES "${MYLIBRARYNAME}${CMAKE_DEBUG_POSTFIX}"
        PATHS        
        $ENV{GPSD_DIR}/lib/$ENV{OSSARCH}/Debug
        )

    FIND_LIBRARY(${MYLIBRARY} NAMES ${MYLIBRARYNAME} PATHS 
                $ENV{GPSD_LIB}
                )
                
    IF( NOT ${MYLIBRARY}_DEBUG)
        IF(MYLIBRARY)
            SET(${MYLIBRARY}_DEBUG ${MYLIBRARY})
        ENDIF(MYLIBRARY)
    ENDIF( NOT ${MYLIBRARY}_DEBUG)

ENDMACRO(FIND_GPSD_LIBRARY LIBRARY LIBRARYNAME)

# optimized
FIND_GPSD_LIBRARY(GPSD_LIBRARY libgps)

SET(GPSD_FOUND "NO")
IF(GPSD_LIBRARY AND GPSD_INCLUDE_DIR)
    SET(GPSD_FOUND "YES")
ENDIF(GPSD_LIBRARY AND GPSD_INCLUDE_DIR)
