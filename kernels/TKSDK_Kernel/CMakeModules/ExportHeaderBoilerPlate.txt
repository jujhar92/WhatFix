/****************************************************************************      
 *
 *
 *  Copyright 2010-2013, VizExpertsIndia Pvt. Ltd. (unpublished)
 *
 *  All rights reserved. This notice is intended as a precaution against
 *  inadvertent publication and does not imply publication or any waiver
 *  of confidentiality. The year included in the foregoing notice is the
 *  year of creation of the work. No part of this work may be used,
 *  reproduced, or transmitted in any form or by any means without the prior
 *  written permission of Vizexperts India Pvt Ltd.
 *
 *
 ***************************************************************************
 */

/// \file @@LIBNAME@@/export.h

#ifndef @@LIBNAMEINCAPS@@_DLL_EXPORT_H
#define @@LIBNAMEINCAPS@@_DLL_EXPORT_H

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#   if defined(@@LIBNAMEINCAPS@@_LIBRARY)
#      define @@LIBNAMEINCAPS@@_DLL_EXPORT __declspec(dllexport)
#   else
#      define @@LIBNAMEINCAPS@@_DLL_EXPORT __declspec(dllimport)
#   endif 
#else
#   if defined(@@LIBNAMEINCAPS@@_LIBRARY)
#      define @@LIBNAMEINCAPS@@_DLL_EXPORT __attribute__ ((visibility("default")))
#   else
#      define @@LIBNAMEINCAPS@@_DLL_EXPORT
#   endif 
#endif

#endif // @@LIBNAMEINCAPS@@_DLL_EXPORT_H