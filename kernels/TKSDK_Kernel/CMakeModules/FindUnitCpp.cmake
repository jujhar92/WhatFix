# Locate CPP_UNIT
# This module defines
# UNIT_CPP_LIBRARY
# UNIT_CPP_FOUND, if false, do not try to link to gdal
# UNIT_CPP_INCLUDE_DIR, where to find the headers
#
# $UNIT_CPP_DIR is an environment variable that would
# correspond to the ./configure --prefix=$UNIT_CPP_DIR
#
# Created by Nishant

FIND_PATH(UNIT_CPP_INCLUDE_DIR cppunit/TestFixture.h
            ${DELTA_DIR}/inc
            ${UNIT_CPP_DIR}/inc
            $ENV{DELTA_DIR}/inc
            $ENV{UNIT_CPP_DIR}/inc
            ${DELTA_DIR}/include
            ${UNIT_CPP_DIR}/include
            $ENV{DELTA_DIR}/include
            $ENV{UNIT_CPP_DIR}/include)

MACRO(FIND_UNIT_CPP_LIBRARY MYLIBRARY MYLIBRARYNAME)

    FIND_LIBRARY("${MYLIBRARY}_DEBUG"
        NAMES "${MYLIBRARYNAME}${CMAKE_DEBUG_POSTFIX}"
        PATHS
        ${DELTA_DIR}/lib
        ${UNIT_CPP_DIR}/lib
        $ENV{DELTA_DIR}/lib
        $ENV{UNIT_CPP_DIR}/lib
        ${DELTA_DIR}/lib/$ENV{OSSARCH}/Debug
        ${UNIT_CPP_DIR}/lib/$ENV{OSSARCH}/Debug
        ${UNIT_CPP_DIR}/lib/$ENV{OSSARCH}
        $ENV{DELTA_DIR}/lib/$ENV{OSSARCH}/Debug
        $ENV{UNIT_CPP_DIR}/lib/$ENV{OSSARCH}/Debug
        $ENV{UNIT_CPP_DIR}/lib/$ENV{OSSARCH})

    FIND_LIBRARY(${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
        ${DELTA_DIR}/lib
        ${UNIT_CPP_DIR}/lib
        $ENV{DELTA_DIR}/lib
        $ENV{UNIT_CPP_DIR}/lib
        ${DELTA_DIR}/lib/$ENV{OSSARCH}/Release
        ${UNIT_CPP_DIR}/lib/$ENV{OSSARCH}/Release
        ${UNIT_CPP_DIR}/lib/$ENV{OSSARCH}
        $ENV{DELTA_DIR}/lib/$ENV{OSSARCH}/Release
        $ENV{UNIT_CPP_DIR}/lib/$ENV{OSSARCH}/Release
        $ENV{UNIT_CPP_DIR}/lib/$ENV{OSSARCH})
    IF( NOT ${MYLIBRARY}_DEBUG)
        IF(MYLIBRARY)
            SET(${MYLIBRARY}_DEBUG ${MYLIBRARY})
        ENDIF(MYLIBRARY)
    ENDIF( NOT ${MYLIBRARY}_DEBUG)

ENDMACRO(FIND_UNIT_CPP_LIBRARY LIBRARY LIBRARYNAME)

# optimized
FIND_UNIT_CPP_LIBRARY(UNIT_CPP_LIBRARY cppunit)

SET(UNIT_CPP_FOUND "NO")
IF(UNIT_CPP_LIBRARY AND UNIT_CPP_INCLUDE_DIR)
    SET(UNIT_CPP_FOUND "YES")
ENDIF(UNIT_CPP_LIBRARY AND UNIT_CPP_INCLUDE_DIR)