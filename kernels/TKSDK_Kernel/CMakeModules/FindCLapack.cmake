# Locate clapack
# This module defines
# CLAPACK_LIBRARY
# CLAPACK_FOUND, if false, do not try to link to gdal
# CLAPACK_INCLUDE_DIR, where to find the headers
#
# $CLAPACK_DIR is an environment variable that would
# correspond to the ./configure --prefix=$CLAPACK_DIR
#
# Created by Kumar Saurabh

FIND_PATH(CLAPACK_INCLUDE_DIR clapack.h
            ${CLAPACK_DIR}/include
            $ENV{CLAPACK_DIR}/include/$ENV{OSSBUILD}
            $ENV{CLAPACK_INC}
            ${CLAPACK_DIR}/include
            $ENV{CLAPACK_DIR}/include)

MACRO(FIND_CLAPACK_LIBRARY MYLIBRARY MYLIBRARYNAME)

    FIND_LIBRARY(${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
        ${CLAPACK_DIR}/lib
        $ENV{CLAPACK_DIR}/lib
        ${CLAPACK_DIR}/lib/$ENV{OSSARCH}/release
	${CLAPACK_DIR}/lib/$ENV{OSSARCH}/debug
        ${CLAPACK_DIR}/lib/$ENV{OSSARCH}
        $ENV{CLAPACK_DIR}/lib/$ENV{OSSARCH}/release
	$ENV{CLAPACK_DIR}/lib/$ENV{OSSARCH}/debug
        $ENV{CLAPACK_DIR}/lib/$ENV{OSSARCH})

ENDMACRO(FIND_CLAPACK_LIBRARY LIBRARY LIBRARYNAME)

# Find release (optimized) libs
FIND_CLAPACK_LIBRARY(CLAPACK_LIBRARY lapack)
FIND_CLAPACK_LIBRARY(CLAPACK_BLAS_LIBRARY blas)
IF (WIN32)
    FIND_CLAPACK_LIBRARY(CLAPACK_F2C_LIBRARY libf2c)
ELSE (WIN32)
    FIND_CLAPACK_LIBRARY(CLAPACK_F2C_LIBRARY f2c)
ENDIF (WIN32)


# Find debug libsind	
FIND_CLAPACK_LIBRARY(CLAPACK_LIBRARY_DEBUG lapackd)
FIND_CLAPACK_LIBRARY(CLAPACK_F2C_LIBRARY_DEBUG libf2cd)
FIND_CLAPACK_LIBRARY(CLAPACK_BLAS_LIBRARY_DEBUG blasd)


SET(CLAPACK_FOUND "NO")
IF(CLAPACK_LIBRARY AND CLAPACK_INCLUDE_DIR)
    SET(CLAPACK_FOUND "YES")
ENDIF(CLAPACK_LIBRARY AND CLAPACK_INCLUDE_DIR)