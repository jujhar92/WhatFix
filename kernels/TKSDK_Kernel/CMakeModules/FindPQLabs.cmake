# Locate POSTGRESQL
# This module defines
# POSTGRESQL_LIBRARY
# POSTGRESQL_FOUND, if false, do not try to link to POSTGRESQL
# POSTGRESQL_INCLUDE_DIR, where to find the headers
#
# $POSTGRESQL_DIR is an environment variable that would
# correspond to the ./configure --prefix=$POSTGRESQL_DIR
#
# Created by Robert Osfield.

FIND_PATH(PQLabs_INCLUDE_DIR PQMTClient.h
    ${PQLABS_DIR}/include
    ${PQLABS_DIR}/inc
    $ENV{PQLABS_DIR}/inc
    $ENV{PQLABS_DIR}/include
)

MACRO(FIND_PQLABS_LIBRARY MYLIBRARY MYLIBRARYNAME)

    FIND_LIBRARY(${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
		${PQLABS_DIR}/lib/$ENV{OSSARCH}
        ${PQLABS_DIR}/lib
        $ENV{PQLABS_DIR}/lib/$ENV{OSSARCH}
        $ENV{PQLABS_DIR}/lib
    )

ENDMACRO(FIND_PQLABS_LIBRARY LIBRARY LIBRARYNAME)

# Find release (optimized) libs
FIND_PQLABS_LIBRARY(PQLABS_LIBRARY PQMTClient)

SET(POSTGRESQL_FOUND "NO")
IF(POSTGRESQL_LIBRARY AND POSTGRESQL_INCLUDE_DIR)
    SET(POSTGRESQL_FOUND "YES")
ENDIF(POSTGRESQL_LIBRARY AND POSTGRESQL_INCLUDE_DIR)
