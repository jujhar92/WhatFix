FIND_PATH(QT_CUSTOM_INCLUDE_DIR QtCore/QObject
    $ENV{QT_INC}
    ${QT_DIR}/include
    ${QT_DIR}/inc
    $ENV{QT_DIR}/inc
    $ENV{QT_DIR}/include
    NO_DEFAULT_PATH
)

FIND_PROGRAM(QT_MOC_EXECUTABLE "$ENV{QT_BIN}/moc")

###### libraries ######
MACRO(FIND_QT_LIBRARY MYLIBRARY MYLIBRARYNAME SUF)

    FIND_LIBRARY("${MYLIBRARY}_DEBUG"
        NAMES "${MYLIBRARYNAME}${CMAKE_DEBUG_POSTFIX}${SUF}"
        PATHS
        $ENV{QT_LIB}
        ${QT_DIR}/lib
        $ENV{QT_DIR}/lib
        ${QT_DIR}/lib/$ENV{OSSARCH}
        $ENV{QT_DIR}/lib/$ENV{OSSARCH}
        )

    FIND_LIBRARY(${MYLIBRARY}
        NAMES "${MYLIBRARYNAME}${SUF}"
        PATHS
        $ENV{QT_LIB}
        ${QT_DIR}/lib
        $ENV{QT_DIR}/lib
        ${QT_DIR}/lib/$ENV{OSSARCH}
        $ENV{QT_DIR}/lib/$ENV{OSSARCH}
        )

    IF( NOT ${MYLIBRARY}_DEBUG)
        IF(MYLIBRARY)
            SET(${MYLIBRARY}_DEBUG ${MYLIBRARY})
        ENDIF(MYLIBRARY)
    ENDIF( NOT ${MYLIBRARY}_DEBUG)

ENDMACRO(FIND_QT_LIBRARY LIBRARY LIBRARYNAME)

###### libraries ######
MACRO(FIND_QT_LIBRARY_WOPOSTFIX MYLIBRARY MYLIBRARYNAME)

    FIND_LIBRARY("${MYLIBRARY}_DEBUG"
        NAMES "${MYLIBRARYNAME}${CMAKE_DEBUG_POSTFIX}"
        PATHS
        $ENV{QT_LIB}
	${QT_LIB}
        ${QT_DIR}/lib
        $ENV{QT_DIR}/lib
        ${QT_DIR}/lib/$ENV{OSSARCH}
        $ENV{QT_DIR}/lib/$ENV{OSSARCH}
        )

    FIND_LIBRARY(${MYLIBRARY}
        NAMES "${MYLIBRARYNAME}"
        PATHS
        $ENV{QT_LIB}
	${QT_LIB}
        ${QT_DIR}/lib
        $ENV{QT_DIR}/lib
        ${QT_DIR}/lib/$ENV{OSSARCH}
        $ENV{QT_DIR}/lib/$ENV{OSSARCH}
        )

    IF( NOT ${MYLIBRARY}_DEBUG)
        IF(MYLIBRARY)
            SET(${MYLIBRARY}_DEBUG ${MYLIBRARY})
        ENDIF(MYLIBRARY)
    ENDIF( NOT ${MYLIBRARY}_DEBUG)

ENDMACRO(FIND_QT_LIBRARY_WOPOSTFIX LIBRARY LIBRARYNAME)

IF (WIN32)
if(USE_QT4)
    FIND_QT_LIBRARY(QT_CORE_LIBRARY QtCore 4)
    FIND_QT_LIBRARY(QT_GUI_LIBRARY QtGui 4)
    FIND_QT_LIBRARY(QT_OPENGL_LIBRARY QtOpenGL 4)
    FIND_QT_LIBRARY(QT_TEST_LIBRARY QtTest 4)
    FIND_QT_LIBRARY(QT_NETWORK_LIBRARY QtNetwork 4)
	FIND_QT_LIBRARY(QT_DECLARATIVE_LIBRARY QtDeclarative 4)
	FIND_QT_LIBRARY(QT_PHONON_LIBRARY phonon 4)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_UITOOLS_LIBRARY QtUiTools)
	FIND_QT_LIBRARY(QT_WEBKIT_LIBRARY QtWebKit 4)
else(USE_QT4)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_CORE_LIBRARY Qt5Core)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_GUI_LIBRARY Qt5Gui)
	FIND_QT_LIBRARY_WOPOSTFIX(QT_WIDGET_LIBRARY Qt5Widgets)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_OPENGL_LIBRARY Qt5OpenGL)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_TEST_LIBRARY Qt5Test)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_NETWORK_LIBRARY Qt5Network)
	FIND_QT_LIBRARY_WOPOSTFIX(QT_DECLARATIVE_LIBRARY Qt5Declarative)
	FIND_QT_LIBRARY_WOPOSTFIX(QT_QUICK_LIBRARY Qt5Quick)
	FIND_QT_LIBRARY_WOPOSTFIX(QT_QML_LIBRARY Qt5Qml)
	#FIND_QT_LIBRARY(QT_PHONON_LIBRARY phonon 4)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_UITOOLS_LIBRARY Qt5UiTools)
endif(USE_QT4)
ELSE (WIN32)

    FIND_QT_LIBRARY_WOPOSTFIX(QT_CORE_LIBRARY Qt5Core)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_GUI_LIBRARY Qt5Gui)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_WIDGET_LIBRARY Qt5Widgets)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_OPENGL_LIBRARY Qt5OpenGL)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_TEST_LIBRARY Qt5Test)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_NETWORK_LIBRARY Qt5Network)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_DECLARATIVE_LIBRARY Qt5Declarative)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_QUICK_LIBRARY Qt5Quick)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_QML_LIBRARY Qt5Qml)
   #FIND_QT_LIBRARY(QT_PHONON_LIBRARY phonon 4)
    FIND_QT_LIBRARY_WOPOSTFIX(QT_UITOOLS_LIBRARY Qt5UiTools)

ENDIF (WIN32)

SET( QT_FOUND "NO" )
IF( QT_CORE_LIBRARY AND QT_GUI_LIBRARY AND QT_UITOOLS_LIBRARY)
    SET( QT_FOUND "YES" )
ENDIF( QT_CORE_LIBRARY AND QT_GUI_LIBRARY AND QT_UITOOLS_LIBRARY)
