# Locate gdal
# This module defines
# GPSBABEL_LIBRARY
# GPSBABEL_FOUND, if false, do not try to link to gdal
# GPSBABEL_INCLUDE_DIR, where to find the headers
#
# $GPSBABEL_DIR is an environment variable that would
# correspond to the ./configure --prefix=$GPSBABEL_DIR
#
# Created by Robert Osfield.

FIND_PATH(GPSBABEL_INCLUDE_DIR defs.h
            ${DELTA_DIR}/inc
            ${GPSBABEL_DIR}/inc
            $ENV{DELTA_DIR}/inc
            $ENV{GPSBABEL_DIR}/inc
            $ENV{OSSROOT}/libpq/include
            ${DELTA_DIR}/include
            ${GPSBABEL_DIR}/include
            $ENV{DELTA_DIR}/include
            $ENV{GPSBABEL_DIR}/include)

MACRO(FIND_GPSBABEL_LIBRARY MYLIBRARY MYLIBRARYNAME)

    FIND_LIBRARY("${MYLIBRARY}_DEBUG"
        NAMES "${MYLIBRARYNAME}"
        PATHS
        ${DELTA_DIR}/lib
        ${GPSBABEL_DIR}/lib
        $ENV{DELTA_DIR}/lib
        $ENV{GPSBABEL_DIR}/lib
        ${DELTA_DIR}/lib/$ENV{OSSARCH}/Debug
        ${GPSBABEL_DIR}/lib/$ENV{OSSARCH}/Debug
        ${GPSBABEL_DIR}/lib/$ENV{OSSARCH}
        $ENV{DELTA_DIR}/lib/$ENV{OSSARCH}/Debug
        $ENV{GPSBABEL_DIR}/lib/$ENV{OSSARCH}/Debug
        $ENV{GPSBABEL_DIR}/lib/$ENV{OSSARCH})

    FIND_LIBRARY(${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
        ${DELTA_DIR}/lib
        ${GPSBABEL_DIR}/lib
        $ENV{DELTA_DIR}/lib
        $ENV{GPSBABEL_DIR}/lib
        ${DELTA_DIR}/lib/$ENV{OSSARCH}/Release
        ${GPSBABEL_DIR}/lib/$ENV{OSSARCH}/Release
        ${GPSBABEL_DIR}/lib/$ENV{OSSARCH}
        $ENV{DELTA_DIR}/lib/$ENV{OSSARCH}/Release
        $ENV{GPSBABEL_DIR}/lib/$ENV{OSSARCH}/Release
        $ENV{GPSBABEL_DIR}/lib/$ENV{OSSARCH})
    IF( NOT ${MYLIBRARY}_DEBUG)
        IF(MYLIBRARY)
            SET(${MYLIBRARY}_DEBUG ${MYLIBRARY})
        ENDIF(MYLIBRARY)
    ENDIF( NOT ${MYLIBRARY}_DEBUG)

ENDMACRO(FIND_GPSBABEL_LIBRARY LIBRARY LIBRARYNAME)

# optimized
FIND_GPSBABEL_LIBRARY(GPSBABEL_LIBRARY GPSBabel)

SET(GPSBABEL_FOUND "NO")
IF(GPSBABEL_LIBRARY AND GPSBABEL_INCLUDE_DIR)
    SET(GPSBABEL_FOUND "YES")
ENDIF(GPSBABEL_LIBRARY AND GPSBABEL_INCLUDE_DIR)
