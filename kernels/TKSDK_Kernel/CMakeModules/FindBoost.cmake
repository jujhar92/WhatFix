# Locate boost
# This module defines
# BOOST_THREAD_LIBRARY
# BOOST_FOUND, if false, do not try to link to gdal
# BOOST_INCLUDE_DIR, where to find the headers
#
# $BOOST_DIR is an environment variable that would
# correspond to the ./configure --prefix=$BOOST_DIR
#
# Created by Nishant

FIND_PATH(BOOST_INCLUDE_DIR boost/any.hpp
            ${BOOST_DIR}/include
            $ENV{BOOST_DIR}/include/$ENV{OSSBUILD}
            ${BOOST_DIR}/include
            $ENV{BOOST_DIR}/include
            NO_DEFAULT_PATH)

MACRO(FIND_BOOST_LIBRARY MYLIBRARY MYLIBRARYNAME)

    FIND_LIBRARY(${MYLIBRARY}
        NAMES ${MYLIBRARYNAME}
        PATHS
        $ENV{BOOST_LIB}
        $ENV{BOOST_DIR}/lib/$ENV{OSSARCH}/debug
        $ENV{BOOST_DIR}/lib/$ENV{OSSARCH}/release
		$ENV{BOOST_DIR}/lib/$ENV{OSSARCH}
        NO_DEFAULT_PATH
        )

ENDMACRO(FIND_BOOST_LIBRARY LIBRARY LIBRARYNAME)

# optimized
IF (WIN32)
    FIND_BOOST_LIBRARY(BOOST_THREAD_LIBRARY             boost_thread-vc90-mt-1_44)
	FIND_BOOST_LIBRARY(BOOST_CHRONO_LIBRARY             boost_chrono-vc90-mt-1_44)
    FIND_BOOST_LIBRARY(BOOST_THREAD_LIBRARY_DEBUG       boost_thread-vc90-mt-gd-1_44)
	FIND_BOOST_LIBRARY(BOOST_CHRONO_LIBRARY_DEBUG       boost_chrono-vc90-mt-gd-1_44)
    FIND_BOOST_LIBRARY(BOOST_DATETIME_LIBRARY           boost_date_time-vc90-mt-1_44)
    FIND_BOOST_LIBRARY(BOOST_DATETIME_LIBRARY_DEBUG     boost_date_time-vc90-mt-gd-1_44)
    FIND_BOOST_LIBRARY(BOOST_FILESYSTEM_LIBRARY         boost_filesystem-vc90-mt-1_44)
    FIND_BOOST_LIBRARY(BOOST_FILESYSTEM_LIBRARY_DEBUG   boost_filesystem-vc90-mt-gd-1_44)
    FIND_BOOST_LIBRARY(BOOST_SYSTEM_LIBRARY             boost_system-vc90-mt-1_44)
    FIND_BOOST_LIBRARY(BOOST_SYSTEM_LIBRARY_DEBUG       boost_system-vc90-mt-gd-1_44)
	FIND_BOOST_LIBRARY(BOOST_TIMER_LIBRARY              boost_timer-vc90-mt-1_44)
	FIND_BOOST_LIBRARY(BOOST_TIMER_LIBRARY_DEBUG        boost_timer-vc90-mt-gd-1_44)	
ELSE (WIN32)
    FIND_BOOST_LIBRARY(BOOST_THREAD_LIBRARY             boost_thread)
    FIND_BOOST_LIBRARY(BOOST_THREAD_LIBRARY_DEBUG       boost_thread)
    FIND_BOOST_LIBRARY(BOOST_DATETIME_LIBRARY           boost_date_time)
    FIND_BOOST_LIBRARY(BOOST_DATETIME_LIBRARY_DEBUG     boost_date_time)
    FIND_BOOST_LIBRARY(BOOST_FILESYSTEM_LIBRARY         boost_filesystem)
    FIND_BOOST_LIBRARY(BOOST_FILESYSTEM_LIBRARY_DEBUG   boost_filesystem)
    FIND_BOOST_LIBRARY(BOOST_SYSTEM_LIBRARY             boost_system)
    FIND_BOOST_LIBRARY(BOOST_SYSTEM_LIBRARY_DEBUG       boost_system)
    FIND_BOOST_LIBRARY(BOOST_TIMER_LIBRARY              boost_timer)
    FIND_BOOST_LIBRARY(BOOST_TIMER_LIBRARY_DEBUG        boost_timer)
    FIND_BOOST_LIBRARY(BOOST_TEST_LIBRARY               boost_unit_test_framework)
    FIND_BOOST_LIBRARY(BOOST_TEST_LIBRARY_DEBUG         boost_unit_test_framework)
ENDIF (WIN32)

SET(BOOST_FOUND "NO")
IF(BOOST_LIBRARY AND BOOST_INCLUDE_DIR)
    SET(BOOST_FOUND "YES")
ENDIF(BOOST_LIBRARY AND BOOST_INCLUDE_DIR)
