:: arg 1 is release or debug option

:: set OSSROOT Here
set OSSROOT=C:\OSS_DSM_GGN\consulting\OSS

:: Portico directory
set PORTICO_DIR=C:\Program Files\Portico\portico-2.0.0

::Set PostgreSQL directory
set POSTGRESQL_DIR=C:\Program Files\PostgreSQL\9.3

:: Architecutre type
set OSSARCH=x64

:: Build type release or debug
set OSSBUILD=%1%

:: Set your /path/to/VizSim SDK sdk
set TERRAINSDK_DIR=C:\Teamcity\TKSDK_GGN

:: Call common env file
call vrplatformenv.bat %1%

:: Generate project files for building
call gen_cmake_from_scratch_64.bat

:: Compile code
cd %OSSARCH%
call "%VS90COMNTOOLS%..\..\VC\vcvarsall.bat" amd64
devenv /Rebuild release VRPLATFORM.sln

