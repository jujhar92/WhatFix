#!/bin/bash
export OSSROOT=/root/tree/consulting/OSS
echo $OSSROOT
#Set PlatForm Options
export OSSBUILD=$1
echo $OSSBUILD
export OSSARCH=$2
echo $OSSARCH
export TERRAINSDK_DIR=/root/BuildAgent/work/TKP_SDK
export VRPLATFORM_DIR=/root/BuildAgent/work/VRPlatform
export GEOORBIS_DEPS_DIR=/root/projects/geoDeps
