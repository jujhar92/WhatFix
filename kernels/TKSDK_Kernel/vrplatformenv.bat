echo off
:: Call localSettings batch files
call localSettings.bat

:: Set build options   debug|release
set OSSBUILD=%1%


set VRPLATFORM_DIR=.

:: set Terrain sdk
set TERRAINSDK_INC=%TERRAINSDK_DIR%\include
set TERRAINSDK_LIB=%TERRAINSDK_DIR%\%OSSARCH%\lib
set TERRAINSDK_BIN=%TERRAINSDK_DIR%\%OSSARCH%\bin;%TERRAINSDK_DIR%\%OSSARCH%\bin\vizDBPlugins;

:: Set SQLite directory
set SQLITE_DIR=%OSSROOT%\sqlite\3.7.2
set SQLITE_INC=%SQLITE_DIR%\include
set SQLITE_BIN=%SQLITE_DIR%\lib\%OSSARCH%
set SQLITE_LIB=%SQLITE_DIR%\lib\%OSSARCH%

:: Set Spatialite, proj, iconv, geos, xerces and QT include and library directories
set SPATIALITE_DIR=%OSSROOT%\spatialite\4.1
set SPATIALITE_INC=%SPATIALITE_DIR%\include
set SPATIALITE_BIN=%SPATIALITE_DIR%\bin\%OSSARCH%\%OSSBUILD%
set SPATIALITE_LIB=%SPATIALITE_DIR%\lib\%OSSARCH%\%OSSBUILD%

set LIBPROJ_DIR=%OSSROOT%\Proj
set LIBPROJ_INC=%LIBPROJ_DIR%\include
set LIBPROJ_BIN=%LIBPROJ_DIR%\lib\%OSSARCH%\release
set LIBPROJ_LIB=%LIBPROJ_DIR%\lib\%OSSARCH%\release

set ICONV_DIR=%OSSROOT%\iconv\1.9.2
set ICONV_INC=%ICONV_DIR%\include
set ICONV_BIN=%ICONV_DIR%\lib\%OSSARCH%
set ICONV_LIB=%ICONV_DIR%\lib\%OSSARCH%

set GEOS_DIR=%OSSROOT%\geos\3.3.8
set GEOS_INC=%GEOS_DIR%\include
set GEOS_BIN=%GEOS_DIR%\lib\%OSSARCH%\%OSSBUILD%
set GEOS_LIB=%GEOS_DIR%\lib\%OSSARCH%

set XERCESC_DIR=%OSSROOT%\xercesc\3.1.1
set XERCESC_INC=%XERCESC_DIR%\include
set XERCESC_BIN=%XERCESC_DIR%\lib\%OSSARCH%
set XERCESC_LIB=%XERCESC_DIR%\lib\%OSSARCH%

:: set OSG include and library directories
set OSG_DIR=%OSSROOT%\OSG\3.2.0
set OSG_BIN=%OSG_DIR%\bin\%OSSARCH%\%OSSBUILD%
set OSG_INC=%OSG_DIR%\include
set OSG_LIB=%OSG_DIR%\lib\%OSSARCH%\%OSSBUILD%

:: Set OPENTHREADS include and library directories
set OPENTHREADS_INC=%OSG_INC%
set OPENTHREADS_LIB=%OSG_LIB%
:: Set gdal include and library directories
set GDAL_DIR=%OSSROOT%\gdal\1.10.0(CustomChanges)_HDFsupport
set GDAL_INC=%GDAL_DIR%\include
set GDAL_LIB=%GDAL_DIR%\lib\%OSSARCH%\%OSSBUILD%
set GDAL_BIN=%GDAL_DIR%\bin\%OSSARCH%\%OSSBUILD%;%GDAL_LIB%
set GDAL_DATA=%GDAL_DIR%\data

::Set HDF4 include and library directories
set HDF4_DIR=%OSSROOT%\hdf\HDF4
set HDF4_INC=%HDF4_DIR%\include
set HDF4_LIB=%HDF4_DIR%\lib
set HDF4_BIN=%HDF4_DIR%\bin\%OSSARCH%\%OSSBUILD%

::Set HDF5 include and library directories
set HDF5_DIR=%OSSROOT%\hdf\HDF5
set HDF5_INC=%HDF5_DIR%\include
set HDF5_LIB=%HDF5_DIR%\lib
set HDF5_BIN=%HDF5_DIR%\bin;%HDF5_LIB%

:: Set osgEarth include and library directories
::set OSGEARTH_DIR=%OSSROOT%\osgEarth\osgEarth2.4_GoogleAndBingDriver_HDFsupport
set OSGEARTH_DIR=%OSSROOT%\osgEarth\osgEarth2.5Trunk_OSG_3.2.0
set OSGEARTH_BIN=%OSGEARTH_DIR%\bin\%OSSARCH%\%OSSBUILD%;%OSGEARTH_DIR%\bin\%OSSARCH%\%OSSBUILD%\osgPlugins-3.2.0
set OSGEARTH_INC=%OSGEARTH_DIR%\include
set OSGEARTH_LIB=%OSGEARTH_DIR%\lib\%OSSARCH%\%OSSBUILD%

:: Set the curl directory
set CURL_DIR=%OSSROOT%\\curl\7.23.1
set CURL_INC=%CURL_DIR%\include\
set CURL_LIB=%CURL_DIR%\lib\%OSSARCH%\%OSSBUILD%
set CURL_BIN=%CURL_DIR%\lib\%OSSARCH%\%OSSBUILD%

:: Set expat variables
set EXPAT_DIR=%OSSROOT%\expat\2.0.1
set EXPAT_INC=%EXPAT_DIR%\include
set EXPAT_LIB=%EXPAT_DIR%\lib\%OSSARCH%\%OSSBUILD%
set EXPAT_BIN=%EXPAT_LIB%

::set osgEphemeris variables
set OSG_EPHEMERIS_DIR=%OSSROOT%\OSGEphemeris\OSG_301
set OSG_EPHEMERIS_LIB=%OSSROOT%\OSGEphemeris\OSG_301\lib\%OSSARCH%\%OSSBUILD%
set OSG_EPHEMERIS_INC=%OSSROOT%\OSGEphemeris\OSG_301\include
set OSG_EPHEMERIS_BIN=%OSSROOT%\OSGEphemeris\OSG_301\lib\%OSSARCH%\%OSSBUILD%

:: Set Proj directory
set PROJ_DIR=%OSSROOT%\Proj
set PROJ_BIN=%PROJ_DIR%\lib\%OSSARCH%\release

:: Set tiff directory
set TIFF_DIR=%OSSROOT%\tiff
set TIFF_BIN=%TIFF_DIR%\lib\%OSSARCH%\release

:: Set boost directory
set BOOST_DIR=%OSSROOT%\Boost\1.44.0
set BOOST_BIN=%BOOST_DIR%\lib\%OSSARCH%\%OSSBUILD%
set BOOST_INC=%BOOST_DIR%\include
set BOOST_LIB=%BOOST_DIR%\lib\%OSSARCH%\%OSSBUILD%

:: Set jpeg directory
set JPEG_DIR=%OSSROOT%\jpeg
set JPEG_BIN=%JPEG_DIR%\lib\%OSSARCH%\release

:: Set zlib directory
set ZLIB_DIR=%OSSROOT%\zlib
set ZLIB_BIN=%ZLIB_DIR%\lib\%OSSARCH%\release

:: Set png directory
set PNG_DIR=%OSSROOT%\png
set PNG_BIN=%PNG_DIR%\lib\%OSSARCH%\release

:: FFMPEG binary path
set FFMPEG_BIN=%OSSROOT%\ffmpeg\bin\%OSSARCH%

:: QWT library
set QWT_DIR=%OSSROOT%\qwt\5.2
set QWT_INC=%QWT_DIR%\include
set QWT_BIN=%QWT_DIR%\bin\%OSSARCH%
set QWT_LIB=%QWT_DIR%\lib\%OSSARCH%

:: GPSBabel library
set GPSBABEL_DIR=%OSSROOT%\GPSBabel\1.3.6
set GPSBABEL_INC=%GPSBABEL_DIR%\include
set GPSBABEL_BIN=%GPSBABEL_DIR%\lib\%OSSARCH%\%OSSBUILD%
set GPSBABEL_LIB=%GPSBABEL_DIR%\lib\%OSSARCH%\%OSSBUILD%

:: PQLabs
set PQLABS_DIR=%OSSROOT%\PQLabs\PQClient
set PQLABS_BIN=%OSSROOT%\PQLabs\PQClient\bin

:: Geo trans library
set GEOTRANS_DIR=%OSSROOT%\GeoTrans\2.4.2
set GEOTRANS_BIN=%GEOTRANS_DIR%\lib\%OSSARCH%\%OSSBUILD%
set GEOTRANS_LIB=%GEOTRANS_DIR%\lib\%OSSARCH%\%OSSBUILD%
set GEOTRANS_INC=%GEOTRANS_DIR%\include

:: Setting current directory as INDIGIS3D directory
FOR /F "tokens=1" %%i in ('cd') do set SMCREATOR_DIR=%%i

:: Set clapack directory
set CLAPACK_DIR=%OSSROOT%\CLapack

:: Set xulrunner binary path
set XULRUNNER_DIR=%OSSROOT%\OpenVRML\0.18.6\xulrunner-sdk
set XULRUNNER_BIN=%XULRUNNER_DIR%\bin

:: Set OpenVrml binary path
set OPENVRML_DIR=%OSSROOT%\OpenVRML\0.18.6
set OPENVRML_BIN=%OPENVRML_DIR%\bin\%OSSARCH%\%OSSBUILD%
set OPENVRML_LIB=%OPENVRML_DIR%\lib\%OSSARCH%\%OSSBUILD%
set OPENVRML_INC=%OPENVRML_DIR%\include
set OPENVRML_DATADIR=%OPENVRML_DIR%\data
set OPENVRML_NODE_PATH=%OPENVRML_DIR%\lib\%OSSARCH%\%OSSBUILD%\node
set OPENVRML_SCRIPT_PATH=%OPENVRML_DIR%\lib\%OSSARCH%\%OSSBUILD%\script

:: set freetype binary path
set FREETYPE_DIR=%OSSROOT%\freetype\2.3.5
set FREETYPE_LIB=%FREETYPE_DIR%\lib\%OSSARCH%\release

:: Taudem binary path
set TAUDEM_BIN=%OSSROOT%\taudem\bin\%OSSARCH%

:: Set gif directory
set GIF_DIR=%OSSROOT%\giflib\4.1.4\
set GIF_BIN=%GIF_DIR%\lib\%OSSARCH%\release

::Set DGNLib directory
set DGNLIB_DIR=%OSSROOT%\dgnlib
set DGNLIB_BIN=%DGNLIB_DIR%\lib\%OSSARCH%
set DGNLIB_LIB=%DGNLIB_DIR%\lib\%OSSARCH%
set DGNLIB_INC=%DGNLIB_DIR%\include

set LIBJSON_DIR=%OSSROOT%\LibJSON\7.61
set LIBJSON_INC=%LIBJSON_DIR%\include
set LIBJSON_LIB=%LIBJSON_DIR%\lib\%OSSARCH%\%OSSBUILD%

:: PQ library
set PQLIB_DIR=%OSSROOT%\libpq\9.0
set PQLIB_BIN=%PQLIB_DIR%\lib\%OSSARCH%
set PQLIB_LIB=%PQLIB_DIR%\lib\%OSSARCH%
set PQLIB_INC=%PQLIB_DIR%\include

:: ECW binaries
set ECW_DIR=%OSSROOT%\ecw\4.2
set ECW_BIN=%ECW_DIR%\bin\%OSSARCH%\release

:: Portico directory
set RTI_HOME=%PORTICO_DIR%
set JAVA_HOME=%PORTICO_DIR%/jre
set RTI_DIR=%RTI_HOME%
set RTI_INC=%RTI_DIR%\include
set RTI_LIB=%RTI_DIR%\lib\vc9
set RTI_BIN=%RTI_DIR%\bin
set RTI_JAVA_BIN=%RTI_DIR%\jre\bin\server
::set RTI_JRE_BIN=%RTI_DIR%\jre\bin
set RTI_RID_FILE=../../data/HLA/RTI.rid
set CLASSPATH=%RTI_DIR%\lib\portico.jar;%CLASSPATH%

:: CGAL Directory
set CGAL_DIR=%OSSROOT%\CGAL\CGAL-4.0
set CGAL_INC=%CGAL_DIR%\include
set CGAL_BIN=%CGAL_DIR%\bin\%OSSARCH%
set CGAL_LIB=%CGAL_DIR%\lib\%OSSARCH%

::Poppler Directory
set POPPLER_DIR=%OSSROOT%\poppler\poppler-0.22.3
set POPPLER_INC=%POPPLER_DIR%\include
set POPPLER_LIB=%POPPLER_DIR%\lib\%OSSARCH%\%OSSBUILD%

:: OpenAL Directory
set OpenAL_DIR=%OSSROOT%\openAL\1.1
set OpenAL_INC=%OpenAL_DIR%\include
set OpenAL_LIB=%OpenAL_DIR%\libs\%OSSARCH%

:: ALUT Directory
set ALUT_DIR=%OSSROOT%\ALUT\freealut-1.1.0
set ALUT_INC=%ALUT_DIR%\include
set ALUT_LIB=%ALUT_DIR%\lib\%OSSARCH%\release

:: FMOD Directory
set FMOD_DIR=%OSSROOT%\FMOD\4.44.47
set FMOD_INC=%FMOD_DIR%\include
set FMOD_BIN=%FMOD_DIR%\lib\%OSSARCH%
set FMOD_LIB=%FMOD_DIR%\lib\%OSSARCH%

:: Matlab Directory
set MATLAB_DIR=C:\\Program Files (x86)\\MATLAB\\R2013a
set MATLAB_INC=%MATLAB_DIR%\extern\include
set MATLAB_LIB=%MATLAB_DIR%\extern\lib\win32\microsoft
set MATLAB_BIN=%MATLAB_DIR%\bin\win32

:: Qt Directory
set QT_DIR=%OSSROOT%\..\Qt\4.8.2
set QT_INC=%QT_DIR%\include
set QT_LIB=%QT_DIR%\lib\%OSSARCH%
set QT_BIN=%QT_DIR%\bin\%OSSARCH%

::3rd Party folder for OsgDB SVG Plugin
set THIRD_PARTY_BIN=%SMCREATOR_DIR%\3rdParty
set QML_IMPORT_PATH=%THIRD_PARTY_BIN%\imports

:: Set Dependency Binary Paths
set PATH=%OSG_BIN%;%GDAL_BIN%;%CURL_BIN%;%EXPAT_BIN%;%PROJ_BIN%;%XERCESC_BIN%;%GEOS_BIN%;%ICONV_BIN%;%LIBPROJ_BIN%;%SPATIALITE_BIN%;%TIFF_BIN%;%BOOST_BIN%;%PATH%
set PATH=%SMCREATOR_DIR%\%OSSARCH%\bin;%OSGEARTH_BIN%;%QT_BIN%;%SQLITE_BIN%;%PATH%
set PATH=%QWT_BIN%;%ZLIB_BIN%;%JPEG_BIN%;%PNG_BIN%;%GNUPLOT_BIN%;%GS_BIN%;%PATH%
set PATH=%FFMPEG_BIN%;%CLAPACK_DIR%;%CLAPACK_DIR%;%PATH%
set PATH=%OSG_EPHEMERIS_BIN%;%PATH%
set PATH=%XULRUNNER_BIN%;%OPENVRML_BIN%;%OPENVRML_LIB%;%OPENVRML_INC%;%PATH%
set PATH=%FREETYPE_LIB%;%TAUDEM_BIN%;%MPICH_BIN%;%PATH%
set PATH=%GPSD_BIN%;%EXPAT_BIN%;%GPSBABEL_BIN%;%OSG_EPHEMERIS_BIN%;%PATH%
set PATH=%ECW_BIN%;%PATH%
set PATH=%RTI_JAVA_BIN%;%RTI_BIN%;%RTI_JRE_BIN%;%PQLIB_BIN%;%GIF_BIN%;%DGNLIB_BIN%;%PATH%
set PATH=%CGAL_BIN%;;%HDF4_BIN%;%HDF5_BIN%;%PATH%
set PATH=%TERRAINSDK_LIB%;%TERRAINSDK_BIN%;%THIRD_PARTY_BIN%;%PATH%
set PATH=%TERRAINSDK_BIN%/vizDBPlugins;%PATH%
set PATH=%PQLABS_BIN%;%PATH%
set PATH=%PATH%;%POPPLER_LIB%
set PATH=%PATH%;%GEOTRANS_BIN%;%ALUT_LIB%;%OpenAL_LIB%;%FMOD_BIN%;%MATLAB_BIN%
set PATH=%PATH%;%QT_BIN%

:: Compute Near Far plane using primitives
set OSG_COMPUTE_NEAR_FAR_MODE=COMPUTE_NEAR_FAR_USING_PRIMITIVES
set DATA_SOURCE_COMPONENT_NAME=TempFeature

::set OSG_NOTIFY_LEVEL=ALWAYS
::set OSGEARTH_NOTIFY_LEVEL=ALWAYS
