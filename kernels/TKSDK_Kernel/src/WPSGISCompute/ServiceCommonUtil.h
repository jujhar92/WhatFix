/****************************************************************************
 *
 *
 *  Copyright 2010-2014, VizExpertsIndia Pvt. Ltd. (unpublished)
 *
 *  All rights reserved. This notice is intended as a precaution against
 *  inadvertent publication and does not imply publication or any waiver
 *  of confidentiality. The year included in the foregoing notice is the
 *  year of creation of the work. No part of this work may be used,
 *  reproduced, or transmitted in any form or by any means without the prior
 *  written permission of Vizexperts India Pvt Ltd.
 *
 *
 ***************************************************************************
 */

#ifndef WPSGISCOMPUTE_SERVICECOMMONUTIL_H
#define WPSGISCOMPUTE_SERVICECOMMONUTIL_H

#include <boost/shared_ptr.hpp>
#include <gdal/ogr_geometry.h>
#include "service.h"

extern "C" {

struct ServiceConfig
{
    static std::string getThriftServerIP()
    {
        return "127.0.0.1";
    }

    static int getThriftServerPort()
    {
        return 9003;
    }
private:
    static std::string _thriftServerIP;
    static int _thriftServerPort;
};

enum GeometryType
{
    POINT = 0,
    SURFACE,
    GEOMETRY_COLLECTION,
    CURVE,
    COMPOUND_CURVE,
    SIMPLE_CURVE,
    CIRCULAR_STRING,
    LINE_STRING,
    LINEAR_RING,
    MULTI_CURVE,
    MULTI_POINT,
    MULTI_SURFACE,
    MULTI_LINE_STRING,
    MULTI_POLYGON,
    CURVE_POLYGON,
    POLYGON
};
typedef GeometryType GeometryType;

/// \brief dumps zoo maps class to given file
void logMap(maps*& data, const std::string& filepath);

/// \brief Function to get GDAL Geometry class for given type
boost::shared_ptr<OGRGeometry> getOGRGeometryFromMap(maps* dataMap, GeometryType type);

/// \brief Functions to get concrete class object from zoo maps (For internal Use)
boost::shared_ptr<OGRGeometryCollection> getGeometryCollectionFromMap(maps* dataMap);
boost::shared_ptr<OGRPoint> getPointGeometryFromMap(maps* dataMap);
boost::shared_ptr<OGRMultiPoint> getMultiPointGeometryFromMap(maps* dataMap);
boost::shared_ptr<OGRLineString> getLineStringGeometryFromMap(maps* dataMap);
boost::shared_ptr<OGRMultiPolygon> getMultiPolygonGeometryFromMap(maps* dataMap);
boost::shared_ptr<OGRPolygon> getPolygonGeometryFromMap(maps* dataMap);
boost::shared_ptr<OGRLinearRing> getLinearRingGeometryFromMap(maps* dataMap);

}

#endif
