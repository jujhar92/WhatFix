/****************************************************************************
 *
 *
 *  Copyright 2010-2014, VizExpertsIndia Pvt. Ltd. (unpublished)
 *
 *  All rights reserved. This notice is intended as a precaution against
 *  inadvertent publication and does not imply publication or any waiver
 *  of confidentiality. The year included in the foregoing notice is the
 *  year of creation of the work. No part of this work may be used,
 *  reproduced, or transmitted in any form or by any means without the prior
 *  written permission of Vizexperts India Pvt Ltd.
 *
 *
 ***************************************************************************
 */

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TMultiplexedProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/transport/TTransport.h>
#include <thrift/transport/TSocket.h>
#include <thrift/TProcessor.h>

#include <boost/shared_ptr.hpp>

#include <gdal/ogr_geometry.h>

#include "AngleSlopeQuery.h"

#include <fstream>
#include <sstream>
#include <iostream>

#include "service.h"

#include "ServiceCommonUtil.h"

#ifdef WIN32
__declspec(dllexport)
#endif

extern "C" {

int getAngleBetweenPoints(maps*& conf,maps*& inputs, maps*& outputs)
{
    std::vector<GeorbIS::Core::GeoPoint> points;
    if(NULL == inputs) return SERVICE_FAILED;
    maps* inputHead = inputs;
    while(inputHead != NULL)
    {
        boost::shared_ptr<OGRGeometry> geometry = getOGRGeometryFromMap(inputHead, POINT);
        if(geometry != NULL)
        {
            boost::shared_ptr<OGRPoint> ogrPoint = boost::shared_dynamic_cast<OGRPoint>(geometry);
            if(ogrPoint != NULL)
            {
                GeorbIS::Core::GeoPoint point;
                point.latitude = ogrPoint->getX();
                point.longitude = ogrPoint->getY();
                point.altitude = ogrPoint->getZ();
                points.push_back(point);
            }
        }
        inputHead = inputHead->next;
    }
    if(points.size() < 2)
    {
        return SERVICE_FAILED;
    }
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(ServiceConfig::getThriftServerIP(), ServiceConfig::getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> angleSlopeQueryProtocol(new thrift::protocol::TMultiplexedProtocol(protocol, "AngleSlopeQuery"));
    boost::shared_ptr<GeorbIS::Processing::AngleSlopeQueryClient> angleSlopeQueryClient(new GeorbIS::Processing::AngleSlopeQueryClient(angleSlopeQueryProtocol));
    double angle = 0.0;
    try
    {
        transport->open();
        angle = angleSlopeQueryClient->getAngleBetweenPoints(points[0], points[1]);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating elevation difference\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",angle);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getSlopeBetweenPoints(maps*& conf,maps*& inputs, maps*& outputs)
{
    std::vector<GeorbIS::Core::GeoPoint> points;
    if(NULL == inputs) return SERVICE_FAILED;
    maps* inputHead = inputs;
    while(inputHead != NULL)
    {
        boost::shared_ptr<OGRGeometry> geometry = getOGRGeometryFromMap(inputHead, POINT);
        if(geometry != NULL)
        {
            boost::shared_ptr<OGRPoint> ogrPoint = boost::shared_dynamic_cast<OGRPoint>(geometry);
            if(ogrPoint != NULL)
            {
                GeorbIS::Core::GeoPoint point;
                point.latitude = ogrPoint->getX();
                point.longitude = ogrPoint->getY();
                point.altitude = ogrPoint->getZ();
                points.push_back(point);
            }
        }
        inputHead = inputHead->next;
    }
    if(points.size() < 2)
    {
        return SERVICE_FAILED;
    }
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(ServiceConfig::getThriftServerIP(), ServiceConfig::getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> angleSlopeQueryProtocol(new thrift::protocol::TMultiplexedProtocol(protocol, "AngleSlopeQuery"));
    boost::shared_ptr<GeorbIS::Processing::AngleSlopeQueryClient> angleSlopeQueryClient(new GeorbIS::Processing::AngleSlopeQueryClient(angleSlopeQueryProtocol));
    double angle = 0.0;
    try
    {
        transport->open();
        angle = angleSlopeQueryClient->getSlopeBetweenPoints(points[0], points[1]);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating elevation difference\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",angle);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

}
