#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TMultiplexedProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/transport/TTransport.h>
#include <thrift/transport/TSocket.h>
#include <thrift/TProcessor.h>

#include <boost/shared_ptr.hpp>

#include <gdal/ogr_geometry.h>

#include "MeasurementQuery.h"
#include "ElevationQuery.h"

#include <fstream>
#include <sstream>
#include <iostream>

#include "service.h"

#include "ServiceCommonUtil.h"

#ifdef WIN32
__declspec(dllexport)
#endif

extern "C" {

int getAerielDistanceE(maps*& conf,maps*& inputs, maps*& outputs)
{
    std::vector<GeorbIS::Core::Point3D> points;
    if(NULL == inputs) return SERVICE_FAILED;
    maps* inputHead = inputs;
    while(inputHead != NULL)
    {
        boost::shared_ptr<OGRGeometry> geometry = getOGRGeometryFromMap(inputHead, POINT);
        if(geometry != NULL)
        {
            boost::shared_ptr<OGRPoint> ogrPoint = boost::shared_dynamic_cast<OGRPoint>(geometry);
            if(ogrPoint != NULL)
            {
                GeorbIS::Core::Point3D point;
                point.x = ogrPoint->getX();
                point.y = ogrPoint->getY();
                point.z = ogrPoint->getZ();
                points.push_back(point);
            }
        }
        inputHead = inputHead->next;
    }
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(ServiceConfig::getThriftServerIP(), ServiceConfig::getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    double calculatedDistance = 0;
    try
    {
        transport->open();
        calculatedDistance = measurementclient->getAerielDistanceE(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating aeriel distance\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedDistance);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getAerielDistanceG(maps*& conf,maps*& inputs, maps*& outputs)
{
    std::vector<GeorbIS::Core::GeoPoint> points;
    if(NULL == inputs) return SERVICE_FAILED;
    maps* inputHead = inputs;
    while(inputHead != NULL)
    {
        boost::shared_ptr<OGRGeometry> geometry = getOGRGeometryFromMap(inputHead, POINT);
        if(geometry != NULL)
        {
            boost::shared_ptr<OGRPoint> ogrPoint = boost::shared_dynamic_cast<OGRPoint>(geometry);
            if(ogrPoint != NULL)
            {
                GeorbIS::Core::GeoPoint point;
                point.latitude = ogrPoint->getX();
                point.longitude = ogrPoint->getY();
                point.altitude = ogrPoint->getZ();
                points.push_back(point);
            }
        }
        inputHead = inputHead->next;
    }
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(ServiceConfig::getThriftServerIP(), ServiceConfig::getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    double calculatedDistance = 0;
    try
    {
        transport->open();
        calculatedDistance = measurementclient->getAeirelDistanceG(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating aeriel distance\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedDistance);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getProjectedDistanceE(maps*& conf,maps*& inputs, maps*& outputs)
{
    std::vector<GeorbIS::Core::Point3D> points;
    if(NULL == inputs) return SERVICE_FAILED;
    maps* inputHead = inputs;
    while(inputHead != NULL)
    {
        boost::shared_ptr<OGRGeometry> geometry = getOGRGeometryFromMap(inputHead, POINT);
        if(geometry != NULL)
        {
            boost::shared_ptr<OGRPoint> ogrPoint = boost::shared_dynamic_cast<OGRPoint>(geometry);
            if(ogrPoint != NULL)
            {
                GeorbIS::Core::Point3D point;
                point.x = ogrPoint->getX();
                point.y = ogrPoint->getY();
                point.z = ogrPoint->getZ();
                points.push_back(point);
            }
        }
        inputHead = inputHead->next;
    }
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(ServiceConfig::getThriftServerIP(), ServiceConfig::getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    double calculatedDistance = 0;
    try
    {
        transport->open();
        calculatedDistance = measurementclient->getProjectedDistanceE(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating aeriel distance\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedDistance);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getProjectedDistanceG(maps*& conf,maps*& inputs, maps*& outputs)
{
    std::vector<GeorbIS::Core::GeoPoint> points;
    if(NULL == inputs) return SERVICE_FAILED;
    maps* inputHead = inputs;
    while(inputHead != NULL)
    {
        boost::shared_ptr<OGRGeometry> geometry = getOGRGeometryFromMap(inputHead, POINT);
        if(geometry != NULL)
        {
            boost::shared_ptr<OGRPoint> ogrPoint = boost::shared_dynamic_cast<OGRPoint>(geometry);
            if(ogrPoint != NULL)
            {
                GeorbIS::Core::GeoPoint point;
                point.latitude = ogrPoint->getX();
                point.longitude = ogrPoint->getY();
                point.altitude = ogrPoint->getZ();
                points.push_back(point);
            }
        }
        inputHead = inputHead->next;
    }
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(ServiceConfig::getThriftServerIP(), ServiceConfig::getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    double calculatedDistance = 0;
    try
    {
        transport->open();
        calculatedDistance = measurementclient->getProjectedDistanceG(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating aeriel distance\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedDistance);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getAerielAreaE(maps*& conf,maps*& inputs, maps*& outputs)
{
    std::vector<GeorbIS::Core::Point3D> points;
    if(NULL == inputs) return SERVICE_FAILED;
    maps* inputHead = inputs;
    while(inputHead != NULL)
    {
        boost::shared_ptr<OGRGeometry> geometry = getOGRGeometryFromMap(inputHead, MULTI_POINT);
        if(geometry != NULL)
        {
            boost::shared_ptr<OGRMultiPoint> ogrMultiPoint = boost::shared_dynamic_cast<OGRMultiPoint>(geometry);
            if(ogrMultiPoint != NULL)
            {
                for(int i = 0; i < ogrMultiPoint->getNumGeometries(); ++i)
                {
                    OGRPoint* ogrPoint = static_cast<OGRPoint*>(ogrMultiPoint->getGeometryRef(i));
                    GeorbIS::Core::Point3D point;
                    point.x = ogrPoint->getX();
                    point.y = ogrPoint->getY();
                    point.z = ogrPoint->getZ();
                    points.push_back(point);
                }
            }
        }
        inputHead = inputHead->next;
    }
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(ServiceConfig::getThriftServerIP(), ServiceConfig::getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    double calculatedArea = 0;
    try
    {
        transport->open();
        calculatedArea = measurementclient->getAerielAreaE(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating aeriel area\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedArea);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getAerielAreaG(maps*& conf,maps*& inputs, maps*& outputs)
{
    std::vector<GeorbIS::Core::GeoPoint> points;
    if(NULL == inputs) return SERVICE_FAILED;
    maps* inputHead = inputs;
    while(inputHead != NULL)
    {
        boost::shared_ptr<OGRGeometry> geometry = getOGRGeometryFromMap(inputHead, MULTI_POINT);
        if(geometry != NULL)
        {
            boost::shared_ptr<OGRMultiPoint> ogrMultiPoint = boost::shared_dynamic_cast<OGRMultiPoint>(geometry);
            if(ogrMultiPoint != NULL)
            {
                for(int i = 0; i < ogrMultiPoint->getNumGeometries(); ++i)
                {
                    OGRPoint* ogrPoint = static_cast<OGRPoint*>(ogrMultiPoint->getGeometryRef(i));
                    GeorbIS::Core::GeoPoint point;
                    point.latitude = ogrPoint->getX();
                    point.longitude = ogrPoint->getY();
                    point.altitude = ogrPoint->getZ();
                    points.push_back(point);
                }
            }
        }
        inputHead = inputHead->next;
    }
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(ServiceConfig::getThriftServerIP(), ServiceConfig::getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    double calculatedArea = 0;
    try
    {
        transport->open();
        calculatedArea = measurementclient->getAerielAreaG(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating aeriel area\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedArea);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getProjectedAreaE(maps*& conf,maps*& inputs, maps*& outputs)
{
    std::vector<GeorbIS::Core::Point3D> points;
    if(NULL == inputs) return SERVICE_FAILED;
    maps* inputHead = inputs;
    while(inputHead != NULL)
    {
        boost::shared_ptr<OGRGeometry> geometry = getOGRGeometryFromMap(inputHead, MULTI_POINT);
        if(geometry != NULL)
        {
            boost::shared_ptr<OGRMultiPoint> ogrMultiPoint = boost::shared_dynamic_cast<OGRMultiPoint>(geometry);
            if(ogrMultiPoint != NULL)
            {
                for(int i = 0; i < ogrMultiPoint->getNumGeometries(); ++i)
                {
                    OGRPoint* ogrPoint = static_cast<OGRPoint*>(ogrMultiPoint->getGeometryRef(i));
                    GeorbIS::Core::Point3D point;
                    point.x = ogrPoint->getX();
                    point.y = ogrPoint->getY();
                    point.z = ogrPoint->getZ();
                    points.push_back(point);
                }
            }
        }
        inputHead = inputHead->next;
    }
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(ServiceConfig::getThriftServerIP(), ServiceConfig::getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    double calculatedArea = 0;
    try
    {
        transport->open();
        calculatedArea = measurementclient->getProjectedAreaE(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating projected area\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedArea);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getProjectedAreaG(maps*& conf,maps*& inputs, maps*& outputs)
{
    std::vector<GeorbIS::Core::GeoPoint> points;
    if(NULL == inputs) return SERVICE_FAILED;
    maps* inputHead = inputs;
    while(inputHead != NULL)
    {
        boost::shared_ptr<OGRGeometry> geometry = getOGRGeometryFromMap(inputHead, MULTI_POINT);
        if(geometry != NULL)
        {
            boost::shared_ptr<OGRMultiPoint> ogrMultiPoint = boost::shared_dynamic_cast<OGRMultiPoint>(geometry);
            if(ogrMultiPoint != NULL)
            {
                for(int i = 0; i < ogrMultiPoint->getNumGeometries(); ++i)
                {
                    OGRPoint* ogrPoint = static_cast<OGRPoint*>(ogrMultiPoint->getGeometryRef(i));
                    GeorbIS::Core::GeoPoint point;
                    point.latitude = ogrPoint->getX();
                    point.longitude = ogrPoint->getY();
                    point.altitude = ogrPoint->getZ();
                    points.push_back(point);
                }
            }
        }
        inputHead = inputHead->next;
    }
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(ServiceConfig::getThriftServerIP(), ServiceConfig::getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    double calculatedArea = 0;
    try
    {
        transport->open();
        calculatedArea = measurementclient->getProjectedAreaG(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating projected area\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedArea);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

}
