#include <fstream>
#include "ServiceCommonUtil.h"


void indent(int currentIndent, std::ofstream& stream)
{
    while(currentIndent > 0)
    {
        stream << " ";
        --currentIndent;
    }
}

void logMap(maps*& data, const std::string& filePath)
{
    std::ofstream ofs;
    ofs.open(filePath.c_str(), std::ios::app);
    ofs << "\n-------------------------------------------------------------------------\n";
    int currentIndent = 0;
    maps* headMap = data;
    while(headMap != NULL)
    {
        indent(currentIndent, ofs);
        ofs << headMap->name << "\n";
        currentIndent += 4;
        map* inMap = headMap->content;
        while(inMap != NULL)
        {
            indent(currentIndent, ofs);
            ofs << inMap->name << " :: " << inMap->value << "\n";
            inMap = inMap->next;
        }
        currentIndent -= 4;
        headMap = headMap->next;
    }
    ofs.close();
}

boost::shared_ptr<OGRGeometry> getOGRGeometryFromMap(maps* dataMap, GeometryType type)
{
    boost::shared_ptr<OGRGeometry> geometry;
    switch (type) {
        case GEOMETRY_COLLECTION:
        {
            boost::shared_ptr<OGRGeometryCollection> geometryCollection = getGeometryCollectionFromMap(dataMap);
            geometry = boost::shared_dynamic_cast<OGRGeometry>(geometryCollection);
            break;
        }
        case POINT:
        {
            boost::shared_ptr<OGRPoint> point = getPointGeometryFromMap(dataMap);
            geometry = boost::shared_dynamic_cast<OGRGeometry>(point);
            break;
        }
        case MULTI_POINT:
        {
            boost::shared_ptr<OGRMultiPoint> multiPoint = getMultiPointGeometryFromMap(dataMap);
            geometry = boost::shared_dynamic_cast<OGRGeometry>(multiPoint);
            break;
        }
        case LINE_STRING:
        {
            boost::shared_ptr<OGRLineString> lineString = getLineStringGeometryFromMap(dataMap);
            geometry = boost::shared_dynamic_cast<OGRGeometry>(lineString);
            break;
        }
        case MULTI_POLYGON:
        {
            boost::shared_ptr<OGRMultiPolygon> multiPolygon = getMultiPolygonGeometryFromMap(dataMap);
            geometry = boost::shared_dynamic_cast<OGRGeometry>(multiPolygon);
            break;
        }
        case POLYGON:
        {
            boost::shared_ptr<OGRPolygon> polygon = getPolygonGeometryFromMap(dataMap);
            geometry = boost::shared_dynamic_cast<OGRPolygon>(polygon);
            break;
        }
        case LINEAR_RING:
        {
            boost::shared_ptr<OGRLinearRing> linearRing = getLinearRingGeometryFromMap(dataMap);
            geometry = boost::shared_dynamic_cast<OGRLinearRing>(linearRing);
            break;
        }
        default:
        {
            break;
        }
    }
    return geometry;
}

boost::shared_ptr<OGRGeometryCollection> getGeometryCollectionFromMap(maps* dataMap)
{
    boost::shared_ptr<OGRGeometryCollection> geometryCollection;
    if(dataMap == NULL) return geometryCollection;
    map* mimetypeMap = getMap(dataMap->content, "mimeType");
    map* valueMap = getMap(dataMap->content, "value");
    if(mimetypeMap == NULL || valueMap == NULL) return geometryCollection;
    const char* mimetype = mimetypeMap->value;
    if(0 == strcmp(mimetype, "application/wkt"))
    {
        geometryCollection = boost::shared_ptr<OGRGeometryCollection>(new OGRGeometryCollection());
        geometryCollection->importFromWkt(&(valueMap->value));
        return geometryCollection;
    }
    return geometryCollection;
}

boost::shared_ptr<OGRPoint> getPointGeometryFromMap(maps* dataMap)
{
    boost::shared_ptr<OGRPoint> point;
    if(dataMap == NULL) return point;
    map* mimetypeMap = getMap(dataMap->content, "mimeType");
    map* valueMap = getMap(dataMap->content, "value");
    if(mimetypeMap == NULL || valueMap == NULL) return point;
    const char* mimetype = mimetypeMap->value;
    if(0 == strcmp(mimetype, "application/wkt"))
    {
        point = boost::shared_ptr<OGRPoint>(new OGRPoint());
        point->importFromWkt(&(valueMap->value));
        return point;
    }
    return point;
}

boost::shared_ptr<OGRMultiPoint> getMultiPointGeometryFromMap(maps* dataMap)
{
    boost::shared_ptr<OGRMultiPoint> multiPoint;
    if(dataMap == NULL) return multiPoint;
    map* mimetypeMap = getMap(dataMap->content, "mimeType");
    map* valueMap = getMap(dataMap->content, "value");
    if(mimetypeMap == NULL || valueMap == NULL) return multiPoint;
    const char* mimetype = mimetypeMap->value;
    if(0 == strcmp(mimetype, "application/wkt"))
    {
        multiPoint = boost::shared_ptr<OGRMultiPoint>(new OGRMultiPoint());
        multiPoint->importFromWkt(&(valueMap->value));
        return multiPoint;
    }
    return multiPoint;
}

boost::shared_ptr<OGRLineString> getLineStringGeometryFromMap(maps* dataMap)
{
    boost::shared_ptr<OGRLineString> lineString;
    if(dataMap == NULL) return lineString;
    map* mimetypeMap = getMap(dataMap->content, "mimeType");
    map* valueMap = getMap(dataMap->content, "value");
    if(mimetypeMap == NULL || valueMap == NULL) return lineString;
    const char* mimetype = mimetypeMap->value;
    if(0 == strcmp(mimetype, "application/wkt"))
    {
        lineString = boost::shared_ptr<OGRLineString>(new OGRLineString());
        lineString->importFromWkt(&(valueMap->value));
        return lineString;
    }
    return lineString;
}

boost::shared_ptr<OGRMultiPolygon> getMultiPolygonGeometryFromMap(maps* dataMap)
{
    boost::shared_ptr<OGRMultiPolygon> multiPolygon;
    if(dataMap == NULL) return multiPolygon;
    map* mimetypeMap = getMap(dataMap->content, "mimeType");
    map* valueMap = getMap(dataMap->content, "value");
    if(mimetypeMap == NULL || valueMap == NULL) return multiPolygon;
    const char* mimetype = mimetypeMap->value;
    if(0 == strcmp(mimetype, "application/wkt"))
    {
        multiPolygon = boost::shared_ptr<OGRMultiPolygon>(new OGRMultiPolygon());
        multiPolygon->importFromWkt(&(valueMap->value));
        return multiPolygon;
    }
    return multiPolygon;
}

boost::shared_ptr<OGRPolygon> getPolygonGeometryFromMap(maps* dataMap)
{
    boost::shared_ptr<OGRPolygon> polygon;
    if(dataMap == NULL) return polygon;
    map* mimetypeMap = getMap(dataMap->content, "mimeType");
    map* valueMap = getMap(dataMap->content, "value");
    if(mimetypeMap == NULL || valueMap == NULL) return polygon;
    const char* mimetype = mimetypeMap->value;
    if(0 == strcmp(mimetype, "application/wkt"))
    {
        polygon = boost::shared_ptr<OGRPolygon>(new OGRPolygon());
        polygon->importFromWkt(&(valueMap->value));
        return polygon;
    }
    return polygon;
}

boost::shared_ptr<OGRLinearRing> getLinearRingGeometryFromMap(maps* dataMap)
{
    boost::shared_ptr<OGRLinearRing> linearRing;
    if(dataMap == NULL) return linearRing;
    map* mimetypeMap = getMap(dataMap->content, "mimeType");
    map* valueMap = getMap(dataMap->content, "value");
    if(mimetypeMap == NULL || valueMap == NULL) return linearRing;
    const char* mimetype = mimetypeMap->value;
    if(0 == strcmp(mimetype, "application/wkt"))
    {
        linearRing = boost::shared_ptr<OGRLinearRing>(new OGRLinearRing());
        linearRing->importFromWkt(&(valueMap->value));
        return linearRing;
    }
    return linearRing;
}

