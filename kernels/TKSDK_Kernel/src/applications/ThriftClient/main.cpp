#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TMultiplexedProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/transport/TTransport.h>
#include <thrift/transport/TSocket.h>
#include <thrift/TProcessor.h>

#include <iostream>

#include "MeasurementQuery.h"
#include "ElevationQuery.h"

namespace thrift = apache::thrift;

int main(int argc, char* argv[])
{

    int port = 9003;

    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket("localhost", port));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));

    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
	(new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
	(new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));

    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> elevationProtocol
	(new thrift::protocol::TMultiplexedProtocol(protocol, "ElevationQuery"));
    boost::shared_ptr<GeorbIS::Processing::ElevationQueryClient> elevationClient
    (new GeorbIS::Processing::ElevationQueryClient(elevationProtocol));

    try{
    	transport->open();
    	std::vector<GeorbIS::Core::Point3D> points;
    	GeorbIS::Core::Point3D p1;
    	GeorbIS::Core::Point3D p2;
    	GeorbIS::Core::Point3D p3;
    	points.push_back(p1);
    	points.push_back(p2);
    	points.push_back(p3);
    	double area = measurementclient->getAerielAreaE(points);
    	std::cout << "Calculated area is " << area << "\n";

    	double ele = elevationClient->getElevationAtPointE(p1);
    	std::cout << "Elevation is " << ele << "\n";

    	transport->close();
    }
    catch(thrift::TException& ex)
    {
    	std::cout << "Error:: " << ex.what() << "\n";
    }
    std::cout << "Client Exiting\n";
    return 0;
}
