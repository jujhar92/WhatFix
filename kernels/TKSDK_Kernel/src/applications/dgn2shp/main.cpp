#include <Core/WorldMaintainer.h>
#include <Core/ICompositeObject.h>
#include <Core/IFeatureObject.h>
#include <Core/CoreRegistry.h>
#include <Core/IFeatureLayer.h>

#include <Elements/ElementsPlugin.h>

#include <GIS/GISPlugin.h>
#include <GIS/IFeatureLayer.h>

#include <DB/ReadFile.h>
#include <DB/WriteFile.h>

#include <boost/filesystem.hpp>

void progressCallback(int value)
{
    LOG_INFO_VAR("Reading DGN : ", value)
}

int main(int argc, char* argv[])
{
    if(argc < 2)
    {
        LOG_ERROR("no argument specified. required arguments are i)dgn file  ii) complete address of directory to save shp");
        return 0;
    }
    boost::filesystem2::path dgnfullPath = boost::filesystem2::complete(boost::filesystem2::path(argv[1]));
    boost::filesystem2::path shpDirectoryPath;
    if(argc >= 3)
    {
        shpDirectoryPath = boost::filesystem2::complete(boost::filesystem2::path(argv[2]));
    }
    else
    {
        shpDirectoryPath = dgnfullPath.parent_path();
    }
    if(boost::filesystem2::exists(dgnfullPath) == false)
    {
        LOG_ERROR("Invalid dgn file")
        return 0;
    }
    if(boost::filesystem2::exists(shpDirectoryPath) == false)
    {
        LOG_ERROR("Invalid shp directory path")
        return 0;
    }
    vizCore::CoreRegistry::instance()->registerAll("vizCore");
    vizCore::CoreRegistry::instance()->registerAll("vizElements");

    CORE::RefPtr<CORE::IComponentFactory> cfactory = CORE::CoreRegistry::instance()->getComponentFactory();
    CORE::RefPtr<CORE::IComponent> dataSourceComponent=cfactory->createComponent(*ELEMENTS::ElementsRegistryPlugin::DataSourceComponentType.get(),CORE::UniqueID::CreateUniqueID());
    CORE::RefPtr<vizCore::IBase> base = dataSourceComponent->getInterface<vizCore::IBase>();
    base->setName("DataSourceComponent");
    CORE::WorldMaintainer::instance()->addComponent(dataSourceComponent);
    CORE::RefPtr<CORE::IComponent> iconLoaderComponent=cfactory->createComponent(*ELEMENTS::ElementsRegistryPlugin::IconModelLoaderComponentType.get(),CORE::UniqueID::CreateUniqueID());
    vizCore::RefPtr<vizCore::IBase> base1 = iconLoaderComponent->getInterface<vizCore::IBase>();
    base1->setName("IconModelLoaderComponent");
    CORE::WorldMaintainer::instance()->addComponent(iconLoaderComponent);
    CORE::RefPtr<CORE::IComponent> DGNInfoComponent=cfactory->createComponent(*ELEMENTS::ElementsRegistryPlugin::DGNInfoComponentType.get(),CORE::UniqueID::CreateUniqueID());
    vizCore::RefPtr<vizCore::IBase> base2 = DGNInfoComponent->getInterface<vizCore::IBase>();
    base2->setName("DGNInfoComponent");
    CORE::WorldMaintainer::instance()->addComponent(DGNInfoComponent);
    CORE::RefPtr<CORE::IComponent> featureComponent=cfactory->createComponent(*GIS::GISRegistryPlugin::FeatureComponentType.get(),CORE::UniqueID::CreateUniqueID());
    vizCore::RefPtr<vizCore::IBase> base3 = featureComponent->getInterface<vizCore::IBase>();
    base3->setName("FeatureComponent");
    CORE::WorldMaintainer::instance()->addComponent(featureComponent);
    CORE::RefPtr<CORE::IComponent> c=cfactory->createComponent(*ELEMENTS::ElementsRegistryPlugin::SettingComponentType.get(),CORE::UniqueID::CreateUniqueID());
    vizCore::RefPtr<vizCore::IBase> b = c->getInterface<vizCore::IBase>();
    b->setName("SettingComponent");
    CORE::WorldMaintainer::instance()->addComponent(c);

    CORE::RefPtr<CORE::IWorldFactory> factory = CORE::CoreRegistry::instance()->getWorldFactory();
    vizCore::RefPtr<vizCore::IWorld> world = factory->createWorld(*vizElements::ElementsRegistryPlugin::GenericWorldObjectType);
    CORE::WorldMaintainer::instance()->addWorld(world);
    CORE::RefPtr<DB::ReaderWriter::Options> options = new DB::ReaderWriter::Options();
    CORE::RefPtr<CORE::IObject> iobject = DB::readFeature(dgnfullPath.string(), options, &progressCallback);
    if(!iobject.valid())
    {
        std::string errstring = std::string("Unable to load dgn file ");
        return 0;
    }
    CORE::RefPtr<vizCore::ICompositeObject> icObject = iobject->getInterface<vizCore::ICompositeObject>();
    if(!icObject.valid())
    {
        std::string errorString = "Object is not a valid composite string\n";
        return 0;
    }

    CORE::ICompositeObject::ObjectMap objectList = icObject->getObjectMap();
    int i = 1;
    for(CORE::ICompositeObject::ObjectMap::iterator itr=objectList.begin(); itr!=objectList.end(); ++itr)
    {
        CORE::RefPtr<CORE::IFeatureObject> featureObject = itr->second->getInterface<CORE::IFeatureObject>();
        CORE::RefPtr<CORE::IObject> object = itr->second->getInterface<CORE::IObject>();
        CORE::RefPtr<CORE::IFeatureLayer> coreFeatureLayer = object->getInterface<CORE::IFeatureLayer>();

        std::stringstream stream;
        stream << shpDirectoryPath.string().c_str() << "/shape"<< i << ".shp";
        vizDB::writeFeatureFile(featureObject, stream.str());
        ++i;
    }
    LOG_INFO("Dgn converted successfully")
    return 0;
}
