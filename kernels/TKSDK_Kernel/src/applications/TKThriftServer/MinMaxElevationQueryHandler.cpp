#include "MinMaxElevationQueryHandler.h"

#include <Core/RefPtr.h>
#include <Core/CoreRegistry.h>
#include <Core/CorePlugin.h>
#include <Core/IObjectType.h>
#include <Core/IObjectFactory.h>
#include <Core/IVisitorFactory.h>
#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>
#include <Core/ITerrain.h>
#include <Core/IBaseVisitor.h>
#include <Core/IBase.h>
#include <Core/IPoint.h>

#include <GISCompute/IFilterVisitor.h>
#include <GISCompute/IMinMaxElevCalcVisitor.h>
#include <GISCompute/GISComputePlugin.h>

#include <Util/CoordinateConversionUtils.h>
#include <iostream>
#include <vector>
#include <osg/Array>

namespace thriftcore = ::GeorbIS::Core;

namespace GeoOrbisProcessing
{

    DEFINE_IREFERENCED(MinMaxElevationQueryHandler, CORE::Referenced)

    MinMaxElevationQueryHandler::MinMaxElevationQueryHandler()
    {

    }
    MinMaxElevationQueryHandler::~MinMaxElevationQueryHandler()
    {

    }

    void MinMaxElevationQueryHandler::getMinMaxElevationE(std::vector< ::GeorbIS::Core::Point3D> & _return, const std::vector< ::GeorbIS::Core::Point3D> & points)
    {
        GeorbIS::Core::Point3D minElevationPoint; minElevationPoint.x = 0.0; minElevationPoint.y = 0.0; minElevationPoint.z = 0.0;
        GeorbIS::Core::Point3D maxElevationPoint; maxElevationPoint.x = 0.0; maxElevationPoint.y = 0.0; maxElevationPoint.z = 0.0;
        if(points.size() < 2)
        {
            _return.push_back(minElevationPoint);
            _return.push_back(maxElevationPoint);
            return;
        }
        osg::ref_ptr<osg::Vec2Array> osgArray = new osg::Vec2Array();
        std::vector<GeorbIS::Core::Point3D>::const_iterator it;
        for(it = points.begin(); it != points.end(); ++it)
        {
            const GeorbIS::Core::Point3D& thriftPoint = *it;
            osg::Vec3d ecefPoint(thriftPoint.x, thriftPoint.y, thriftPoint.z);
            osg::Vec3d geodaticPoint = UTIL::CoordinateConversionUtils::ECEFToGeodetic(ecefPoint);
            osg::Vec2 point2d(geodaticPoint.y(), geodaticPoint.x());
            osgArray->push_back(point2d);
        }
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating distance. No world defined", __FILE__, __LINE__)
            _return.push_back(minElevationPoint);
            _return.push_back(maxElevationPoint);
            return;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::ITerrain* terrain = world->getTerrain();
        CORE::IBase* terrainAsBase = terrain->getInterface<CORE::IBase>();
        CORE::RefPtr<CORE::IBaseVisitor> minMaxBaseVisitor = CORE::CoreRegistry::instance()->getVisitorFactory()->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::MinMaxElevCalcVisitorType, NULL);
        CORE::RefPtr<GISCOMPUTE::IMinMaxElevCalcVisitor> minMaxEleCalculator = minMaxBaseVisitor->getInterface<GISCOMPUTE::IMinMaxElevCalcVisitor>();
        minMaxEleCalculator->setExtents(*osgArray);
        minMaxEleCalculator->setMultiThreaded(false);
        minMaxBaseVisitor->apply(*terrainAsBase);
        CORE::RefPtr<CORE::IPoint> minElevationIPoint = minMaxEleCalculator->getMinElevOutput();
        CORE::RefPtr<CORE::IPoint> maxElevationIPoint = minMaxEleCalculator->getMaxElevOutput();
        osg::Vec3d minElevationOSGPoint(minElevationIPoint->getY(), minElevationIPoint->getX(), minElevationIPoint->getZ());
        osg::Vec3d maxElevationOSGPoint(maxElevationIPoint->getY(), maxElevationIPoint->getX(), maxElevationIPoint->getZ());
        osg::Vec3d minElevationECEFPoint = UTIL::CoordinateConversionUtils::GeodeticToECEF(minElevationOSGPoint);
        osg::Vec3d maxElevationECEFPoint = UTIL::CoordinateConversionUtils::GeodeticToECEF(maxElevationOSGPoint);
        minElevationPoint.x = minElevationECEFPoint.x();
        minElevationPoint.y = minElevationECEFPoint.y();
        minElevationPoint.z = minElevationECEFPoint.z();
        maxElevationPoint.x = maxElevationECEFPoint.x();
        maxElevationPoint.y = maxElevationECEFPoint.y();
        maxElevationPoint.z = maxElevationECEFPoint.z();
        _return.push_back(minElevationPoint);
        _return.push_back(maxElevationPoint);
    }

    void MinMaxElevationQueryHandler::getMinMaxElevationG(std::vector< ::GeorbIS::Core::GeoPoint> & _return, const std::vector< ::GeorbIS::Core::GeoPoint> & points)
    {
        GeorbIS::Core::GeoPoint minElevationPoint; minElevationPoint.latitude = 0.0; minElevationPoint.longitude = 0.0; minElevationPoint.altitude = 0.0;
        GeorbIS::Core::GeoPoint maxElevationPoint; maxElevationPoint.latitude = 0.0; maxElevationPoint.longitude = 0.0; maxElevationPoint.altitude = 0.0;
        if(points.size() < 2)
        {
            _return.push_back(minElevationPoint);
            _return.push_back(maxElevationPoint);
            return;
        }
        osg::ref_ptr<osg::Vec2Array> osgArray = new osg::Vec2Array();
        std::vector<GeorbIS::Core::GeoPoint>::const_iterator it;
        for(it = points.begin(); it != points.end(); ++it)
        {
            const GeorbIS::Core::GeoPoint& thriftPoint = *it;
            osg::Vec3d geodaticPoint(thriftPoint.latitude, thriftPoint.longitude, thriftPoint.altitude);
            osg::Vec2 point2d(geodaticPoint.y(), geodaticPoint.x());
            osgArray->push_back(point2d);
        }
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating distance. No world defined", __FILE__, __LINE__)
            _return.push_back(minElevationPoint);
            _return.push_back(maxElevationPoint);
            return;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::ITerrain* terrain = world->getTerrain();
        CORE::IBase* terrainAsBase = terrain->getInterface<CORE::IBase>();
        CORE::RefPtr<CORE::IBaseVisitor> minMaxBaseVisitor = CORE::CoreRegistry::instance()->getVisitorFactory()->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::MinMaxElevCalcVisitorType, NULL);
        CORE::RefPtr<GISCOMPUTE::IMinMaxElevCalcVisitor> minMaxEleCalculator = minMaxBaseVisitor->getInterface<GISCOMPUTE::IMinMaxElevCalcVisitor>();
        minMaxEleCalculator->setExtents(*osgArray);
        minMaxEleCalculator->setMultiThreaded(false);
        minMaxBaseVisitor->apply(*terrainAsBase);
        CORE::RefPtr<CORE::IPoint> minElevationIPoint = minMaxEleCalculator->getMinElevOutput();
        CORE::RefPtr<CORE::IPoint> maxElevationIPoint = minMaxEleCalculator->getMaxElevOutput();
        minElevationPoint.latitude = minElevationIPoint->getY();
        minElevationPoint.longitude = minElevationIPoint->getX();
        minElevationPoint.altitude = minElevationIPoint->getZ();
        maxElevationPoint.latitude = maxElevationIPoint->getY();
        maxElevationPoint.longitude = maxElevationIPoint->getX();
        maxElevationPoint.altitude = maxElevationIPoint->getZ();
        _return.push_back(minElevationPoint);
        _return.push_back(maxElevationPoint);
    }
}
