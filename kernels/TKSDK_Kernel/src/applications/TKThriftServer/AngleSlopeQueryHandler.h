/****************************************************************************
 *
 *
 *  Copyright 2010-2014, VizExpertsIndia Pvt. Ltd. (unpublished)
 *
 *  All rights reserved. This notice is intended as a precaution against
 *  inadvertent publication and does not imply publication or any waiver
 *  of confidentiality. The year included in the foregoing notice is the
 *  year of creation of the work. No part of this work may be used,
 *  reproduced, or transmitted in any form or by any means without the prior
 *  written permission of Vizexperts India Pvt Ltd.
 *
 *
 ***************************************************************************
 */

#ifndef GEORBISPROCESSING_ANGLESLOPEQUERYHANDLER_H
#define GEORBISPROCESSING_ANGLESLOPEQUERYHANDLER_H

#include <Core/Referenced.h>
#include <Core/IBaseUtils.h>

#include "AngleSlopeQuery.h"

namespace GeoOrbisProcessing
{
    class AngleSlopeQueryHandler : public CORE::Referenced, public GeorbIS::Processing::AngleSlopeQueryIf
    {
        DECLARE_IREFERENCED
    public:
        AngleSlopeQueryHandler();
        virtual ~AngleSlopeQueryHandler();
        double getAngleBetweenPoints(const  ::GeorbIS::Core::GeoPoint& point1, const  ::GeorbIS::Core::GeoPoint& point2);
        double getSlopeBetweenPoints(const  ::GeorbIS::Core::GeoPoint& point1, const  ::GeorbIS::Core::GeoPoint& point2);
    };
}

#endif
