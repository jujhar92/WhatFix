#include "ElevationQueryHandler.h"

#include <Core/RefPtr.h>
#include <Core/CoreRegistry.h>
#include <Core/CorePlugin.h>
#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>
#include <Core/ITerrain.h>

#include <Util/CoordinateConversionUtils.h>

namespace thriftcore = ::GeorbIS::Core;

namespace GeoOrbisProcessing
{
    DEFINE_IREFERENCED(ElevationQueryHandler, CORE::Referenced)

    ElevationQueryHandler::ElevationQueryHandler()
    {

    }

    ElevationQueryHandler::~ElevationQueryHandler()
    {

    }

    double ElevationQueryHandler::getElevationAtPointE(const  ::GeorbIS::Core::Point3D& point)
    {
        osg::Vec3d ecefpoint(point.x, point.y, point.z);
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error in getting Elevation. No World loaded", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::IWorld* world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::ITerrain* terrain = world->getTerrain();
        osg::Vec3d geoPoint = UTIL::CoordinateConversionUtils::ECEFToGeodetic(ecefpoint);
        osg::Vec2d lonLat(geoPoint.y(), geoPoint.x());
        double elevation = 0.0;
        terrain->getElevationAt(lonLat, elevation);
        return elevation;
    }

    double ElevationQueryHandler::getElevationAtPointG(const  ::GeorbIS::Core::GeoPoint& point)
    {
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error in getting Elevation. No World loaded", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::IWorld* world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::ITerrain* terrain = world->getTerrain();
        osg::Vec2d lonLat(point.longitude, point.latitude);
        double elevation = 0.0;
        terrain->getElevationAt(lonLat, elevation);
        return elevation;
    }

    void ElevationQueryHandler::getElevationsAtPointsE(std::vector<double> & _return, const std::vector< ::GeorbIS::Core::Point3D> & points)
    {
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error in getting Elevation. No World loaded", __FILE__, __LINE__)
            return;
        }
        CORE::IWorld* world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::ITerrain* terrain = world->getTerrain();
        std::vector<thriftcore::Point3D>::const_iterator itr;
        for(itr = points.begin(); itr != points.end(); ++itr)
        {
            const thriftcore::Point3D& point = *itr;
            osg::Vec3d ecefPoint(point.x, point.y, point.z);
            osg::Vec3d geoPoint = UTIL::CoordinateConversionUtils::ECEFToGeodetic(ecefPoint);
            osg::Vec2d lonLat(geoPoint.y(), geoPoint.x());
            double elevation = 0.0;
            terrain->getElevationAt(lonLat, elevation);
            _return.push_back(elevation);
        }
    }

    void ElevationQueryHandler::getElevationsAtPointsG(std::vector<double> & _return, const std::vector< ::GeorbIS::Core::GeoPoint> & points)
    {
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error in getting Elevation. No World loaded", __FILE__, __LINE__)
            return;
        }
        CORE::IWorld* world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::ITerrain* terrain = world->getTerrain();
        std::vector<thriftcore::GeoPoint>::const_iterator itr;
        for(itr = points.begin(); itr != points.end(); ++itr)
        {
            const thriftcore::GeoPoint& point = *itr;
            osg::Vec2d lonLat(point.longitude, point.latitude);
            double elevation = 0.0;
            terrain->getElevationAt(lonLat, elevation);
            _return.push_back(elevation);
        }
    }

}
