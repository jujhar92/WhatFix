/****************************************************************************
 *
 *
 *  Copyright 2010-2014, VizExpertsIndia Pvt. Ltd. (unpublished)
 *
 *  All rights reserved. This notice is intended as a precaution against
 *  inadvertent publication and does not imply publication or any waiver
 *  of confidentiality. The year included in the foregoing notice is the
 *  year of creation of the work. No part of this work may be used,
 *  reproduced, or transmitted in any form or by any means without the prior
 *  written permission of Vizexperts India Pvt Ltd.
 *
 *
 ***************************************************************************
 */

#ifndef GEORBISPROCESSING_MEASUREMENTQUERYHANDLER_H
#define GEORBISPROCESSING_MEASUREMENTQUERYHANDLER_H

#include <Core/Referenced.h>
#include <Core/IBaseUtils.h>
#include <Core/Base.h>

#include "MeasurementQuery.h"

namespace GeoOrbisProcessing
{
    class MeasurementQueryHandler : public CORE::Referenced, public GeorbIS::Processing::MeasurementQueryIf
    {
        DECLARE_IREFERENCED

    public:
        MeasurementQueryHandler();
        ~MeasurementQueryHandler();
        double getAerielDistanceE(const std::vector< ::GeorbIS::Core::Point3D> & points);
        double getAeirelDistanceG(const std::vector< ::GeorbIS::Core::GeoPoint> & points);
        double getProjectedDistanceE(const std::vector< ::GeorbIS::Core::Point3D> & points);
        double getProjectedDistanceG(const std::vector< ::GeorbIS::Core::GeoPoint> & points);
        double getAerielAreaE(const std::vector< ::GeorbIS::Core::Point3D> & points);
        double getAerielAreaG(const std::vector< ::GeorbIS::Core::GeoPoint> & points);
        double getProjectedAreaE(const std::vector< ::GeorbIS::Core::Point3D> & points);
        double getProjectedAreaG(const std::vector< ::GeorbIS::Core::GeoPoint> & points);

    };
}

#endif
