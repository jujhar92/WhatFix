#include <Core/CoreRegistry.h>
#include <Core/CorePlugin.h>
#include <Core/IWorldFactory.h>
#include <Core/IWorld.h>
#include <Core/ITerrain.h>
#include <Core/IBase.h>
#include <Core/IObserver.h>
#include <Core/IObject.h>
#include <Core/WorldMaintainer.h>

#include <Elements/ElementsPlugin.h>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/TProcessor.h>
#include <thrift/processor/TMultiplexedProcessor.h>

#include "MeasurementQueryHandler.h"
#include "ElevationQueryHandler.h"
#include "MinMaxElevationQueryHandler.h"
#include "ElevationDifferenceQueryHandler.h"
#include "AngleSlopeQueryHandler.h"
#include "WorldManagerHandler.h"

#include <Terrain/IElevationObject.h>
#include <Terrain/TerrainPlugin.h>
#include <GISCompute/IDistanceCalculationVisitor.h>
#include <GISCompute/GISComputePlugin.h>
#include <GISCompute/IFilterVisitor.h>

namespace thrift = apache::thrift;

int main(int argc, char* argv[])
{
    vizCore::CoreRegistry::instance()->registerAll("vizCore");
    vizCore::CoreRegistry::instance()->registerAll("vizUtil");
    vizCore::CoreRegistry::instance()->registerAll("vizElements");
    vizCore::CoreRegistry::instance()->registerAll("vizTerrain");
    vizCore::CoreRegistry::instance()->registerAll("vizGISCompute");
    CORE::RefPtr<CORE::IWorld> world = CORE::CoreRegistry::instance()->getWorldFactory()->createWorld(*ELEMENTS::ElementsRegistryPlugin::GenericWorldObjectType);
    CORE::WorldMaintainer::instance()->addWorld(world);
    CORE::ITerrain* terrain = world->getTerrain();
    CORE::IObserver* observer = dynamic_cast<CORE::IObserver*>(terrain);
    CORE::RefPtr<CORE::IMessage> message = CORE::CoreRegistry::instance()->getMessageFactory()->createMessage(*CORE::IWorld::WorldLoadedMessageType);
    observer->update(*CORE::IWorld::WorldLoadedMessageType, *message);

    boost::shared_ptr<thrift::TMultiplexedProcessor> processor(new thrift::TMultiplexedProcessor());

    boost::shared_ptr<GeoOrbisProcessing::MeasurementQueryHandler> measurementHandler(new GeoOrbisProcessing::MeasurementQueryHandler());
    boost::shared_ptr<thrift::server::TProcessor> measurementProcessor(new GeorbIS::Processing::MeasurementQueryProcessor(measurementHandler));
    processor->registerProcessor("MeasurementQuery", measurementProcessor);

    boost::shared_ptr<GeoOrbisProcessing::ElevationQueryHandler> elevationHandler(new GeoOrbisProcessing::ElevationQueryHandler());
    boost::shared_ptr<thrift::server::TProcessor> elevationProcessor(new GeorbIS::Processing::ElevationQueryProcessor(elevationHandler));
    processor->registerProcessor("ElevationQuery", elevationProcessor);

    boost::shared_ptr<GeoOrbisProcessing::MinMaxElevationQueryHandler> minMaxElevationHandler(new GeoOrbisProcessing::MinMaxElevationQueryHandler());
    boost::shared_ptr<thrift::server::TProcessor> minMaxElevationProcessor(new GeorbIS::Processing::MinMaxElevationQueryProcessor(minMaxElevationHandler));
    processor->registerProcessor("MinMaxElevationQuery", minMaxElevationProcessor);

    boost::shared_ptr<GeoOrbisProcessing::ElevationDifferenceQueryHandler> elevationDifferenceHandler(new GeoOrbisProcessing::ElevationDifferenceQueryHandler());
    boost::shared_ptr<thrift::server::TProcessor> elevationDifferenceProcessor(new GeorbIS::Processing::ElevationDifferenceQueryProcessor(elevationDifferenceHandler));
    processor->registerProcessor("ElevationDifferenceQuery", elevationDifferenceProcessor);

    boost::shared_ptr<GeoOrbisProcessing::AngleSlopeQueryHandler> angleSlopeHandler(new GeoOrbisProcessing::AngleSlopeQueryHandler());
    boost::shared_ptr<thrift::server::TProcessor> angleSlopeProcessor(new GeorbIS::Processing::AngleSlopeQueryProcessor(angleSlopeHandler));
    processor->registerProcessor("AngleSlopeQuery", angleSlopeProcessor);

    boost::shared_ptr<GeoOrbisProcessing::WorldManagerHandler> worldManagerHandler(new GeoOrbisProcessing::WorldManagerHandler());
    boost::shared_ptr<thrift::server::TProcessor> worldManagerProcessor(new GeorbIS::Processing::WorldManagerProcessor(worldManagerHandler));
    processor->registerProcessor("WorldManager", worldManagerProcessor);

    int port = 9003;
    boost::shared_ptr<thrift::transport::TServerTransport> serverTransport(new thrift::transport::TServerSocket(port));
    boost::shared_ptr<thrift::transport::TTransportFactory> transportFactory(new thrift::transport::TBufferedTransportFactory());
    boost::shared_ptr<thrift::protocol::TProtocolFactory> protocolFactory(new thrift::protocol::TBinaryProtocolFactory());
    thrift::server::TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);
    LOG_INFO("Server Starting");
    server.serve();
    LOG_INFO("Server Exiting");
    return 0;
}
