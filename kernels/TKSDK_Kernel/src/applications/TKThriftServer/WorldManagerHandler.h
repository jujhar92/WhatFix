/****************************************************************************
 *
 *
 *  Copyright 2010-2014, VizExpertsIndia Pvt. Ltd. (unpublished)
 *
 *  All rights reserved. This notice is intended as a precaution against
 *  inadvertent publication and does not imply publication or any waiver
 *  of confidentiality. The year included in the foregoing notice is the
 *  year of creation of the work. No part of this work may be used,
 *  reproduced, or transmitted in any form or by any means without the prior
 *  written permission of Vizexperts India Pvt Ltd.
 *
 *
 ***************************************************************************
 */

#ifndef GEORBISPROCESSING_WORLDMANAGERSERVICE_H
#define GEORBISPROCESSING_WORLDMANAGERSERVICE_H

#include <Core/Referenced.h>
#include <Core/IBaseUtils.h>
#include <Core/Base.h>

#include "WorldManager.h"

namespace GeoOrbisProcessing
{
    class WorldManagerHandler : public CORE::Referenced, public GeorbIS::Processing::WorldManagerIf
    {
        DECLARE_IREFERENCED
    public:
        WorldManagerHandler();
        virtual ~WorldManagerHandler();
        bool addNewWorld();
        void removeCurrentWorld();
        bool addRasterLayer(const std::string& url);
        bool addElevationLayer(const std::string& url);
        bool addVectorLayer(const std::string& url);
        bool removeLayer(const std::string& url);
        void removeAllLayer();
    };
}

#endif
