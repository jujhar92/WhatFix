/****************************************************************************
 *
 *
 *  Copyright 2010-2014, VizExpertsIndia Pvt. Ltd. (unpublished)
 *
 *  All rights reserved. This notice is intended as a precaution against
 *  inadvertent publication and does not imply publication or any waiver
 *  of confidentiality. The year included in the foregoing notice is the
 *  year of creation of the work. No part of this work may be used,
 *  reproduced, or transmitted in any form or by any means without the prior
 *  written permission of Vizexperts India Pvt Ltd.
 *
 *
 ***************************************************************************
 */

#ifndef GEORBISPROCESSING_MINMAXELEVATIONQUERYHANDLER_H
#define GEORBISPROCESSING_MINMAXELEVATIONQUERYHANDLER_H

#include <Core/Referenced.h>
#include <Core/IBaseUtils.h>
#include <Core/Base.h>

#include "MinMaxElevationQuery.h"

namespace GeoOrbisProcessing
{
    class MinMaxElevationQueryHandler : public CORE::Referenced, public GeorbIS::Processing::MinMaxElevationQueryIf
    {
        DECLARE_IREFERENCED
    public:
        MinMaxElevationQueryHandler();
        ~MinMaxElevationQueryHandler();
        void getMinMaxElevationE(std::vector< ::GeorbIS::Core::Point3D> & _return, const std::vector< ::GeorbIS::Core::Point3D> & points);
        void getMinMaxElevationG(std::vector< ::GeorbIS::Core::GeoPoint> & _return, const std::vector< ::GeorbIS::Core::GeoPoint> & points);
    };
}

#endif
