SET(EXE_NAME TKThriftServer)

SET(LIB_HEADERS ElevationQueryHandler.h
                MeasurementQueryHandler.h
                MinMaxElevationQueryHandler.h
                ElevationDifferenceQueryHandler.h
                WorldManagerHandler.h
                AngleSlopeQueryHandler.h
   )

SET(LIB_SOURCES main.cpp
                ElevationQueryHandler.cpp
                MeasurementQueryHandler.cpp
                MinMaxElevationQueryHandler.cpp
                ElevationDifferenceQueryHandler.cpp
                WorldManagerHandler.cpp
                AngleSlopeQueryHandler.cpp
   )

SET(THRIFT_FILE_PATH $ENV{THRIFT_API_GEN_DIR}/cpp)
file(GLOB THRIFT_OUTPUT_SOURCES
    ${THRIFT_FILE_PATH}/*.cpp
)
foreach(TMP_PATH ${THRIFT_OUTPUT_SOURCES})
    string(FIND ${TMP_PATH} "skeleton" TMP_FOUND)
    if(NOT ${TMP_FOUND} EQUAL -1)
        list(REMOVE_ITEM THRIFT_OUTPUT_SOURCES ${TMP_PATH})
    endif()
endforeach(TMP_PATH)

SET(THRIFT_OUTPUT
    ${THRIFT_OUTPUT_SOURCES}
    )

SET(TARGET_SRC ${LIB_HEADERS} ${LIB_SOURCES} ${THRIFT_OUTPUT})

INCLUDE_DIRECTORIES(${TERRAINSDK_INCLUDE_DIR}
                    ${THRIFT_INCLUDE_DIR}
                    ${THRIFT_FILE_PATH}
                    ${BOOST_INCLUDE_DIR}
                    ${OSG_INCLUDE_DIR}
                    ${OSG_EARTH_INCLUDE_DIR}
                    )

BUILD_EXE_EXAMPLE(${EXE_NAME}) 

LINK_WITH_VARIABLES(${EXE_NAME}
                    TKCORE_LIBRARY
                    TKELEMENTS_LIBRARY
                    TKGISCOMPUTE_LIBRARY
                    TKTERRAIN_LIBRARY
                    TKAPP_LIBRARY
                    TKUTIL_LIBRARY
                    OSG_LIBRARY
                    THRIFT_LIBRARY
                    THRIFTZ_LIBRARY
                    )
