#include "WorldManagerHandler.h"

#include <Core/RefPtr.h>
#include <Core/CoreRegistry.h>
#include <Core/CorePlugin.h>
#include <Core/IObjectType.h>
#include <Core/IObjectFactory.h>
#include <Core/IVisitorFactory.h>
#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>
#include <Core/ITerrain.h>
#include <Core/IBaseVisitor.h>
#include <Core/IBase.h>
#include <Core/IPoint.h>

#include <Elements/ElementsPlugin.h>

#include <Terrain/IRasterObject.h>
#include <Terrain/IElevationObject.h>
#include <Terrain/IVectorObject.h>
#include <Terrain/TerrainPlugin.h>

#include <GISCompute/IFilterVisitor.h>
#include <GISCompute/IMinMaxElevCalcVisitor.h>
#include <GISCompute/GISComputePlugin.h>

#include <Util/CoordinateConversionUtils.h>
#include <iostream>
#include <vector>
#include <osg/Array>

namespace thriftcore = ::GeorbIS::Core;

namespace GeoOrbisProcessing
{
    DEFINE_IREFERENCED(WorldManagerHandler, CORE::Referenced)

    WorldManagerHandler::WorldManagerHandler()
    {

    }

    WorldManagerHandler::~WorldManagerHandler()
    {

    }

    bool WorldManagerHandler::addNewWorld()
    {
        CORE::RefPtr<CORE::IWorld> world = CORE::CoreRegistry::instance()->getWorldFactory()->createWorld(*ELEMENTS::ElementsRegistryPlugin::GenericWorldObjectType);
        CORE::WorldMaintainer::instance()->addWorld(world);
        return true;
    }

    void WorldManagerHandler::removeCurrentWorld()
    {
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1) return;
        CORE::WorldMaintainer::instance()->removeWorldByName("ELEMENTS::IWORLD");
    }

    bool WorldManagerHandler::addRasterLayer(const std::string& url)
    {
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Cannot add raster! No world available", __FILE__, __LINE__);
            return false;
        }
        CORE::RefPtr<CORE::IObject> rasterObject = CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::RasterObjectType);
        CORE::RefPtr<TERRAIN::IRasterObject> raster = rasterObject->getInterface<TERRAIN::IRasterObject>();
        raster->setURL(url);
        CORE::IWorld* world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        world->addObject(rasterObject);
        return true;
    }

    bool WorldManagerHandler::addElevationLayer(const std::string& url)
    {
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Cannot add elevation! No world available", __FILE__, __LINE__);
            return false;
        }
        CORE::RefPtr<CORE::IObject> elevationObject = CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::ElevationObjectType);
        CORE::RefPtr<TERRAIN::IElevationObject> elevation = elevationObject->getInterface<TERRAIN::IElevationObject>();
        elevation->setURL(url);
        CORE::IWorld* world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        world->addObject(elevationObject);
        return true;
    }

    bool WorldManagerHandler::addVectorLayer(const std::string& url)
    {
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Cannot add vector! No world available", __FILE__, __LINE__);
            return false;
        }
        CORE::RefPtr<CORE::IObject> vectorObject = CORE::CoreRegistry::instance()->getObjectFactory()->createObject(*TERRAIN::TerrainRegistryPlugin::ElevationObjectType);
        CORE::RefPtr<TERRAIN::IVectorObject> vector = vectorObject->getInterface<TERRAIN::IVectorObject>();
        vector->setURL(url);
        CORE::IWorld* world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        world->addObject(vectorObject);
        return true;
    }

    bool WorldManagerHandler::removeLayer(const std::string& url)
    {
        return false;
    }

    void WorldManagerHandler::removeAllLayer()
    {
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1) return;
        //CORE::IWorld* world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;

    }
}
