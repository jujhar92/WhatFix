#include "AngleSlopeQueryHandler.h"

#include <Core/RefPtr.h>
#include <Core/CoreRegistry.h>
#include <Core/CorePlugin.h>
#include <Core/IObjectType.h>
#include <Core/IObjectFactory.h>
#include <Core/IVisitorFactory.h>
#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>
#include <Core/ITerrain.h>
#include <Core/IBaseVisitor.h>
#include <Core/IBase.h>
#include <Core/IPoint.h>

#include <Util/CoordinateConversionUtils.h>

namespace GeoOrbisProcessing
{
    DEFINE_IREFERENCED(AngleSlopeQueryHandler, CORE::Referenced)

    AngleSlopeQueryHandler::AngleSlopeQueryHandler()
    {

    }

    AngleSlopeQueryHandler::~AngleSlopeQueryHandler()
    {

    }

    double AngleSlopeQueryHandler::getAngleBetweenPoints(const  ::GeorbIS::Core::GeoPoint& point1, const  ::GeorbIS::Core::GeoPoint& point2)
    {
        osg::Vec3d point1Geodatic(point1.latitude, point1.longitude, point1.altitude);
        osg::Vec3d point2Geodatic(point2.latitude, point2.longitude, point2.altitude);
        const osg::Vec3d& point1InECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(point1Geodatic);
        const osg::Vec3d& point2InECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(point2Geodatic);
        osg::Vec3d Point1toPoint2Vector = point2InECEF - point1InECEF;
        Point1toPoint2Vector.normalize();
        CORE::RefPtr<UTIL::ENUToECEFConverter> converter(new UTIL::ENUToECEFConverter());
        converter->setOrigin(point1Geodatic, true);
        osg::Vec3d northVector = converter->convert(osg::Vec3d(0.0,1.0,0.0));
        northVector.normalize();
        double angleBetweenVector = acos(Point1toPoint2Vector * northVector);
        return angleBetweenVector;
    }

    double AngleSlopeQueryHandler::getSlopeBetweenPoints(const  ::GeorbIS::Core::GeoPoint& point1, const  ::GeorbIS::Core::GeoPoint& point2)
    {
        osg::Vec3d point1Geodatic(point1.latitude, point1.longitude, point1.altitude);
        osg::Vec3d point2Geodatic(point2.latitude, point2.longitude, point2.altitude);
        osg::Vec3d point2ClampedInGeodatic(point2.latitude, point2.longitude, point1.altitude);
        const osg::Vec3d& point1InECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(point1Geodatic);
        const osg::Vec3d& point2InECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(point2Geodatic);
        const osg::Vec3d& point2ClampedInECEF = UTIL::CoordinateConversionUtils::GeodeticToECEF(point2ClampedInGeodatic);
        osg::Vec3d point1toPoint2Vector = (point2InECEF - point1InECEF);
        point1toPoint2Vector.normalize();
        osg::Vec3d point1toPoint2ClampedVector = (point2ClampedInECEF - point1InECEF);
        point1toPoint2ClampedVector.normalize();
        double angleBetweenVector = acos(point1toPoint2Vector * point1toPoint2ClampedVector);
        return angleBetweenVector;
    }
}
