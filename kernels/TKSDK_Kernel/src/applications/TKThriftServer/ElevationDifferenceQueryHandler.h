/****************************************************************************
 *
 *
 *  Copyright 2010-2014, VizExpertsIndia Pvt. Ltd. (unpublished)
 *
 *  All rights reserved. This notice is intended as a precaution against
 *  inadvertent publication and does not imply publication or any waiver
 *  of confidentiality. The year included in the foregoing notice is the
 *  year of creation of the work. No part of this work may be used,
 *  reproduced, or transmitted in any form or by any means without the prior
 *  written permission of Vizexperts India Pvt Ltd.
 *
 *
 ***************************************************************************
 */

#ifndef GEORBISPROCESSING_ELEVATIONDIFFERENCEQUERYHANDLER_H
#define GEORBISPROCESSING_ELEVATIONDIFFERENCEQUERYHANDLER_H

#include <Core/Referenced.h>
#include <Core/IBaseUtils.h>
#include <Core/Base.h>

#include "ElevationDifferenceQuery.h"

namespace GeoOrbisProcessing
{
    class ElevationDifferenceQueryHandler : public CORE::Referenced, public GeorbIS::Processing::ElevationDifferenceQueryIf
    {
        DECLARE_IREFERENCED

    public:
        ElevationDifferenceQueryHandler();
        ~ElevationDifferenceQueryHandler();
        double getElevationDifference(const std::vector< ::GeorbIS::Core::GeoPoint> & points);

    };
}

#endif
