/****************************************************************************
 *
 *
 *  Copyright 2010-2014, VizExpertsIndia Pvt. Ltd. (unpublished)
 *
 *  All rights reserved. This notice is intended as a precaution against
 *  inadvertent publication and does not imply publication or any waiver
 *  of confidentiality. The year included in the foregoing notice is the
 *  year of creation of the work. No part of this work may be used,
 *  reproduced, or transmitted in any form or by any means without the prior
 *  written permission of Vizexperts India Pvt Ltd.
 *
 *
 ***************************************************************************
 */

#ifndef GEORBISPROCESSING_ELEVATIONQUERYHANDLER_H
#define GEORBISPROCESSING_ELEVATIONQUERYHANDLER_H

#include <Core/Referenced.h>
#include <Core/IBaseUtils.h>

#include "ElevationQuery.h"

namespace GeoOrbisProcessing
{
    class ElevationQueryHandler : public CORE::Referenced, public GeorbIS::Processing::ElevationQueryIf
    {
        DECLARE_IREFERENCED
    public:
        ElevationQueryHandler();
        ~ElevationQueryHandler();
        double getElevationAtPointE(const  ::GeorbIS::Core::Point3D& point);
        double getElevationAtPointG(const  ::GeorbIS::Core::GeoPoint& point);
        void getElevationsAtPointsE(std::vector<double> & _return, const std::vector< ::GeorbIS::Core::Point3D> & points);
        void getElevationsAtPointsG(std::vector<double> & _return, const std::vector< ::GeorbIS::Core::GeoPoint> & points);
    };
}

#endif
