#include "MeasurementQueryHandler.h"

#include <Core/RefPtr.h>
#include <Core/CoreRegistry.h>
#include <Core/CorePlugin.h>
#include <Core/IObjectType.h>
#include <Core/IObjectFactory.h>
#include <Core/IVisitorFactory.h>
#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>
#include <Core/ITerrain.h>
#include <Core/IBaseVisitor.h>
#include <Core/IBase.h>

#include <GISCompute/IFilterVisitor.h>
#include <GISCompute/IDistanceCalculationVisitor.h>
#include <GISCompute/ISurfaceAreaCalcVisitor.h>
#include <GISCompute/GISComputePlugin.h>

#include <Util/CoordinateConversionUtils.h>
#include <iostream>
namespace thriftcore = ::GeorbIS::Core;

namespace GeoOrbisProcessing
{
    DEFINE_IREFERENCED(MeasurementQueryHandler, CORE::Referenced)

    MeasurementQueryHandler::MeasurementQueryHandler()
    {

    }

    MeasurementQueryHandler::~MeasurementQueryHandler()
    {

    }

    double MeasurementQueryHandler::getAerielDistanceE(const std::vector< ::GeorbIS::Core::Point3D> & points)
    {
        if(points.size() <= 1)
        {
            return 0.0;
        }
        std::vector<thriftcore::Point3D>::const_iterator constItr;
        osg::ref_ptr<osg::Vec3dArray> osgArray = new osg::Vec3dArray();
        for(constItr = points.begin(); constItr != points.end(); ++constItr)
        {
            const thriftcore::Point3D& point = *constItr;
            osg::Vec3d pointInGeo = UTIL::CoordinateConversionUtils::ECEFToGeodetic(osg::Vec3d(point.x, point.y, point.z));
            osg::Vec3d pointInLonLat = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(pointInGeo);
            osgArray->push_back(pointInLonLat);
        }
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating distance. No world defined", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::ITerrain* terrain = world->getTerrain();
        CORE::IBase* terrainAsBase = terrain->getInterface<CORE::IBase>();
        CORE::RefPtr<CORE::IBaseVisitor> distanceBaseVisitor = CORE::CoreRegistry::instance()->getVisitorFactory()->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::DistanceCalculationVisitorType, NULL);
        CORE::RefPtr<GISCOMPUTE::IDistanceCalculationVisitor> distanceVisitor = distanceBaseVisitor->getInterface<GISCOMPUTE::IDistanceCalculationVisitor>();
        distanceVisitor->setComputationType(GISCOMPUTE::IDistanceCalculationVisitor::AERIAL);
        distanceVisitor->setPoints(osgArray);
        distanceBaseVisitor->apply(*terrainAsBase);
#ifdef _WIN32
        Sleep(1000);
#else
        usleep(1000000);
#endif
        CORE::RefPtr<GISCOMPUTE::IFilterVisitor> filterVisitor = distanceVisitor->getInterface<GISCOMPUTE::IFilterVisitor>();
        while(filterVisitor->getFilterStatus() == GISCOMPUTE::FILTER_PENDING)
        {
#ifdef _WIN32
            Sleep(1000);
#else
            usleep(1000000);
#endif
        }
        double distance = distanceVisitor->getDistance();
        return distance;
    }

    double MeasurementQueryHandler::getAeirelDistanceG(const std::vector< ::GeorbIS::Core::GeoPoint> & points)
    {
        if(points.size() <= 1)
        {
            return 0.0;
        }
        std::vector<thriftcore::GeoPoint>::const_iterator constItr;
        osg::ref_ptr<osg::Vec3dArray> osgArray = new osg::Vec3dArray();
        for(constItr = points.begin(); constItr != points.end(); ++constItr)
        {
            const thriftcore::GeoPoint& point = *constItr;
            osg::Vec3d pointInLonLat = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight
                    (osg::Vec3d(point.latitude, point.longitude, point.altitude));
            osgArray->push_back(pointInLonLat);
        }
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating distance. No world defined", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::ITerrain* terrain = world->getTerrain();
        CORE::IBase* terrainAsBase = terrain->getInterface<CORE::IBase>();
        CORE::RefPtr<CORE::IBaseVisitor> distanceBaseVisitor = CORE::CoreRegistry::instance()->getVisitorFactory()->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::DistanceCalculationVisitorType);
        CORE::RefPtr<GISCOMPUTE::IDistanceCalculationVisitor> distanceVisitor = distanceBaseVisitor->getInterface<GISCOMPUTE::IDistanceCalculationVisitor>();
        distanceVisitor->setComputationType(GISCOMPUTE::IDistanceCalculationVisitor::AERIAL);
        distanceVisitor->setPoints(osgArray);
        distanceBaseVisitor->apply(*terrainAsBase);
#ifdef _WIN32
        Sleep(1000);
#else
        usleep(1000000);
#endif
        CORE::RefPtr<GISCOMPUTE::IFilterVisitor> filterVisitor = distanceVisitor->getInterface<GISCOMPUTE::IFilterVisitor>();
        while(filterVisitor->getFilterStatus() == GISCOMPUTE::FILTER_PENDING)
        {
#ifdef _WIN32
            Sleep(1000);
#else
            usleep(1000000);
#endif
        }
        double distance = distanceVisitor->getDistance();
        return distance;
    }

    double MeasurementQueryHandler::getProjectedDistanceE(const std::vector< ::GeorbIS::Core::Point3D> & points)
    {
        if(points.size() <= 1)
        {
            return 0.0;
        }
        std::vector<thriftcore::Point3D>::const_iterator constItr;
        osg::ref_ptr<osg::Vec3dArray> osgArray = new osg::Vec3dArray();
        for(constItr = points.begin(); constItr != points.end(); ++constItr)
        {
            const thriftcore::Point3D& point = *constItr;
            osg::Vec3d pointInGeo = UTIL::CoordinateConversionUtils::ECEFToGeodetic(osg::Vec3d(point.x, point.y, point.z));
            osg::Vec3d pointInLonLat = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(pointInGeo);
            osgArray->push_back(pointInLonLat);
        }
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating distance. No world defined", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::ITerrain* terrain = world->getTerrain();
        CORE::IBase* terrainAsBase = terrain->getInterface<CORE::IBase>();
        CORE::RefPtr<CORE::IBaseVisitor> distanceBaseVisitor = CORE::CoreRegistry::instance()->getVisitorFactory()->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::DistanceCalculationVisitorType, NULL);
        CORE::RefPtr<GISCOMPUTE::IDistanceCalculationVisitor> distanceVisitor = distanceBaseVisitor->getInterface<GISCOMPUTE::IDistanceCalculationVisitor>();
        distanceVisitor->setComputationType(GISCOMPUTE::IDistanceCalculationVisitor::PROJECTED);
        distanceVisitor->setPoints(osgArray);
        distanceBaseVisitor->apply(*terrainAsBase);
#ifdef _WIN32
        Sleep(1000);
#else
        usleep(1000000);
#endif
        CORE::RefPtr<GISCOMPUTE::IFilterVisitor> filterVisitor = distanceVisitor->getInterface<GISCOMPUTE::IFilterVisitor>();
        while(filterVisitor->getFilterStatus() == GISCOMPUTE::FILTER_PENDING)
        {
#ifdef _WIN32
            Sleep(1000);
#else
            usleep(1000000);
#endif
        }
        double distance = distanceVisitor->getDistance();
        return distance;
    }

    double MeasurementQueryHandler::getProjectedDistanceG(const std::vector< ::GeorbIS::Core::GeoPoint> & points)
    {
        if(points.size() <= 1)
        {
            return 0.0;
        }
        std::vector<thriftcore::GeoPoint>::const_iterator constItr;
        osg::ref_ptr<osg::Vec3dArray> osgArray = new osg::Vec3dArray();
        for(constItr = points.begin(); constItr != points.end(); ++constItr)
        {
            const thriftcore::GeoPoint& point = *constItr;
            osg::Vec3d pointInLonLat = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight
                    (osg::Vec3d(point.latitude, point.longitude, point.altitude));
            osgArray->push_back(pointInLonLat);
        }
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating distance. No world defined", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::RefPtr<CORE::ITerrain> terrain = world->getTerrain();
        CORE::RefPtr<CORE::IBase> terrainAsBase = terrain->getInterface<CORE::IBase>();
        CORE::IBaseVisitor* distanceBaseVisitor = CORE::CoreRegistry::instance()->getVisitorFactory()->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::DistanceCalculationVisitorType);
        GISCOMPUTE::IDistanceCalculationVisitor* distanceVisitor = distanceBaseVisitor->getInterface<GISCOMPUTE::IDistanceCalculationVisitor>();
        distanceVisitor->setComputationType(GISCOMPUTE::IDistanceCalculationVisitor::PROJECTED);
        distanceVisitor->setPoints(osgArray);
        distanceBaseVisitor->apply(*terrainAsBase);
#ifdef _WIN32
        Sleep(1000);
#else
        usleep(1000000);
#endif
        GISCOMPUTE::IFilterVisitor* filterVisitor = distanceVisitor->getInterface<GISCOMPUTE::IFilterVisitor>();
        while(filterVisitor->getFilterStatus() == GISCOMPUTE::FILTER_PENDING)
        {
#ifdef _WIN32
            Sleep(1000);
#else
            usleep(1000000);
#endif
        }
        double distance = distanceVisitor->getDistance();
        return distance;
    }

    double MeasurementQueryHandler::getAerielAreaE(const std::vector< ::GeorbIS::Core::Point3D> & points)
    {
        if(points.size() <= 2)
        {
            return 0.0;
        }
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating area. No world defined", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        std::vector<thriftcore::Point3D>::const_iterator constItr;
        osg::ref_ptr<osg::Vec3dArray> pointArray = new osg::Vec3dArray();
        for(constItr = points.begin(); constItr != points.end(); ++constItr)
        {
            const thriftcore::Point3D& point = *constItr;
            osg::Vec3d geoPoint = UTIL::CoordinateConversionUtils::ECEFToGeodetic(osg::Vec3d(point.x, point.y, point.z));
            osg::Vec3d lonLatPoint = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(geoPoint);
            pointArray->push_back(lonLatPoint);
        }
        CORE::RefPtr<CORE::IBaseVisitor> areaBaseVisitor = CORE::CoreRegistry::instance()->getVisitorFactory()->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::SurfaceAreaCalcVisitorType);
        CORE::RefPtr<GISCOMPUTE::ISurfaceAreaCalcVisitor> areaVisitor = areaBaseVisitor->getInterface<GISCOMPUTE::ISurfaceAreaCalcVisitor>();
        areaVisitor->setPoints(pointArray);
        areaVisitor->setComputationType(GISCOMPUTE::ISurfaceAreaCalcVisitor::AERIAL);
        CORE::RefPtr<GISCOMPUTE::IFilterVisitor> areaFilterVisitor = areaBaseVisitor->getInterface<GISCOMPUTE::IFilterVisitor>();
        CORE::ITerrain* terrain = world->getTerrain();
        CORE::IBase* terrainAsBase = terrain->getInterface<CORE::IBase>();
        areaBaseVisitor->apply(*terrainAsBase);
#ifdef _WIN32
        Sleep(1000);
#else
        usleep(1000000);
#endif
        while(areaFilterVisitor->getFilterStatus() == GISCOMPUTE::FILTER_PENDING)
        {
#ifdef _WIN32
            Sleep(1000);
#else
            usleep(1000000);
#endif
        }
        double area = areaVisitor->getArea();
        return area;
    }

    double MeasurementQueryHandler::getAerielAreaG(const std::vector< ::GeorbIS::Core::GeoPoint> & points)
    {
        if(points.size() <= 2)
        {
            return 0.0;
        }
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating area. No world defined", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        std::vector<thriftcore::GeoPoint>::const_iterator constItr;
        osg::ref_ptr<osg::Vec3dArray> pointArray = new osg::Vec3dArray();
        for(constItr = points.begin(); constItr != points.end(); ++constItr)
        {
            const thriftcore::GeoPoint& point = *constItr;
            osg::Vec3d lonLatPoint = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight
                    (osg::Vec3d(point.latitude, point.longitude, point.altitude));
            pointArray->push_back(lonLatPoint);
        }
        CORE::RefPtr<CORE::IBaseVisitor> areaBaseVisitor = CORE::CoreRegistry::instance()->getVisitorFactory()->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::SurfaceAreaCalcVisitorType);
        CORE::RefPtr<GISCOMPUTE::ISurfaceAreaCalcVisitor> areaVisitor = areaBaseVisitor->getInterface<GISCOMPUTE::ISurfaceAreaCalcVisitor>();
        areaVisitor->setPoints(pointArray);
        areaVisitor->setComputationType(GISCOMPUTE::ISurfaceAreaCalcVisitor::AERIAL);
        CORE::RefPtr<GISCOMPUTE::IFilterVisitor> areaFilterVisitor = areaBaseVisitor->getInterface<GISCOMPUTE::IFilterVisitor>();
        CORE::ITerrain* terrain = world->getTerrain();
        CORE::IBase* terrainAsBase = terrain->getInterface<CORE::IBase>();
        areaBaseVisitor->apply(*terrainAsBase);
#ifdef _WIN32
        Sleep(1000);
#else
        usleep(1000000);
#endif
        while(areaFilterVisitor->getFilterStatus() == GISCOMPUTE::FILTER_PENDING)
        {
#ifdef _WIN32
            Sleep(1000);
#else
            usleep(1000000);
#endif
        }
        double area = areaVisitor->getArea();
        return area;
    }

    double MeasurementQueryHandler::getProjectedAreaE(const std::vector< ::GeorbIS::Core::Point3D> & points)
    {
        if(points.size() <= 2)
        {
            return 0.0;
        }
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating area. No world defined", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        std::vector<thriftcore::Point3D>::const_iterator constItr;
        osg::ref_ptr<osg::Vec3dArray> pointArray = new osg::Vec3dArray();
        for(constItr = points.begin(); constItr != points.end(); ++constItr)
        {
            const thriftcore::Point3D& point = *constItr;
            osg::Vec3d geoPoint = UTIL::CoordinateConversionUtils::ECEFToGeodetic(osg::Vec3d(point.x, point.y, point.z));
            osg::Vec3d lonLatPoint = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight(geoPoint);
            pointArray->push_back(lonLatPoint);
        }
        CORE::RefPtr<CORE::IBaseVisitor> areaBaseVisitor = CORE::CoreRegistry::instance()->getVisitorFactory()->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::SurfaceAreaCalcVisitorType);
        CORE::RefPtr<GISCOMPUTE::ISurfaceAreaCalcVisitor> areaVisitor = areaBaseVisitor->getInterface<GISCOMPUTE::ISurfaceAreaCalcVisitor>();
        areaVisitor->setPoints(pointArray);
        areaVisitor->setComputationType(GISCOMPUTE::ISurfaceAreaCalcVisitor::PROJECTED);
        CORE::RefPtr<GISCOMPUTE::IFilterVisitor> areaFilterVisitor = areaBaseVisitor->getInterface<GISCOMPUTE::IFilterVisitor>();
        CORE::ITerrain* terrain = world->getTerrain();
        CORE::IBase* terrainAsBase = terrain->getInterface<CORE::IBase>();
        areaBaseVisitor->apply(*terrainAsBase);
#ifdef _WIN32
        Sleep(1000);
#else
        usleep(1000000);
#endif
        while(areaFilterVisitor->getFilterStatus() == GISCOMPUTE::FILTER_PENDING)
        {
#ifdef _WIN32
            Sleep(1000);
#else
            usleep(1000000);
#endif
        }
        double area = areaVisitor->getArea();
        return area;
    }

    double MeasurementQueryHandler::getProjectedAreaG(const std::vector< ::GeorbIS::Core::GeoPoint> & points)
    {
        if(points.size() <= 2)
        {
            return 0.0;
        }
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating area. No world defined", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        std::vector<thriftcore::GeoPoint>::const_iterator constItr;
        osg::ref_ptr<osg::Vec3dArray> pointArray = new osg::Vec3dArray();
        for(constItr = points.begin(); constItr != points.end(); ++constItr)
        {
            const thriftcore::GeoPoint& point = *constItr;
            osg::Vec3d lonLatPoint = UTIL::CoordinateConversionUtils::latLongHeightToLongLatHeight
                    (osg::Vec3d(point.latitude, point.longitude, point.altitude));
            pointArray->push_back(lonLatPoint);
        }
        CORE::RefPtr<CORE::IBaseVisitor> areaBaseVisitor = CORE::CoreRegistry::instance()->getVisitorFactory()->createVisitor(*GISCOMPUTE::GISComputeRegistryPlugin::SurfaceAreaCalcVisitorType);
        CORE::RefPtr<GISCOMPUTE::ISurfaceAreaCalcVisitor> areaVisitor = areaBaseVisitor->getInterface<GISCOMPUTE::ISurfaceAreaCalcVisitor>();
        areaVisitor->setPoints(pointArray);
        areaVisitor->setComputationType(GISCOMPUTE::ISurfaceAreaCalcVisitor::PROJECTED);
        CORE::RefPtr<GISCOMPUTE::IFilterVisitor> areaFilterVisitor = areaBaseVisitor->getInterface<GISCOMPUTE::IFilterVisitor>();
        CORE::ITerrain* terrain = world->getTerrain();
        CORE::IBase* terrainAsBase = terrain->getInterface<CORE::IBase>();
        areaBaseVisitor->apply(*terrainAsBase);
#ifdef _WIN32
        Sleep(1000);
#else
        usleep(1000000);
#endif
        while(areaFilterVisitor->getFilterStatus() == GISCOMPUTE::FILTER_PENDING)
        {
#ifdef _WIN32
            Sleep(1000);
#else
            usleep(1000000);
#endif
        }
        double area = areaVisitor->getArea();
        return area;
    }

}
