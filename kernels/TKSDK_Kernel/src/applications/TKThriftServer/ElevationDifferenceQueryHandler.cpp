#include "ElevationDifferenceQueryHandler.h"

#include <Core/RefPtr.h>
#include <Core/CoreRegistry.h>
#include <Core/CorePlugin.h>
#include <Core/IObjectType.h>
#include <Core/IObjectFactory.h>
#include <Core/IVisitorFactory.h>
#include <Core/WorldMaintainer.h>
#include <Core/IWorld.h>
#include <Core/ITerrain.h>
#include <Core/IBaseVisitor.h>
#include <Core/IBase.h>

#include <GISCompute/IFilterVisitor.h>
#include <GISCompute/GISComputePlugin.h>

#include <Util/CoordinateConversionUtils.h>
#include <iostream>

namespace thriftcore = ::GeorbIS::Core;

namespace GeoOrbisProcessing
{
    DEFINE_IREFERENCED(ElevationDifferenceQueryHandler, CORE::Referenced)

    ElevationDifferenceQueryHandler::ElevationDifferenceQueryHandler()
    {

    }

    ElevationDifferenceQueryHandler::~ElevationDifferenceQueryHandler()
    {

    }

    double ElevationDifferenceQueryHandler::getElevationDifference(const std::vector< ::GeorbIS::Core::GeoPoint> & points)
    {
        if(points.size() < 2) return 0.0;
        if(CORE::WorldMaintainer::instance()->getWorldMap().size() < 1)
        {
            LOG_WARNING_VAR("Error calculating min max elevation. No world defined", __FILE__, __LINE__)
            return 0.0;
        }
        CORE::RefPtr<CORE::IWorld> world = CORE::WorldMaintainer::instance()->getWorldMap().begin()->second;
        CORE::ITerrain* terrain = world->getTerrain();
        const thriftcore::GeoPoint& p1 = points[0];
        const thriftcore::GeoPoint& p2 = points[1];
        osg::Vec2d point1(p1.longitude, p1.latitude);
        osg::Vec2d point2(p2.longitude, p2.latitude);
        double elevation1 = 0.0, elevation2 = 0.0;
        terrain->getElevationAt(point1, elevation1);
        terrain->getElevationAt(point2, elevation2);
        return elevation1-elevation2;
    }
}
