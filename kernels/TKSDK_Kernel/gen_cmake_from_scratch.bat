echo off

::Deleting the cmake cache in the root folder
If exist CMakeCache.txt del CMakeCache.txt

::Checking whether the build folder exists or not
IF "%OSSARCH%"=="" (
  ECHO Build Folder is NOT defined. Creating Default Build 
  set FolderName="Build"
  ) ELSE (
 set FolderName=%OSSARCH%
 )

if not exist %FolderName% mkdir %FolderName%

::Changing to build folder and running cmake
cd %FolderName%
If exist CMakeCache.txt del CMakeCache.txt
:: option to use QT4
::cmake -G "Visual Studio 9 2008" -DUSE_QT4=1 -DUSE_NAMESPACE=viz ..
::option to use QT5
cmake -G "Visual Studio 9 2008" -DUSE_NAMESPACE=viz ..
cd ..
echo on