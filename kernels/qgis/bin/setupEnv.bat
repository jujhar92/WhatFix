@ECHO OFF
echo Setting up QGIS Python environment...

set GEORBIS_PYTHON_API_PATH=%~dp0\..\..\..\api\python
set QGIS_PREFIX_PATH=%QGIS_PATH%\apps\qgis
set GDAL_DATA=%QGIS_PATH%\share\gdal
set PYTHONHOME=%QGIS_PATH%\apps\Python27
set PYTHONPATH=%GEORBIS_PYTHON_API_PATH%;%QGIS_PATH%;%QGIS_PATH%\lib;%QGIS_PATH%\bin;%QGIS_PATH%\apps\qgis\python\plugins;%QGIS_PATH%\apps\Python27;%QGIS_PATH%\apps\Python27\Lib;%QGIS_PATH%\apps\Python27\Lib\site-packages;%QGIS_PATH%\apps\qgis\python;%QGIS_PATH%\apps\Python27\DLLs;%QGIS_PATH%\apps\qgis\bin;%QGIS_PATH%\apps\qgis\plugins;%QGIS_PATH%\bin\gdalplugins;%QGIS_PATH%\apps\saga\modules;%QGIS_PATH%\apps\Qt4\plugins;%QGIS_PATH%\apps\grass\grass-6.4.3\lib;%QGIS_PATH%\apps\grass\grass-6.4.3\bin;%QGIS_PATH%\apps\qgis\python\plugins\processing
set PATH=%PYTHONPATH%;%THRIFT_PATH%;%PATH%

call "%QGIS_PATH%\bin\o4w_env.bat"
call "%OSGEO4W_ROOT%"\apps\grass\grass-6.4.3\etc\env.bat
path %OSGEO4W_ROOT%\apps\qgis\bin;%OSGEO4W_ROOT%\apps\grass\grass-6.4.3\lib;%PATH%
set QGIS_PREFIX_PATH=%OSGEO4W_ROOT:\=/%/apps/qgis
set GDAL_FILENAME_IS_UTF8=YES
rem Set VSI cache to be used as buffer, see #6448
set VSI_CACHE=TRUE
set VSI_CACHE_SIZE=1000000
set QT_PLUGIN_PATH=%OSGEO4W_ROOT%\apps\qgis\qtplugins;%OSGEO4W_ROOT%\apps\qt4\plugins
echo "...Ready"