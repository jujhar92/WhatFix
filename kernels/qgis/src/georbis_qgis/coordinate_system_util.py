
__date__ = 'June 5 2015'
__copyright__ = ' (C) 2013 VizExperts India Pvt Ltd'

import math

from georbis.core.ttypes import GeoPoint


class CoordinateSystemUtil:

    # Constants according to WGS84 standards
    earth_semi_major_axis = 6378137.0
    earth_semi_minor_axis = 6356752.3142
    eccentricity = 0.081819190842621
    eccentricitySquared = 0.00669437999014
    eccentricity2Squared = 0.0067394967565869027

    # Constructor
    def __init__(self):
        pass

    # Convert Given Geodatic Point into Cartesian Coordinate System using WGS84 reference
    @staticmethod
    def convert_geodatic_point_to_ecef(point):
        latitude_in_radian = point.latitude * math.pi / 180
        longitude_in_radian = point.longitude * math.pi / 180
        n_phi = CoordinateSystemUtil.earth_semi_major_axis / math.sqrt(1-(CoordinateSystemUtil.eccentricitySquared*math.pow(math.sin(latitude_in_radian), 2)))
        x = (n_phi + point.elevation) * math.cos(latitude_in_radian) * math.cos(longitude_in_radian)
        y = (n_phi + point.elevation) * math.cos(latitude_in_radian) * math.sin(longitude_in_radian)
        z = (n_phi * (1-CoordinateSystemUtil.eccentricitySquared) + point.elevation) * math.sin(latitude_in_radian)
        new_point = GeoPoint(x, y, z)
        return new_point

    # Convert Given ECEF Coordinate to Geodatic Cooridnate System using WGS84 reference
    @staticmethod
    def convert_ecef_point_to_geodatic(point):
        p = math.sqrt(math.pow(point.latitude,2) + math.pow(point.longitude,2))
        theta = math.atan((point.elevation * CoordinateSystemUtil.earth_semi_major_axis) / (p * CoordinateSystemUtil.earth_semi_minor_axis))
        nume = point.elevation+(CoordinateSystemUtil.eccentricity2Squared*CoordinateSystemUtil.earth_semi_minor_axis*math.pow(math.sin(theta),3))
        deno = p-(CoordinateSystemUtil.eccentricitySquared*CoordinateSystemUtil.earth_semi_major_axis*math.pow(math.cos(theta),3))
        phi = math.atan(nume/deno)
        lamda = math.atan2(point.longitude, point.latitude)
        n_phi = CoordinateSystemUtil.earth_semi_major_axis / math.sqrt(1-CoordinateSystemUtil.eccentricitySquared*math.pow(math.sin(phi),2))
        h = (p/math.cos(phi)) - n_phi
        new_point = GeoPoint(phi * 180 / math.pi,lamda * 180 / math.pi, h)
        return new_point
