__date__ = 'June 9 2015'
__copyright__ = ' (C) 2013 VizExperts India Pvt Ltd'

import math
from georbis.core.ttypes import GeoPoint


class PointUtil:
    def __init__(self):
        pass

    # Create and returns a normalized vector of given point
    @staticmethod
    def normalize(point):
        new_point = GeoPoint()
        magnitude = math.sqrt(math.pow(point.latitude, 2) + math.pow(point.longitude, 2) + math.pow(point.elevation, 2))
        new_point.latitude = point.latitude / magnitude
        new_point.longitude = point.longitude / magnitude
        new_point.elevation = point.elevation / magnitude
        return new_point

    # Add given two point
    @staticmethod
    def add(*args):
        new_point = GeoPoint(0, 0, 0)
        for point in args:
            new_point.latitude += point.latitude
            new_point.longitude += point.longitude
            new_point.elevation += point.elevation
        return new_point

    # Substract second point from first
    @staticmethod
    def subtract(point1, point2):
        new_point = GeoPoint()
        new_point.latitude = point1.latitude - point2.latitude
        new_point.longitude = point1.longitude - point2.longitude
        new_point.elevation = point1.elevation - point2.elevation
        return new_point

    # Add scaler to given point
    @staticmethod
    def add_scaler(point, scaler):
        new_point = GeoPoint()
        new_point.latitude = point.latitude + scaler
        new_point.longitude = point.longitude + scaler
        new_point.elevation = point.elevation + scaler
        return new_point

    # subtract scaler from given point
    @staticmethod
    def sub_scaler(point, scaler):
        new_point = GeoPoint()
        new_point.latitude = point.latitude - scaler
        new_point.longitude = point.longitude - scaler
        new_point.elevation = point.elevation - scaler
        return new_point

    # Multiply scaler to given point
    @staticmethod
    def multiply_scaler(point, scaler):
        new_point = GeoPoint()
        new_point.latitude = point.latitude * scaler
        new_point.longitude = point.longitude * scaler
        new_point.elevation = point.elevation * scaler
        return new_point

    # Divide scaler from given point
    @staticmethod
    def divide_scaler(point, scaler):
        new_point = GeoPoint(0, 0, 0)
        if scaler == 0:
            return new_point
        new_point.latitude = point.latitude / scaler
        new_point.longitude = point.longitude / scaler
        new_point.elevation = point.elevation / scaler
        return new_point

    # returns length of given vector
    @staticmethod
    def length(point):
        return math.sqrt(math.pow(point.latitude, 2) + math.pow(point.longitude, 2) + math.pow(point.elevation, 2))

    # Calculate length between two point
    @staticmethod
    def length_between_points(point1, point2):
        x2 = math.pow(point2.latitude - point1.latitude, 2)
        y2 = math.pow(point2.longitude - point1.longitude, 2)
        z2 = math.pow(point2.elevation - point1.elevation, 2)
        length = math.sqrt(x2 + y2 + z2)
        return length
