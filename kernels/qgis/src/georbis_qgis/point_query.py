import os

from qgis.core import QgsApplication
from qgis.core import QgsRaster
from qgis.core import QgsPoint

class PointQuery:
    """Helps with queries related to a set of points"""

    def queryElevations(self, pointList, rasterLayer):
        """Return a list of elevations for an input list of points in input raster layer"""
        rasterExtents = rasterLayer.extent()
        outputElevations = []
        for point in pointList:
            resultList = rasterLayer.dataProvider().identify(point, QgsRaster.IdentifyFormatValue).results().values()
            # We only want results from the first band
            ptHeight = resultList[0]
            if ptHeight:
                outputElevations.append(ptHeight)
            else:
                outputElevations.append(0.0)
        return outputElevations

    # Returns Elevation for given GeoPoint object in Geodatic form from the given raster layer
    def query_elevation(self, point, rasterlayer):
        qgs_point = QgsPoint(point.latitude, point.longitude)
        result_list = rasterlayer.dataProvider().identify(qgs_point, QgsRaster.IdentifyFormatValue).results().values()
        height = result_list[0]
        return height;
