__date__ = 'June 4 2015'
__copyright__ = ' (C) 2013 VizExperts India Pvt Ltd'

import math

from qgis.core import QgsRaster
from qgis.core import QgsRasterLayer
from qgis.core import QgsPoint
from georbis.core.ttypes import GeoPoint
from point_query import PointQuery
from coordinate_system_util import CoordinateSystemUtil
from point_util import PointUtil

class AreaMeasurement:

    # Constructor
    def __init__(self):
        self.elevation_query = PointQuery()

    # returns calculated area using aeriel measurement represented by given set of points
    def get_aeriel_area(self, points, layers):
        if len(points) < 3:
            return 0
        total_area = 0
        for idx in range(1, len(points)-1, 1):
            calculated_area = self.get_aeriel_area_of_triangle(points[0], points[idx], points[idx+1])
            total_area += calculated_area
        return total_area

    # returns calculated area using projection represented by given set of points
    def get_projected_area(self, points, layers):
        if len(points) < 3:
            return 0
        total_area = 0
        for idx in range(1, len(points)-1, 1):
            claculated_area = self.get_projected_area_of_triangle(points[0], points[idx], points[idx+1], layers)
            total_area += claculated_area
        return total_area

    # returns calculated area for given traingle using aeriel method
    def get_aeriel_area_of_triangle(self, point1, point2, point3):
        length1 = math.sqrt(math.pow(point2.latitude-point1.latitude,2) + math.pow(point2.longitude-point1.longitude,2) + math.pow(point2.elevation-point1.elevation,2))
        length2 = math.sqrt(math.pow(point3.latitude-point2.latitude,2) + math.pow(point3.longitude-point2.longitude,2) + math.pow(point3.elevation-point2.elevation,2))
        length3 = math.sqrt(math.pow(point1.latitude-point3.latitude,2) + math.pow(point1.longitude-point3.longitude,2) + math.pow(point1.elevation-point3.elevation,2))
        perimeter = (length1 + length2 + length3) / 2.0
        area_square = perimeter * (perimeter - length1) * (perimeter - length2) * (perimeter - length3)
        return math.sqrt(area_square)

    # returns calculated area for given triangle using projected method
    def get_projected_area_of_triangle(self, point1, point2, point3, layers):
        point1_in_geodatic = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point1)
        point2_in_geodatic = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point2)
        point3_in_geodatic = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point3)
        max_latitude = max(point1_in_geodatic.latitude, point2_in_geodatic.latitude, point3_in_geodatic.latitude)
        min_latitude = min(point1_in_geodatic.latitude, point2_in_geodatic.latitude, point3_in_geodatic.latitude)
        max_longitude = max(point1_in_geodatic.longitude, point2_in_geodatic.longitude, point3_in_geodatic.longitude)
        min_longitude = min(point1_in_geodatic.longitude, point2_in_geodatic.longitude, point3_in_geodatic.longitude)
        point00_geodatic = GeoPoint(min_latitude, min_longitude, 0)
        point01_geodatic = GeoPoint(max_latitude, min_longitude, 0)
        point10_geodatic = GeoPoint(min_latitude, max_longitude, 0)
        point11_geodatic = GeoPoint(max_latitude, max_longitude, 0)
        point00 = CoordinateSystemUtil.convert_geodatic_point_to_ecef(point00_geodatic)
        point01 = CoordinateSystemUtil.convert_geodatic_point_to_ecef(point01_geodatic)
        point10 = CoordinateSystemUtil.convert_geodatic_point_to_ecef(point10_geodatic)
        point11 = CoordinateSystemUtil.convert_geodatic_point_to_ecef(point11_geodatic)
        y_direction = PointUtil.subtract(point01, point00)
        y_length = PointUtil.length(y_direction)
        y_direction = PointUtil.normalize(y_direction)
        x_direction = PointUtil.subtract(point10, point00)
        x_length = PointUtil.length(x_direction)
        x_direction = PointUtil.normalize(x_direction)
        x_length = int(x_length) + 1
        y_length = int(y_length) + 1
        total_area = 0
        step = 50
        for idy in range(1, y_length, step):
            for idx in range(1, x_length, step):
                cell_point00 = PointUtil.add(point00, PointUtil.multiply_scaler(x_direction, idx-step), PointUtil.multiply_scaler(y_direction, idy-step))
                cell_point01 = PointUtil.add(point00, PointUtil.multiply_scaler(x_direction, idx-step), PointUtil.multiply_scaler(y_direction, idy))
                cell_point10 = PointUtil.add(point00, PointUtil.multiply_scaler(x_direction, idx), PointUtil.multiply_scaler(y_direction, idy-step))
                cell_point11 = PointUtil.add(point00, PointUtil.multiply_scaler(x_direction, idx), PointUtil.multiply_scaler(y_direction, idy))
                center_point = self.get_center_point_of_square(cell_point00, cell_point01, cell_point11, cell_point10)
                is_inside = self.is_point_in_triangle(center_point, point1, point2, point3)
                if is_inside is True:
                    calculated_area = self.get_area_of_rectangle(cell_point00, cell_point01, cell_point11, cell_point10, layers)
                    total_area += calculated_area
        return total_area

    # returns point is in the triangle or not
    def is_point_in_triangle(self, point, point0, point1, point2):
        p = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point)
        p1 = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point0)
        p2 = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point1)
        p3 = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point2)
        dt = (p2.longitude - p3.longitude)*(p1.latitude - p3.latitude) + (p3.latitude - p2.latitude)*(p1.longitude - p3.longitude)
        alpha = ((p2.longitude - p3.longitude)*(p.latitude - p3.latitude) + (p3.latitude - p2.latitude)*(p.longitude - p3.longitude)) / dt
        beta = ((p3.longitude - p1.longitude)*(p.latitude - p3.latitude) + (p1.latitude - p3.latitude)*(p.longitude - p3.longitude)) / dt
        gamma = 1.0 - alpha - beta
        return alpha > 0.0 and beta > 0.0 and gamma > 0.0

    # returns central point of given Square
    def get_center_point_of_square(self, point1, point2, point3, point4):
        new_point = GeoPoint()
        new_point.latitude = (point1.latitude + point2.latitude + point3.latitude + point4.latitude) / 4
        new_point.longitude = (point1.longitude + point2.longitude + point3.longitude + point4.longitude) / 4
        new_point.elevation = (point1.elevation + point2.elevation + point3.elevation + point4.elevation) / 4
        return new_point

    def get_area_of_rectangle(self, point1, point2, point3, point4, layer):
        point1_in_geodatic = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point1)
        point2_in_geodatic = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point2)
        point3_in_geodatic = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point3)
        point4_in_geodatic = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point4)
        point1_in_geodatic.elevation = self.elevation_query.query_elevation(point1_in_geodatic,layer)
        point2_in_geodatic.elevation = self.elevation_query.query_elevation(point2_in_geodatic,layer)
        point3_in_geodatic.elevation = self.elevation_query.query_elevation(point3_in_geodatic,layer)
        point4_in_geodatic.elevation = self.elevation_query.query_elevation(point4_in_geodatic,layer)
        point1_in_ecef = CoordinateSystemUtil.convert_geodatic_point_to_ecef(point1_in_geodatic)
        point2_in_ecef = CoordinateSystemUtil.convert_geodatic_point_to_ecef(point2_in_geodatic)
        point3_in_ecef = CoordinateSystemUtil.convert_geodatic_point_to_ecef(point3_in_geodatic)
        point4_in_ecef = CoordinateSystemUtil.convert_geodatic_point_to_ecef(point4_in_geodatic)
        width = PointUtil.length_between_points(point1_in_ecef, point2_in_ecef)
        height = PointUtil.length_between_points(point2_in_ecef, point3_in_ecef)
        return width * height
