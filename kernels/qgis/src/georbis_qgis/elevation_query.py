class PointQuery:
    """Helps with queries related to a set of points"""

    def queryElevations(self, pointList, rasterLayer):
        """Return a list of elevations for an input list of points in input raster layer"""
        rasterExtents = rasterLayer.extent()
        print rasterExtents.xMinimum(), rasterExtents.yMinimum(), rasterExtents.width(), rasterExtents.height()

        outputElevations = []
        for point in pointList:
            outputElevations.append(rasterLayer.dataProvider().identify(point,QgsRaster.IdentifyFormatValue).results().values())
    
