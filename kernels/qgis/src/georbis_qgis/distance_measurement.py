__date__ = 'June 4 2015'
__copyright__ = ' (C) 2013 VizExperts India Pvt Ltd'

import math

from qgis.core import QgsPoint
from georbis.core.ttypes import GeoPoint
from point_query import PointQuery
from coordinate_system_util import CoordinateSystemUtil

class DistanceMeasurement:

    # Constructor
    def __init__(self):
        self.elevation_query = PointQuery()

    # Returns sum of aeriel distance between given points
    def get_aeriel_distance(self, pointlist, layer):
        total_distance = 0
        for idx in range(0, len(pointlist)-1, 1):
            calculated_distance = self.get_aeriel_distance_between_two_point(pointlist[idx], pointlist[idx+1], layer)
            total_distance += calculated_distance
        return total_distance

    # Returns sum of projected distance between given points
    def get_projected_distance(self, pointlist, layer):
        total_distance = 0
        for idx in range(0, len(pointlist)-1, 1):
            calculated_distance = self.get_projected_distance_between_two_point(pointlist[idx], pointlist[idx+1], layer)
            total_distance += calculated_distance
        return total_distance

    # Returns aeriel distance between two given point
    def get_aeriel_distance_between_two_point(self, point1, point2, layer):
        distance = math.sqrt(math.pow(point2.latitude - point1.latitude, 2) + math.pow(point2.longitude - point1.longitude, 2) + math.pow(point2.elevation - point1.elevation, 2))
        return distance

    # Returns projected distance between two given point
    def get_projected_distance_between_two_point(self, point1, point2, layer):
        return self.get_distance_between_points_with_interval(point1, point2, 1, layer)

    # Returns distance between two points calculated with given interval in projected mode
    def get_distance_between_points_with_interval(self, point1, point2, interval, layer):
        direction_vector = GeoPoint(point2.latitude-point1.latitude, point2.longitude-point1.longitude, point2.elevation-point1.elevation)
        length = math.sqrt(math.pow(direction_vector.latitude,2) + math.pow(direction_vector.longitude,2) + math.pow(direction_vector.elevation,2))
        direction_vector.latitude /= length
        direction_vector.longitude /= length
        direction_vector.elevation /= length
        total_distance = 0.0
        point1_in_geodatic = CoordinateSystemUtil.convert_ecef_point_to_geodatic(point1)
        point1_in_geodatic.elevation = self.elevation_query.query_elevation(point1_in_geodatic, layer)
        point1_in_ecef = CoordinateSystemUtil.convert_geodatic_point_to_ecef(point1_in_geodatic)
        last_point = point1_in_ecef
        int_range = int(length)+1
        for i in range(0, int_range, interval):
            interpolated_point = GeoPoint()
            interpolated_point.latitude = point1.latitude + (direction_vector.latitude * i)
            interpolated_point.longitude = point1.longitude + (direction_vector.longitude * i)
            interpolated_point.elevation = point1.elevation + (direction_vector.elevation * i)
            interpolated_point_in_geodatic = CoordinateSystemUtil.convert_ecef_point_to_geodatic(interpolated_point)
            interpolated_point_in_geodatic.elevation = self.elevation_query.query_elevation(interpolated_point_in_geodatic,layer)
            interpolated_point_in_ecef = CoordinateSystemUtil.convert_geodatic_point_to_ecef(interpolated_point_in_geodatic)
            calculated_distance = self.get_aeriel_distance_between_two_point(last_point,interpolated_point_in_ecef,layer)
            total_distance += calculated_distance
            last_point = interpolated_point_in_ecef
        return total_distance
