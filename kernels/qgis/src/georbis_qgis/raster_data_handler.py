from PyQt4.QtCore import QFileInfo
from qgis.core import QgsRasterLayer

class RasterDataHandler:
    """Loads raster files"""

    def __init__(self):
        self._errorList = []

    def errors(self):
        """Return the list of errors that occured during loading"""
        return self._errorList

    def lastError(self):
        """Returns the last known error that occured"""
        return self._errorList[len(self._errorList)-1]    

    def loadFile(self, rasterFile):
        """Load and return a QgsRasterLayer from rasterFile

        Does not throw an exception if rasterfile is invalid.
        Instead, it simply returns None. Errors during this call
        can be obtained using the lastError() method    
        """
        fileInfo = QFileInfo(rasterFile)
        fileName=fileInfo.fileName()
        baseName = fileInfo.baseName()

        if not fileInfo.exists():
            self._errorList.append("File does not exist")
            return None

        errorStr = "Unknown"
        try:
            if QgsRasterLayer.isValidRasterFileName(rasterFile, errorStr):
                rlayer = QgsRasterLayer(rasterFile, baseName)
                if not rlayer.isValid():
                    self._errorList.append("Layer failed to load!")
                    return None
                else:
                    return rlayer	
            else:
                self._errorList.append("File is not a valid raster. In particular: " + errorStr)
                return None

        except Exception as e:
            self._errorList.append("Exception while loading raster file: " + str(e))
            return None