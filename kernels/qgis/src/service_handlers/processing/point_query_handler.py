from qgis.core import *

from georbis_qgis.point_query import PointQuery
from georbis_qgis.raster_data_handler import RasterDataHandler

class PointQueryHandler(object):
    def __init__(self):
        self.log = {}

    def get_elevations(self, inputpoints, layerName):
        elevation_list = []
        data_handler = RasterDataHandler()
        raster_layer = data_handler.loadFile("E:\\Projects\\ADE-GIS\\data\\43j11\\43j11.dt2")
        if raster_layer:
            # Convert GeoPoint list to QgsPoints
            qgis_point_list = []
            for pt in inputpoints:
                qgis_point_list.append(QgsPoint(pt.latitude, pt.longitude))

            point_query = PointQuery()
            elevation_list = point_query.queryElevations(qgis_point_list, raster_layer)

        return elevation_list
