import os

from qgis.core import QgsApplication

from georbis.processing import PointsQuery

# from service_handlers.core.workspace_handler import WorkspaceHandler
from service_handlers.processing.point_query_handler import PointQueryHandler

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TJSONProtocol
from thrift.server import TServer

def main():
    prefix_path = os.environ['QGIS_PREFIX_PATH']
    QgsApplication.setPrefixPath(prefix_path, True)
    QgsApplication.initQgis()

    handler = PointQueryHandler()
    processor = PointsQuery.Processor(handler)
    transport = TSocket.TServerSocket(host="127.0.0.1", port=9090)
    tfactory = TTransport.TFramedTransportFactory()
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()

    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)
    print "Starting GeorbIS QGIS kernel"
    server.serve()

if __name__ == "__main__":
    main()