from georbis.core.ttypes import GeoPoint
from georbis.processing import PointsQuery

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TJSONProtocol

try:
    # Make socket
    transport = TSocket.TSocket('localhost', 9090)

    transport = TTransport.TFramedTransport(transport)

    # Wrap in a protocol
    protocol = TBinaryProtocol.TBinaryProtocol(transport)

    client = PointsQuery.Client(protocol)

    # Connect!
    transport.open()

    pts = (GeoPoint(latitude=74.53951, longitude=34.36709, elevation=0.0),
           GeoPoint(latitude=74.53951, longitude=34.36709, elevation=0.0),
           GeoPoint(latitude=74.53951, longitude=34.36709, elevation=0.0),
           GeoPoint(latitude=74.53951, longitude=34.36709, elevation=0.0),
           GeoPoint(latitude=74.53951, longitude=34.36709, elevation=0.0),
           GeoPoint(latitude=74.53951, longitude=34.36709, elevation=0.0))

    result = client.getElevations(pts, "dummylayername")
    for val in result:
        print val

    # close the transport
    transport.close()

except Thrift.TException, tx:
    print 'Error when running PointsQuery Client: \n%s' % (tx.message)