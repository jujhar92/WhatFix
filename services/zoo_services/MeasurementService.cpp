#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TMultiplexedProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/transport/TTransport.h>
#include <thrift/transport/TSocket.h>
#include <thrift/TProcessor.h>

#include "MeasurementQuery.h"
#include "ElevationQuery.h"
#include <fstream>
#include <iostream>
#include "service.h"

#include "ServiceCommonUtil.h"

#ifdef WIN32
__declspec(dllexport)
#endif

std::string getThriftServerIP()
{
    return "127.0.0.1";
}

int getThriftServerPort()
{
    return 9003;
}

void logToFile(std::string data)
{
    std::ofstream ofs;
    ofs.open("/home/bhawesh/Projects/GeoOrbisTKPBuild/lib/linuxx86_64/datalog.txt", std::ios::app);
    ofs << data.c_str() << "\n";
    ofs.close();
}

int getAerielDistanceE(maps*& conf,maps*& inputs, maps*& outputs)
{
    logMap(inputs, "/home/bhawesh/Projects/GeoOrbisTKPBuild/lib/linuxx86_64/maplog.txt");
    return SERVICE_FAILED;
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(getThriftServerIP(), getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    std::vector<GeorbIS::Core::Point3D> points;
    maps* mapsHead = inputs;
    while(mapsHead != NULL)
    {

    }
    float calculatedDistance = 0;
    try
    {
        transport->open();
        GeorbIS::Core::Point3D p1;
        GeorbIS::Core::Point3D p2;
        GeorbIS::Core::Point3D p3;
        points.push_back(p1);
        points.push_back(p2);
        points.push_back(p3);
        calculatedDistance = measurementclient->getAerielDistanceE(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating aeriel distance\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedDistance);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getAeirelDistanceG(maps*& conf,maps*& inputs, maps*& outputs)
{
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(getThriftServerIP(), getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    float calculatedDistance = 0;
    try
    {
        transport->open();
        std::vector<GeorbIS::Core::GeoPoint> points;
        GeorbIS::Core::GeoPoint p1;
        GeorbIS::Core::GeoPoint p2;
        GeorbIS::Core::GeoPoint p3;
        points.push_back(p1);
        points.push_back(p2);
        points.push_back(p3);
        calculatedDistance = measurementclient->getAeirelDistanceG(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating aeriel distance\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedDistance);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getProjectedDistanceE(maps*& conf,maps*& inputs, maps*& outputs)
{
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(getThriftServerIP(), getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    float calculatedDistance = 0;
    try
    {
        transport->open();
        std::vector<GeorbIS::Core::Point3D> points;
        GeorbIS::Core::Point3D p1;
        GeorbIS::Core::Point3D p2;
        GeorbIS::Core::Point3D p3;
        points.push_back(p1);
        points.push_back(p2);
        points.push_back(p3);
        calculatedDistance = measurementclient->getProjectedDistanceE(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating projected distance\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedDistance);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getProjectedDistanceG(maps*& conf,maps*& inputs, maps*& outputs)
{
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(getThriftServerIP(), getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    float calculatedDistance = 0;
    try
    {
        transport->open();
        std::vector<GeorbIS::Core::GeoPoint> points;
        GeorbIS::Core::GeoPoint p1;
        p1.latitude = 34.11032486; p1.longitude = 74.12622833; p1.altitude = 2210.137451;
        GeorbIS::Core::GeoPoint p2;
        p2.latitude = 34.53393173; p2.longitude = 74.19942474; p2.altitude = 1700.283691;
        GeorbIS::Core::GeoPoint p3;
        p3.latitude = 34.77204132; p3.longitude = 74.80120087; p3.altitude = 3087.669922;
        GeorbIS::Core::GeoPoint p4;
        p4.latitude = 34.43319321; p4.longitude = 74.82374573; p4.altitude = 3123.807373;
        points.push_back(p1);
        points.push_back(p2);
        points.push_back(p3);
        points.push_back(p4);
        //logToFile("Calling distance with number of points");
        calculatedDistance = measurementclient->getProjectedDistanceG(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating projected distance\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedDistance);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getAerielAreaE(maps*& conf,maps*& inputs, maps*& outputs)
{
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(getThriftServerIP(), getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    float calculatedArea = 0;
    try
    {
        transport->open();
        std::vector<GeorbIS::Core::Point3D> points;
        GeorbIS::Core::Point3D p1;
        GeorbIS::Core::Point3D p2;
        GeorbIS::Core::Point3D p3;
        points.push_back(p1);
        points.push_back(p2);
        points.push_back(p3);
        calculatedArea = measurementclient->getAerielAreaE(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating aeriel area\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedArea);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getAerielAreaG(maps*& conf,maps*& inputs, maps*& outputs)
{
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(getThriftServerIP(), getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    float calculatedArea = 0;
    try
    {
        transport->open();
        std::vector<GeorbIS::Core::GeoPoint> points;
        GeorbIS::Core::GeoPoint p1;
        GeorbIS::Core::GeoPoint p2;
        GeorbIS::Core::GeoPoint p3;
        points.push_back(p1);
        points.push_back(p2);
        points.push_back(p3);
        calculatedArea = measurementclient->getAerielAreaG(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating aeriel area\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedArea);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getProjectedAreaE(maps*& conf,maps*& inputs, maps*& outputs)
{
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(getThriftServerIP(), getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    float calculatedArea = 0;
    try
    {
        transport->open();
        std::vector<GeorbIS::Core::Point3D> points;
        GeorbIS::Core::Point3D p1;
        GeorbIS::Core::Point3D p2;
        GeorbIS::Core::Point3D p3;
        points.push_back(p1);
        points.push_back(p2);
        points.push_back(p3);
        calculatedArea = measurementclient->getProjectedAreaE(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating projected area\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedArea);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getProjectedAreaG(maps*& conf,maps*& inputs, maps*& outputs)
{
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(getThriftServerIP(), getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> measurementProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "MeasurementQuery"));
    boost::shared_ptr<GeorbIS::Processing::MeasurementQueryClient> measurementclient
    (new GeorbIS::Processing::MeasurementQueryClient(measurementProtocol));
    float calculatedArea = 0;
    try
    {
        transport->open();
        std::vector<GeorbIS::Core::GeoPoint> points;
        GeorbIS::Core::GeoPoint p1;
        GeorbIS::Core::GeoPoint p2;
        GeorbIS::Core::GeoPoint p3;
        points.push_back(p1);
        points.push_back(p2);
        points.push_back(p3);
        calculatedArea = measurementclient->getProjectedAreaG(points);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in calculating projected area\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedArea);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getElevationAtPointE(maps*& conf,maps*& inputs, maps*& outputs)
{
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(getThriftServerIP(), getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> elevationProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "ElevationQuery"));
    boost::shared_ptr<GeorbIS::Processing::ElevationQueryClient> elevationClient
    (new GeorbIS::Processing::ElevationQueryClient(elevationProtocol));
    float calculatedElevation = 0.0f;
    try
    {
        transport->open();
        GeorbIS::Core::Point3D point;
        calculatedElevation = elevationClient->getElevationAtPointE(point);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in getting elevation\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedElevation);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}

int getElevationAtPointG(maps*& conf,maps*& inputs, maps*& outputs)
{
    namespace thrift = apache::thrift;
    boost::shared_ptr<thrift::transport::TTransport> socket(new thrift::transport::TSocket(getThriftServerIP(), getThriftServerPort()));
    boost::shared_ptr<thrift::transport::TTransport> transport(new thrift::transport::TBufferedTransport(socket));
    boost::shared_ptr<thrift::protocol::TProtocol> protocol(new thrift::protocol::TBinaryProtocol(transport));
    boost::shared_ptr<thrift::protocol::TMultiplexedProtocol> elevationProtocol
    (new thrift::protocol::TMultiplexedProtocol(protocol, "ElevationQuery"));
    boost::shared_ptr<GeorbIS::Processing::ElevationQueryClient> elevationClient
    (new GeorbIS::Processing::ElevationQueryClient(elevationProtocol));
    float calculatedElevation = 0.0f;
    try
    {
        transport->open();
        GeorbIS::Core::GeoPoint point;
        calculatedElevation = elevationClient->getElevationAtPointG(point);
    }
    catch(thrift::TException& ex)
    {
        std::cout<< "Error in getting elevation\n";
        return SERVICE_FAILED;
    }
    char resChar[256];
    sprintf(resChar,"%f",calculatedElevation);
    setMapInMaps(outputs, "Result", "value", resChar);
    return SERVICE_SUCCEEDED;
}
