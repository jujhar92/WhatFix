import com.vizexperts.georbis.core.GeoPoint;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TJSONProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import com.vizexperts.georbis.processing.PointsQuery;

import java.util.ArrayList;
import java.util.List;

public class QueryPointsTest {
    public static void main(String [] args) {
        System.out.println("Starting client...");
        try {
            TFramedTransport.Factory transportFactory = new TFramedTransport.Factory();

            TTransport transport;
            transport =  transportFactory.getTransport(new TSocket("127.0.0.1", 9090));
            transport.open();

            TProtocol protocol = new TBinaryProtocol(transport);
            PointsQuery.Client client = new PointsQuery.Client(protocol);

            perform(client);

            transport.close();
        } catch (TTransportException x) {
            System.out.println("Unable to connect to service provider: " + "localhost" + 9090);
        } catch (TException err){
            System.out.println("Error when accessing remote service: ");
            err.printStackTrace();
        }
    }

    private static void perform(PointsQuery.Client client) throws TException
    {
        List<GeoPoint> pointList = new ArrayList<GeoPoint>();

        GeoPoint pt1 = new GeoPoint(74.53951, 34.36709, 0.0);
        GeoPoint pt2 = new GeoPoint(74.52242,34.35413, 0.0);
        GeoPoint pt3 = new GeoPoint(74.51398,34.41069, 0.0);
        GeoPoint pt4 = new GeoPoint(83, 39.36709, 0.0);

        pointList.add(pt1);
        pointList.add(pt2);
        pointList.add(pt3);
        pointList.add(pt4);

        List<Double> result = client.getElevations(pointList, "dummylayername");
        for(Double resultVal : result){
            System.out.print(resultVal.toString() + " ");
        }
    }
}