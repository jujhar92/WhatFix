@echo OFF
SET PATH=%THRIFT_PATH%\bin;%PATH%
::thrift.exe -gen py:tornado -I %~dp0/../thrift_api -r -out %~dp0/../api/python %~dp0/../thrift_api/all.thrift
thrift.exe -gen py -I %~dp0/../thrift_api -r -out %~dp0/../api/python %~dp0/../thrift_api/all.thrift
thrift.exe -gen java -I %~dp0/../thrift_api -r -out %~dp0/../api/java/src/main/java %~dp0/../thrift_api/all.thrift
thrift.exe -gen cpp -I %~dp0/../thrift_api -r -out %~dp0/../api/cpp %~dp0/../thrift_api/all.thrift
thrift.exe -gen html -I %~dp0/../thrift_api -r -out %~dp0/../api/html %~dp0/../thrift_api/all.thrift