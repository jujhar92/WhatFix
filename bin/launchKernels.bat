@echo OFF 
call %~dp0\..\envSettings.bat

:: Start the QGIS kernel
call %~dp0\..\kernels\qgis\bin\setupEnv.bat
call %~dp0\..\kernels\qgis\bin\launch.bat