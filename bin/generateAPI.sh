get_current_script_dir(){
    # Get path to current script file
    current_script_dir="${BASH_SOURCE[0]}"
    while [ -h "$current_script_dir" ]; do # resolve $current_script_dir until the file is no longer a symlink
      current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
      current_script_dir="$(readlink "$current_script_dir")"
      [[ $current_script_dir != /* ]] && current_script_dir="$current_dir/$current_script_dir" # if $current_script_dir was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    current_dir="$( cd -P "$( dirname "$current_script_dir" )" && pwd )"
    echo "$current_dir"
}

current_dir="$(get_current_script_dir)"
source_root="$(dirname $current_dir)"
if [ -z "$GEORBIS_SERVER_DEPENDENCIES_DIRECTORY" ]; then
    chmod a+x "$source_root/envSettings.sh"
    source "$source_root/envSettings.sh"
fi    

chmod a+x "$THRIFT_PATH/bin/thrift"

# Generate the requisite output directories
mkdir -p "$source_root/api/python"
mkdir -p "$source_root/api/java/src/main/java"
mkdir -p "$source_root/api/cpp"
mkdir -p "$source_root/api/html"

export PATH="$THRIFT_PATH/bin":$PATH
thrift -gen py -I "$source_root/thrift_api" -r -out "$source_root/api/python" "$source_root/thrift_api/all.thrift"
thrift -gen java -I "$source_root/thrift_api" -r -out "$source_root/api/java/src/main/java" "$source_root/thrift_api/all.thrift"
thrift -gen cpp -I "$source_root/thrift_api" -r -out "$source_root/api/cpp" "$source_root/thrift_api/all.thrift"
thrift -gen html -I "$source_root/thrift_api" -r -out "$source_root/api/html" "$source_root/thrift_api/all.thrift"